﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class SelfReleaseAddresable : MonoBehaviour
{
    public GameStateWithAddressableAssets owner;

   /* [Button]
    public v oid PrintReferences(Scene scene)
    {
        Addressables.ResourceManager.CleanupSceneInstances(scene);
    }
    */


    [Button]
    public void PrintReferences(Scene scene)
    {
    }

    [Button]
    public void ReleaseFromGameStateOwner()
    {
        owner.Release(this);
    }

    internal void Initialize(GameStateWithAddressableAssets gameStateWithAddressableAssets)
    {
        owner = gameStateWithAddressableAssets;
    }
}
