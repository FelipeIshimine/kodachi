﻿using System.Collections.Generic;

[System.Serializable]
public struct TimelineSequence
{
    public string name;
    public List<double> timestamps;

    public TimelineSequence(string name)
    {
        this.name = name;
        this.timestamps = new List<double>();
    }

    public TimelineSequence(string name, List<double> timestamps)
    {
        this.name = name;
        this.timestamps = timestamps;
    }

    public double GetNext(double currentTime)
    {
        for (int i = 0; i < timestamps.Count; i++)
        {
            if (timestamps[i] >= currentTime)
                return timestamps[i];
        }
        return -1;
    }

    internal double GetPrevious(double currentTime)
    {
        double last = 0;
        for (int i = 0; i < timestamps.Count; i++)
        {
            if (timestamps[i] >= currentTime)
                return last;
            else
                last = timestamps[i];
        }
        return -1;
    }
}
