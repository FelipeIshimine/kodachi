﻿using System;
using UnityEngine;
using UnityEngine.Events;

public abstract class Canvas_Action : MonoBehaviour
{
    public UnityEvent OnSkipped;
    public Action onSkipped;

    public virtual void Initialize(Action skipCallback)
    {
        onSkipped += skipCallback;
        this.enabled = true;
    }

    protected void ExecuteAction()
    {
        Disable();
        OnSkipped?.Invoke();
        onSkipped?.Invoke();
    }

    protected virtual void Disable()
    {
        enabled = false;
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
}