﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_HoldToSkip : Canvas_Skip
{
    [ReadOnly] private bool isPressing = false;

    public Image fillImage;
    public Button button;

    public float emptyDuration = .5f;
    public float fillDuration = 3;

    public override void Initialize(Action skipCallback)
    {
        base.Initialize(skipCallback);
        isPressing = false;
        fillImage.fillAmount = 0;
        button.gameObject.SetActive(true);
    }

    public void Button_Down()
    {
        isPressing = true;
    }

    public void Button_Up()
    {
        isPressing = false;
    }

    public void Update()
    {
        if (isPressing && fillImage.fillAmount < 1)
            fillImage.fillAmount += Time.deltaTime / fillDuration;
        else if (!isPressing && fillImage.fillAmount != 0)
            fillImage.fillAmount -= Time.deltaTime / emptyDuration;

        if (fillImage.fillAmount >= 1)
            ExecuteAction();
    }

    public override void Hide()
    {
        fillImage.fillAmount = 0;
    }

    protected override void Disable()
    {
        base.Disable();
        button.gameObject.SetActive(false);
    }
}