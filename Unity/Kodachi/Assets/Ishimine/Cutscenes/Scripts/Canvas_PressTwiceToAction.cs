﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_PressTwiceToAction : Canvas_Action
{
    public float disappearDuration = 2;
    private bool isReady = false;
    public GameObject messageContainer;

    private float currentT = 0;
    public Button button;


    public override void Initialize(Action skipCallback)
    {
        base.Initialize(skipCallback);
        button.gameObject.SetActive(true);
        Hide();
    }

    public void Pressed()
    {
        if(!isReady)
            FirstPress();
        else
            ExecuteAction();
    }

    private void FirstPress()
    {
        isReady = true;
        currentT = disappearDuration;
        messageContainer.SetActive(true);
    }

    private void Update()
    {
        if (isReady)
        {
            currentT -= Time.deltaTime;
            if(currentT <= 0)
                Hide();
        }
    }
    
    public override void Hide()
    {
        base.Hide();
        currentT = 0;
        isReady = false;
        messageContainer.SetActive(false);
    }

    protected override void Disable()
    {
        base.Disable();
        button.gameObject.SetActive(false);
    }
}
