﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using Sirenix.OdinInspector;
using System;

#if UNITY_EDITOR
using UnityEditor.Timeline;
#endif

public class TimelinePlayer : MonoBehaviour, INotificationReceiver
{
    public bool disableOnEnd = true;
    public const string MAIN_SEQUENCE_TAG = "MAIN";
    public const string SEQUENCE_MARKER_TRACK_NAME = "SequenceMarkers";
    public const string GROUP_TRACK_NAME = "_TimelinePlayer";

    public bool useAnimationOffsetMatchFields = true;
    public bool removeStartOffset = false;

    private Canvas_Action currentCanvasAction;

    public Action<bool> OnPlayerInput;

    public static Action<TimelinePlayer> OnStartAny;
    public static Action<TimelinePlayer> OnEndAny;

    public Action OnPlay;
    public Action OnPause;
    public Action OnResume;
    public Action OnStop;

    public Action OnStart;
    public Action OnEnd;

    public enum PlayerInputResponse { None, Skip, Resume}
    public PlayerInputResponse inputResponse;

    [TabGroup("Dependencies")] [SerializeField] private Canvas_Action skipScreenPrefab;
    [TabGroup("Dependencies")] [SerializeField] private AnimatedContainer pauseScreen;
    [TabGroup("Dependencies")] [SerializeField] private AnimatedContainer playingScreen;
    [TabGroup("Dependencies")] public PlayableDirector current;

    public PlayableDirector Current => current;
    [ReadOnly] public bool isPlaying = false;
    [ReadOnly] public bool isPaused = false;

    public bool useBinds = false;
    [ShowIf("useBinds"), SerializeField] private List<BindElement> binds = new List<BindElement>();

    [ShowInInspector] private bool loopNext = true;

    private MarkerSequenceTrack sequenceMarkersTrack;
    private MarkerSequenceTrack SequenceMarkersTrack
    {
        get
        {
            if (sequenceMarkersTrack == null)
            {
                sequenceMarkersTrack = GetTrackOrCreate<MarkerSequenceTrack>(SEQUENCE_MARKER_TRACK_NAME);
                foreach (PlayableBinding output in sequenceMarkersTrack.outputs)
                    if (output.streamName == SEQUENCE_MARKER_TRACK_NAME)
                        Current.SetGenericBinding(output.sourceObject, this);
                sequenceMarkersTrack.SetGroup(TLGroupTrack);
            }
            return sequenceMarkersTrack;
        }
    }

    private GroupTrack tlGroupTrack;
    private GroupTrack TLGroupTrack
    {
        get
        {
            if (tlGroupTrack == null)
                tlGroupTrack = GetRootTrackOrCreate<GroupTrack>(GROUP_TRACK_NAME);
            return tlGroupTrack;
        }
    }

    public List<SequenceMarker> sequenceMarkers;

    [ReadOnly] public TimelineSequence currentSequence;

   public List<TimelineSequence> sequences = new List<TimelineSequence>();

    private void OnValidate()
    {
        if (Application.isPlaying || current == null) return;
        FillBindigs();
        if (useAnimationOffsetMatchFields)
            InitializeAnimationOffset();
    }

    private T GetRootTrackOrCreate<T>(string trackName) where T : TrackAsset, new()
    {
        TimelineAsset timelineAsset = current.playableAsset as TimelineAsset;
        foreach (TrackAsset item in timelineAsset.GetRootTracks())
        {
            if (item is T track && track.name == trackName)
                return track;
        }
        return timelineAsset.CreateTrack<T>(trackName);
    }


    private T GetTrackOrCreate<T>(string trackName) where T : TrackAsset, new()
    {
        TimelineAsset timelineAsset = current.playableAsset as TimelineAsset;
        foreach (TrackAsset item in timelineAsset.GetOutputTracks())
        {
            if (item is T track && track.name == trackName)
                return track;
        }
        return timelineAsset.CreateTrack<T>(trackName);
    }

    [Button]
    public void AddMarker(float time, string name)
    {
        TimelineAsset timelineAsset = current.playableAsset as TimelineAsset;
        timelineAsset.markerTrack.CreateMarker<Marker>((double)time).name = name;
    }

    private void SetCurrent(PlayableDirector playableDirector)
    {
        if (current != null)
            current.stopped -= OnStopped;

        current = playableDirector;
        current.stopped += OnStopped;
    }

    private void OnStopped(PlayableDirector pd)
    {
        Debug.Log($"{pd.name} Stopped");

        if(current.duration == current.time)
            Debug.Log($"{current.name} Finished");
    }

    public void InsertBind(BindElement bindElement)
    {
        int targetIndex = binds.FindIndex(x => x.trackName == bindElement.trackName);
        if (targetIndex == -1)
        {
            binds.Add(bindElement);
            Debug.Log($"Inserted: {bindElement.ToString()}");
        }
        else
            binds[targetIndex] = bindElement;
    }

    #region Bindings
    [Button, ShowIf("useBinds")]
    public void FillBindigs()
    {
        if (!useBinds) return;

        Debug.Log(TLGroupTrack.name);
        Debug.Log(SequenceMarkersTrack);
        SelfBind();

        if (current != null)
        {
            foreach (PlayableBinding item in current.playableAsset.outputs)
            {
                if (binds.Exists(x => x.trackName == item.streamName))
                    continue;
                InsertBind(new BindElement(item.streamName, current.GetGenericBinding(item.sourceObject)));
            }
        }
        else
            Debug.LogWarning("Current is NULL");

        FixAnimatorBindings();
    }

    void FixAnimatorBindings()
    {
        for (int i = 0; i < binds.Count; i++)
        {
            if(binds[i].trackName.Contains("Animator"))
            {
                Animator animator = binds[i].targetObject as Animator;
                if(animator == null)
                {
                    Debug.LogWarning($"CUIDADO! el binding {binds[i].trackName} deberia ser un ANIMATOR");
                    GameObject go = binds[i].targetObject as GameObject;
                    if(go != null)
                    {
                        animator = go.GetComponent<Animator>();
                        if(animator != null)
                        {
                            Debug.LogWarning($"     >Se reemplazado el binding de  {binds[i].trackName} = GameObject:{go.name} a Animator:{animator}");
                            binds[i] = new BindElement(binds[i].trackName, animator);
                        }
                    }
                }
            }
        }
    }


    private void SelfBind()
    {
        if (!binds.Exists(x => x.trackName == SEQUENCE_MARKER_TRACK_NAME))
            InsertBind(new BindElement(SEQUENCE_MARKER_TRACK_NAME, this));

        if (!binds.Exists(x => x.trackName == "Markers"))
            InsertBind(new BindElement("Markers", this));
    }

    public void ApplyBindReferences()
    {
        if (!useBinds) return;

        List<PlayableBinding> playableBindings = new List<PlayableBinding>(Current.playableAsset.outputs);

        SelfBind();

        for (int i = 0; i < playableBindings.Count; i++)
        {
            for (int j = 0; j < binds.Count; j++)
            {
                if (playableBindings[i].streamName == binds[j].trackName)
                {
                    if (binds[j].targetObject == null)
                        Debug.LogWarning($"ERROR 1: El 'targetObject' del bind {binds[j].trackName} es NULL");
                    Current.SetGenericBinding(playableBindings[i].sourceObject, binds[j].targetObject);
                    Debug.Log($"Set: {playableBindings[i].sourceObject} =>  {binds[j].targetObject}");
                }
            }
        }
    }

    #endregion

    #region Player
    [Button]
    public void SetAndPlay(PlayableDirector playableDirector)
    {
        SetCurrent(playableDirector);
        Play();
    }

    [Button, ButtonGroup("1")]
    public void TestPlay()
    {
        Play();
    }
        

    public void Play(Action endCallback = null)
    {
        playingScreen.Open();
        pauseScreen.Hide();


        //Initialization
        if(currentCanvasAction == null && skipScreenPrefab != null)
            currentCanvasAction = Instantiate(skipScreenPrefab.gameObject).GetComponent<Canvas_Action>();

        InitializeSequenceMarkers();
        ApplyBindReferences();

        if (isPlaying || isPaused)
        {
            Debug.LogWarning($"Cant Play() => isPlaying:{isPlaying} isPaused:{isPaused}");
            return;
        }

        if(useAnimationOffsetMatchFields)
            InitializeAnimationOffset();

        Current.Play();

        isPlaying = true;
        isPaused = false;
        loopNext = true;

        OnPlay?.Invoke();

        InitializeSkipCanvas();
    }

    private void InitializeAnimationOffset()
    {
        TimelineAsset timelineAsset = Current.playableAsset as TimelineAsset;
        foreach (TrackAsset track in timelineAsset.GetOutputTracks())
        {
            if(track.name.Contains("Animator") )
            {
                AnimationTrack animationTrack = track as AnimationTrack;
                if(animationTrack.trackOffset ==  TrackOffset.ApplyTransformOffsets)
                {
                    animationTrack.position = Current.transform.position;
                    animationTrack.rotation = Current.transform.rotation;
                }
                foreach (TimelineClip item in animationTrack.GetClips())
                {
                    AnimationPlayableAsset animationPlayableAsset = item.asset as AnimationPlayableAsset;
                    animationPlayableAsset.removeStartOffset = removeStartOffset;
                }
            }
        }
    }

    [Button,ButtonGroup("2")]
    public void Pause()
    {
        Debug.Log($"{this} Paused");
        if (isPaused)
            return;

        Current.playableGraph.GetRootPlayable(0).SetSpeed(0);
        isPlaying = false;
        isPaused = true;

        OnPause?.Invoke();

        playingScreen.Close();
        pauseScreen.Open();

    }

    [Button,ButtonGroup("2")]
    public void Resume()
    {
        if (inputResponse == PlayerInputResponse.Resume)
        {
            currentCanvasAction.Hide();
            loopNext = false;
        }
        if (isPlaying || !isPaused)
            return;

        Current.playableGraph.GetRootPlayable(0).SetSpeed(1);

        isPlaying = true;
        isPaused = false;
        loopNext = true;

        OnResume?.Invoke();

        playingScreen.Open();
        pauseScreen.Close();
    }

    [Button,ButtonGroup("1")]
    public void Stop()
    {
        if (!isPlaying)
        {
            return;
        }

        isPaused = false;
        isPlaying = false;
        Current.Stop();

        OnStop?.Invoke();
    }

    [Button,ButtonGroup("3")]
    public void Previous()
    {
        if (isPlaying || isPaused)
            current.time = currentSequence.GetPrevious(current.time);
    }

    [Button,ButtonGroup("3")]
    public void Skip()
    {
        if (isPlaying || isPaused)
        {
            if(inputResponse == PlayerInputResponse.Skip)
                currentCanvasAction.Hide();

            current.time = currentSequence.GetNext(current.time);
            if(current.time != current.duration)
                InitializeSkipCanvas();
        }
    }

    private void InitializeSkipCanvas()
    {
        switch (inputResponse)
        {
            case PlayerInputResponse.None:
                break;
            case PlayerInputResponse.Skip:
                currentCanvasAction?.Initialize(Skip);
                currentCanvasAction?.Show();
                break;
            case PlayerInputResponse.Resume:
                currentCanvasAction?.Initialize(Resume);
                loopNext = current.time == 0;
                currentCanvasAction?.Hide();
                break;
        }
    }

    #endregion

    public void Signal_PlayerInput(bool value)
    {
        OnPlayerInput?.Invoke(value);
    }

    public void Signal_Pause()
    {
        Pause();
    }

    #region INotificationReceiver

    [Button]
    private void InitializeSequenceMarkers()
    {
        InitializeStartEndMarkers();

        TestMarkersAreValid();
        InitializeSequences();

        current.RebuildGraph();

        #if UNITY_EDITOR
        TimelineEditor.Refresh(RefreshReason.ContentsAddedOrRemoved);
        #endif
    }

    private void TestMarkersAreValid()
    {
        int counter = 0;
        List<IMarker> markers = new List<IMarker>(SequenceMarkersTrack.GetMarkers());
        for (int i = 0; i < markers.Count; i++)
        {
            if (markers[i] is SequenceMarker sequenceMarker)
            {
                switch (sequenceMarker.type)
                {
                    case SequenceMarker.SequenceMarkerType.START:
                        counter++;
                        break;
                    case SequenceMarker.SequenceMarkerType.END:
                        counter--;
                        break;
                }
            }
        }
        if (counter != 0)
            Debug.LogError($"Cuidado, hay un numero desigual de START/END markers {counter}");
    }

    private void InitializeSequences()
    {
        List<IMarker> markers = new  List<IMarker>(SequenceMarkersTrack.GetMarkers());
        int index = -1;

       sequenceMarkers = new List<SequenceMarker>();

        foreach (IMarker item in markers)
        {
            if (item is SequenceMarker sm)
                sequenceMarkers.Add(sm);
        }

        sequenceMarkers.Sort(SequenceMarkerSorter);

        sequences = new List<TimelineSequence>();

        for (int i = 0; i < sequenceMarkers.Count; i++)
        {
                switch (sequenceMarkers[i].Type)
                {
                    case SequenceMarker.SequenceMarkerType.START:
                        sequences.Add(new TimelineSequence(sequenceMarkers[i].name));
                        index++;
                        sequences[index].timestamps.Add(sequenceMarkers[i].time);
                    break;

                    case SequenceMarker.SequenceMarkerType.SKIP:
                        sequences[index].timestamps.Add(sequenceMarkers[i].time);
                        break;
                   
                    case SequenceMarker.SequenceMarkerType.END:
                        sequences[index].timestamps.Add(sequenceMarkers[i].time);
                        index--;
                        if(index >= 0)
                            sequences[index].timestamps.Add(sequenceMarkers[i].time);
                        break;
                    default:
                        break;
                }
        }
    }

    private int SequenceMarkerSorter(SequenceMarker x, SequenceMarker y)
    {
        if (x.time > y.time) return 1;
        else if (x.time < y.time) return -1;
        else return 0;
    }

    private void InitializeStartEndMarkers()
    {
        TimelineAsset timelineAsset = current.playableAsset as TimelineAsset;
        if (timelineAsset is null)
        {
            Debug.LogError("Current is NOT a TimelineAsset");
            return;
        }
        ClearMainSequenceMarkers(timelineAsset);
        AddMainSequenceMarkers(timelineAsset);
    }


    private void AddMainSequenceMarkers(TimelineAsset timelineAsset)
    {
        SequenceMarker start = SequenceMarkersTrack.CreateMarker<SequenceMarker>(0);
        SequenceMarker end = SequenceMarkersTrack.CreateMarker<SequenceMarker>(timelineAsset.duration);
        start.Initialize(MAIN_SEQUENCE_TAG, SequenceMarker.SequenceMarkerType.START);
        end.Initialize(MAIN_SEQUENCE_TAG, SequenceMarker.SequenceMarkerType.END);
    }

    private void ClearMainSequenceMarkers(TimelineAsset timelineAsset)
    {
        List<IMarker> markers = new List<IMarker>(SequenceMarkersTrack.GetMarkers());
        foreach (IMarker item in markers)
        {
            if (item is SequenceMarker sequenceMarker && sequenceMarker != null)
            {
                switch (sequenceMarker.type)
                {
                    case SequenceMarker.SequenceMarkerType.START:
                        if (sequenceMarker.name == MAIN_SEQUENCE_TAG)
                            SequenceMarkersTrack.DeleteMarker(sequenceMarker);
                        break;
                    case SequenceMarker.SequenceMarkerType.END:
                        if (sequenceMarker.name == MAIN_SEQUENCE_TAG)
                            SequenceMarkersTrack.DeleteMarker(sequenceMarker);
                        break;
                }
            }
        }
    }

    public void OnNotify(Playable origin, INotification notification, object context)
    {
        if (notification is SequenceMarker sequenceMarker && sequenceMarker != null)
        {
            Debug.Log(sequenceMarker);
            switch (sequenceMarker.type)
            {
                case SequenceMarker.SequenceMarkerType.START:
                    {
                        currentSequence = sequences.Find(x => x.name == sequenceMarker.name);
                        if (sequenceMarker.name == MAIN_SEQUENCE_TAG)
                        {
                            TimelineStarted(sequenceMarker);
                        }
                    }
                    break;
                case SequenceMarker.SequenceMarkerType.END:
                    {
                        if (sequenceMarker.name == MAIN_SEQUENCE_TAG)
                        {
                            TimelineStoped(sequenceMarker);
                        }
                    }
                    break;
                case SequenceMarker.SequenceMarkerType.SKIP:
                    break;
            }
        }
        else if (notification is PauseMarker pauseMarker)
        {
            Pause();
            if(pauseMarker.resumeUsingPlayerInput && inputResponse == PlayerInputResponse.Resume)
                currentCanvasAction.Show();
        }
        else if (notification is LoopOriginMarker loopOriginMarker)
        {
            if (loopNext)
            {
                if (inputResponse == PlayerInputResponse.Resume)
                    currentCanvasAction.Show();
                current.time = loopOriginMarker.GetDestinyTime();
            }
            else
                loopNext = true;
        }
    }

    private void TimelineStoped(SequenceMarker sequenceMarker)
    {
        SequenceMarkersTrack.DeleteMarker(sequenceMarker);
        Stop();
        currentCanvasAction?.Hide();
        OnEnd?.Invoke();
        OnEndAny?.Invoke(this);
        gameObject.SetActive(!disableOnEnd);
    }

    private void TimelineStarted(SequenceMarker sequenceMarker)
    {
        OnStart?.Invoke();
        OnStartAny?.Invoke(this);
        SequenceMarkersTrack.DeleteMarker(sequenceMarker);
    }
    #endregion
}

[System.Serializable]
public struct BindElement
{
    public string trackName;
    public UnityEngine.Object targetObject;

    public BindElement(string trackName, UnityEngine.Object p) : this()
    {
        this.trackName = trackName;
        targetObject = p;
    }

    public override string ToString()
    {
        return $"Nam:{trackName} Target:{targetObject}";
    }
}