﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Cutscene
{
    public class CustsceneDirector : MonoBehaviour
    {
        private CutsceneAsset cutscene;
        
        public void SetCutscene(CutsceneAsset cutsceneAsset, Action onDone)
        {
            cutscene = cutsceneAsset;
        }

        public void Play()
        {

        }

        public void Pause()
        {

        }

        public void Stop()
        {

        }
    }

    public class CutsceneAsset : ScriptableObject
    {
        public string cutsceneName;
        public List<PlayableSequence> playableSequences;
    }

    public abstract class PlayableSequence
    {
        public Action OnPlay;
        public Action OnPause;
        public Action OnResume;
        public Action OnStop;

        public Action OnEnd;

        public virtual void Play()
        {
            OnPlay?.Invoke();
        }
    }
}


