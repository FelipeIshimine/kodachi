﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ScreenTransitionSystem
{
    public class ScreenTransition : MonoBehaviour
    {
        [ReadOnly] public ScreenTransitionController DefaultTransition = null;

        private static ScreenTransition instance;
        public static ScreenTransition Instance
        {
            get
            {
                return instance;
            }
            set { instance = value; }
        }

        private ScreenTransitionController currentTransition;
        public ScreenTransitionController CurrentTransition
        {
            get
            {
                if (currentTransition == null) SetTransition(DefaultTransition.gameObject);
                return currentTransition;
            }
        }

        public bool OnTransition => currentTransition.OnTransition;

        public static Action OnOutStart { get; set; }

        public static Action OnOutEnd { get; set; }

        public static Action OnInStart { get; set; }

        public static Action OnInEnd { get; set; }

        private IEnumerator waitAndExecuteRutine;

        internal static void SetProgress(float value)
        {
            Instance.CurrentTransition.SetProgressBar(value);
        }


#if UNITY_EDITOR
        private void OnValidate()
        {
            if (DefaultTransition != null &&
                !PrefabUtility.IsPartOfPrefabAsset(DefaultTransition.gameObject))
            {
                DefaultTransition = null;
                Debug.LogWarning("El objeto 'defaultTransition' debe ser un PREFAB");
            }
        }

        public void SetDefault(ScreenTransitionController nDefault)
        {
            DefaultTransition = nDefault;
            Debug.Log($"{this}->SetDefault({nDefault.name})");
            if (Application.isPlaying)
                SetTransition(DefaultTransition.gameObject);
        }
#endif

        private void Awake()
        {
            if (instance != null)
            {
                Debug.LogError($"Singleton Error {this}");
                return;
            }
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        private IEnumerator WaitAndExecute(Action postAction, float duration, Action preTransitionAction = null)
        {
            yield return new WaitForSecondsRealtime(duration);
            preTransitionAction?.Invoke();
            postAction.Invoke();
        }

        public void SetTransition(GameObject screenTransitionPrefab)
        {
            if (currentTransition != null)
            {
                Destroy(currentTransition.gameObject);
            }
            currentTransition = Instantiate(screenTransitionPrefab, null).GetComponent<ScreenTransitionController>();
            currentTransition.transform.localScale = Vector3.one;
            currentTransition.gameObject.name = "_CurrentTransition";
            currentTransition.Initialize();
        }

        public static void OutIn(Action callback = null, float waitBeforeCallback = 0)
        {
            Instance._OutIn(waitBeforeCallback, callback);
        }

        public static void Out(Action callback = null, float waitBeforeCallback = 0)
        {
            Instance._Out(waitBeforeCallback, callback);
        }

        public static void In(Action callback = null, float waitBeforeCallback = 0)
        {
            Instance._In(waitBeforeCallback, callback);
        }

        public void _Out(float postWaitTime = 0, Action callback = null)
        {
            CurrentTransition.Out(postWaitTime, callback);
        }

        public void _In(float postWaitTime = 0, Action callback = null)
        {
            CurrentTransition.In(postWaitTime, callback);
        }

        public void OutIn(Action onOutCallback = null, Action onInCallback = null)
        {
            _OutIn(0, onOutCallback, 0, onInCallback);
        }

        internal void WantAndTransitionOut(float transitionDuration, float waitTime, Action postAction, Action preTransitionAction = null)
        {
            Debug.Log("         WantAndTransitionOut");

            if (waitAndExecuteRutine != null)
                StopCoroutine(waitAndExecuteRutine);

            waitAndExecuteRutine = WaitAndExecute(() => _Out(waitTime, postAction), transitionDuration);
        }

        internal static void InAfterWaiting(float waitTime, Action callback = null)
        {
            Instance.WaitAndTransitionIn(waitTime, 0, callback);
        }

        internal void WaitAndTransitionIn(float waitDuration, float postInWaitTime, Action postAction, Action preTransitionAction = null)
        {
            Debug.Log("         WaitAndTransitionIn");
            if (waitAndExecuteRutine != null)
                StopCoroutine(waitAndExecuteRutine);
            waitAndExecuteRutine = WaitAndExecute(() => _In(postInWaitTime, postAction), waitDuration, preTransitionAction);
            StartCoroutine(waitAndExecuteRutine);
        }

        public void _OutIn(float postWaitTimeOut = 0, Action onOutCallback = null, float waitTimeIn = 0, Action onInCallback = null)
        {
            _Out(postWaitTimeOut,
                () =>
                {
                    onOutCallback?.Invoke();
                    _In(waitTimeIn, onInCallback);
                });
        }
    }
}