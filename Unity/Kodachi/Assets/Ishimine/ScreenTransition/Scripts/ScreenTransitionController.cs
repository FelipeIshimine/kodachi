﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ScreenTransitionSystem
{
    public class ScreenTransitionController : MonoBehaviour
    {
        IEnumerator rutine;

        public float durationIn = .4f;
        public float durationOut = .4f;

        private bool onTransition = false;
        public bool OnTransition => onTransition;

        public Transition transition;
        public UI_Loading loadBar;

        public void Initialize()
        {
            transition.Initialize();
            loadBar.Initialize();
        }

        #region Public

        public void In(float postWaitTime = 0, Action postAction = null)
        {
            if (rutine != null) StopCoroutine(rutine);
            rutine = StepRoutine(
                durationIn,
                transition.StepIn,
                TransitionIn_Started,
                TransitionIn_Done,
                postWaitTime,
                postAction);
            StartCoroutine(rutine);
        }

        public void Out(float postWaitTime = 0, Action postAction = null)
        {
            if (rutine != null) StopCoroutine(rutine);
            rutine = StepRoutine(
                durationIn,
                transition.StepOut,
                TransitionOut_Started,
                TransitionOut_Done,
                postWaitTime,
                postAction);
            StartCoroutine(rutine);
        }

        IEnumerator StepRoutine(float duration, Action<float> stepMethod, Action onStartMethod, Action onEndMethod, float postWaitTime = 0, Action postAction = null)
        {
            onStartMethod?.Invoke();
            yield return null;
            float time = 0;
            do
            {
                time += Time.unscaledDeltaTime;
                stepMethod?.Invoke(time / duration);
                yield return null;
            } while (time < duration);
            yield return new WaitForSecondsRealtime(postWaitTime);
            onEndMethod?.Invoke();
            postAction?.Invoke();
        }
        #endregion
        #region Private
        /// <summary>
        /// Se ejecuta al INICIAR la animacion de ENTRADA
        /// </summary>
        private void TransitionIn_Started()
        {
            onTransition = true;
            ScreenTransition.OnInStart?.Invoke();
            transition.InStarted();
            loadBar.LoadDone();
        }
        /// <summary>
        /// Se ejecuta al TERMINAR la animacion de ENTRADA
        /// </summary>
        private void TransitionIn_Done()
        {
            onTransition = false;
            ScreenTransition.OnInEnd?.Invoke();
            transition.InDone();
            loadBar.gameObject.SetActive(false);
        }


        /// <summary>
        /// Se ejecuta al INICIAR la animacion de SALIDA
        /// </summary>
        private void TransitionOut_Started()
        {
            onTransition = true;
            ScreenTransition.OnOutStart?.Invoke();
            transition.OutStart();
            loadBar.Initialize();
            loadBar.gameObject.SetActive(true);
        }

        /// <summary>
        /// Se ejecuta al TERMINAR la animacion de SALIDA
        /// </summary>
        private void TransitionOut_Done()
        {
            onTransition = false;
            ScreenTransition.OnOutEnd?.Invoke();
            transition.OutDone();
            loadBar.LoadStarted();
        }
        #endregion
        public void SetProgressBar(float value) => loadBar.SetProgress(value);

    }
}