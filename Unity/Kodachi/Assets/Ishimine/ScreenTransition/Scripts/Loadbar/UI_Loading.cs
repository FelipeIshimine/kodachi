﻿using UnityEngine;

namespace ScreenTransitionSystem
{
    public abstract class UI_Loading : MonoBehaviour
    {
        public abstract void Initialize();
        public abstract void SetProgress(float progress);
        public abstract void LoadStarted();
        public abstract void LoadDone();
        public abstract void SetAlpha(float value);

    }
}
