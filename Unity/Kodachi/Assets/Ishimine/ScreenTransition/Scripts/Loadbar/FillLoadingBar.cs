﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ScreenTransitionSystem
{
    public class FillLoadingBar : UI_Loading
    {
        [SerializeField] private Image progressBarImg = null;

        public float smooth = .15f;
        private float targetValue;
        private float currentValue;
        private float vel;

        public override void Initialize()
        {
            progressBarImg.gameObject.SetActive(false);
            ResetBar();
        }

        public void ResetBar()
        {
            targetValue = 0;
            currentValue = 0;
            progressBarImg.fillAmount = 0;
        }

        public override void SetProgress(float value)
        {
            targetValue = value;
        }

        public override void SetAlpha(float alphaValue)
        {
            progressBarImg.color = new Color(1, 1, 1, alphaValue);
        }

        public void Update()
        {
            if (targetValue != currentValue)
            {
                currentValue = Mathf.SmoothDamp(currentValue, targetValue, ref vel, smooth, float.MaxValue, Time.deltaTime);
                progressBarImg.fillAmount = currentValue;
            }
        }

        public override void LoadStarted()
        {
            progressBarImg.gameObject.SetActive(true);
        }

        public override void LoadDone()
        {
        }
    }
}