﻿using UnityEngine;

namespace ScreenTransitionSystem
{
    public abstract class Transition : MonoBehaviour
    {
        public abstract void Initialize();
        public abstract void StepIn(float progress);
        public abstract void StepOut(float progress);

        public abstract void InDone();
        public abstract void InStarted();
        public abstract void OutDone();
        public abstract void OutStart();
    }
}