﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ScreenTransitionSystem
{
    public class FadeTransition : Transition
    {
        [SerializeField] private Color inColor = Color.clear;
        [SerializeField] private Color outColor = Color.black;
        [SerializeField] private Image img = null;

        [SerializeField] AnimationCurve curveIn = AnimationCurve.EaseInOut(0, 0, 1, 1);
        [SerializeField] AnimationCurve curveOut = AnimationCurve.EaseInOut(0, 0, 1, 1);

        public UI_Loading UI_Loading;

        private Color startColor = Color.clear;

        public override void Initialize()
        {
            img.gameObject.SetActive(false);
        }

        public override void StepIn(float progress)
        {
            img.color = Color.Lerp(startColor, inColor, curveIn.Evaluate(progress));
            UI_Loading?.SetAlpha(1 - curveIn.Evaluate(progress));
        }

        public override void StepOut(float progress)
        {
            img.color = Color.Lerp(startColor, outColor, curveOut.Evaluate(progress));
            UI_Loading?.SetAlpha(curveOut.Evaluate(progress));
        }

        public override void InStarted()
        {
            startColor = img.color;
            img.raycastTarget = true;
        }

        public override void OutStart()
        {
            img.raycastTarget = true;
            startColor = img.color;
            img.gameObject.SetActive(true);
        }

        public override void InDone()
        {
            img.raycastTarget = false;
        }

        public override void OutDone()
        {
            img.raycastTarget = false;
        }

    }
}
