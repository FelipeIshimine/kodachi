﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ScreenTransitionSystem
{
    [CustomEditor(typeof(ScreenTransition))]
    public class ScreenTransitionEditor : Editor
    {
        private string[] options;
        private int selected = 0;
        private int oldSelected = 0;
        ScreenTransition screenTransition;
        ScreenTransitionController[] transitionOptions;

        private void OnEnable()
        {
            screenTransition = target as ScreenTransition;
            CollectTransitions();

            if (options.Length == 0)
                return;

            for (int i = 0; i < transitionOptions.Length; i++)
            {
                Debug.Log($"{i}: {transitionOptions[i].name}");
                options[i] = transitionOptions[i].name;
                if (transitionOptions[i] == screenTransition.DefaultTransition)
                    oldSelected = i;
            }
        }

        private void CollectTransitions()
        {
            GameObject[] foundAssets = UnityUtils.GetAssetsAtPath<GameObject>("/Ishimine/ScreenTransition/Transitions/");
            List<ScreenTransitionController> uI_ScreenTransitions = new List<ScreenTransitionController>();
            Debug.Log("");
            for (int i = 0; i < foundAssets.Length; i++)
            {
                ScreenTransitionController fTransition = foundAssets[i].GetComponent<ScreenTransitionController>();
                if (fTransition != null)
                    uI_ScreenTransitions.Add(fTransition);
            }

            transitionOptions = uI_ScreenTransitions.ToArray();
            options = new string[transitionOptions.Length];

        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (options.Length == 0)
            {
                EditorGUILayout.Popup(oldSelected, new string[1] { "No Transition Found" });
                return;
            }

            selected = EditorGUILayout.Popup(oldSelected, options);
            if (oldSelected != selected)
            {
                screenTransition.SetDefault(transitionOptions[selected]);
                EditorUtility.SetDirty(target);
                oldSelected = selected;
            }


        }
    }



}