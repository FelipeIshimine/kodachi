﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TypingText : MonoBehaviour
{
    public Text txt;

    public void SetText(string nText)
    {
        txt.text = nText;
    }

    public void SetColor(Color c)
    {
        txt.color = c;
    }

    private void OnValidate()
    {
        if (txt == null)
            txt = GetComponent<Text>();
    }
}
