﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class TypingTextTrackMixer : PlayableBehaviour
{
    private string accumulativeText = string.Empty;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (!(playerData is TypingText text)) return;

        string currentText = "";
        Color currentColor = Color.white;

        int inputCount = playable.GetInputCount();

        string previousFullText = string.Empty;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            if(inputWeight > 0)
            {
                ScriptPlayable<TypingTextBehaviour> inputPlayable = (ScriptPlayable<TypingTextBehaviour>)playable.GetInput(i);
                TypingTextBehaviour input = inputPlayable.GetBehaviour();

                if (previousFullText == string.Empty)
                    previousFullText = input.GetFullText();
                else
                    input.previousText = previousFullText;
               
                currentColor = input.color; 
                currentText = input.GetText(inputWeight);
            }
        }

        text.SetText(currentText);
        text.SetColor(currentColor);
    }
}
