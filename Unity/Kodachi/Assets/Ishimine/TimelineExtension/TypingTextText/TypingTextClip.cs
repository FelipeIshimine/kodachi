﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TypingTextClip : PlayableAsset
{
    public bool isBold = false;
    public bool isItalics = false;
    public Color color = Color.white;


    [TextArea(3,20)]public string currentText;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<TypingTextBehaviour>.Create(graph);
        TypingTextBehaviour typingTextBehaviour = playable.GetBehaviour();

        typingTextBehaviour.isBold = isBold;
        typingTextBehaviour.isItalics = isItalics;
        typingTextBehaviour.color = color;
        typingTextBehaviour.currentText = currentText;

        return playable;
    }
}
