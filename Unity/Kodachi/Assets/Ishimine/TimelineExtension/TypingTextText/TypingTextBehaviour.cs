﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using System;

public class TypingTextBehaviour : PlayableBehaviour
{
    public bool isBold = false;
    public bool isItalics = false;
    public Color color = Color.white;
    public string previousText;
    public string currentText;

    internal string GetText(float inputWeight)
    {
        string cText = (currentText.Substring(0, Mathf.FloorToInt(currentText.Length * inputWeight)));

        if (isBold)
            cText = $"<b>{cText}</b>";
        
        if (isItalics)
            cText = $"<i>{cText}</i>";

        cText = $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>{cText}</color>";

        return $"{previousText}{cText}"; 
    }

    internal string GetFullText()
    {
        return GetText(1);
    }
}
