﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

[TrackBindingType(typeof(TypingText))]
[TrackClipType(typeof(TypingTextClip))]
public class TypingTextTrack : TrackAsset
{

    public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
    {
#if UNITY_EDITOR
        TypingText trackBinding = director.GetGenericBinding(this) as TypingText;
        if (trackBinding == null)
            return;

        var serializedObject = new UnityEditor.SerializedObject(trackBinding);
        var iterator = serializedObject.GetIterator();
        while (iterator.NextVisible(true))
        {
            if (iterator.hasVisibleChildren)
                continue;

            driver.AddFromName<TypingText>(trackBinding.gameObject, iterator.propertyPath);
        }
#endif
        base.GatherProperties(director, driver);
    }


    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        foreach (TimelineClip clip in m_Clips)
        {
            TypingTextClip mouthClip = clip.asset as TypingTextClip;
            clip.displayName = mouthClip.currentText;
        }
        return ScriptPlayable<TypingTextTrackMixer>.Create(graph, inputCount);
    }
}
