﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class SubtitleTextTrackMixer : PlayableBehaviour
{
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {

        if (!(playerData is BaseTimelineSubtitle text)) return;

        string currentText = "";
        float currentWight = 0f;
        Color currentColor = Color.white;

        int inputCount = playable.GetInputCount();

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            if(inputWeight > 0)
            {
                ScriptPlayable<SubtitleTextBehaviour> inputPlayable = (ScriptPlayable<SubtitleTextBehaviour>)playable.GetInput(i);
                SubtitleTextBehaviour input = inputPlayable.GetBehaviour();
                currentText = input.subtitleText;
                currentWight = inputWeight;
                currentColor = input.color;
            }
        }

        text.SetText(currentText);
        text.SetColor(currentColor);
        text.SetWeight(currentWight);
    }
}
