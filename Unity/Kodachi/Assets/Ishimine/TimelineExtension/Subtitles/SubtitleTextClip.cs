﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SubtitleTextClip : PlayableAsset
{
    public string subtitleText;
    public Color color;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<SubtitleTextBehaviour>.Create(graph);
        SubtitleTextBehaviour subtitleBehaviour = playable.GetBehaviour();
        subtitleBehaviour.subtitleText = subtitleText;
        subtitleBehaviour.color = color;

        return playable;
    }
}
