﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class SubtitleTextBehaviour : PlayableBehaviour
{
    public string subtitleText;
    public Color color;
}
