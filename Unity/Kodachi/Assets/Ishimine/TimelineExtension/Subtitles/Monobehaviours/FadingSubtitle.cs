﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadingSubtitle : BaseTimelineSubtitle
{
    public Text txt;
    private Color color;

    public override void SetText(string text)
    {
        txt.text = text;
    }

    public override void SetWeight(float value)
    {
        txt.color = new Color(color.r, color.g, color.b,value);
    }

    public override void SetColor(Color c)
    {
        color = c;
    }
}
