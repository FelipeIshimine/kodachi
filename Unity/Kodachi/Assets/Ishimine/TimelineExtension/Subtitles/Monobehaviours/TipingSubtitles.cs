﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipingSubtitles : BaseTimelineSubtitle
{
    public Text text;
    private string currentText = string.Empty;

    public override void SetColor(Color color)
    {
        text.color = color;
    }

    public override void SetText(string text)
    {
        currentText = text;
    }

    public override void SetWeight(float value)
    {
        if (currentText == null)
            currentText = string.Empty;

        text.text = currentText.Substring(0, Mathf.FloorToInt(currentText.Length * value));
    }
}
