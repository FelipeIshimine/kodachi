﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

[TrackBindingType(typeof(BaseTimelineSubtitle))]
[TrackClipType(typeof(SubtitleTextClip))]
public class SubtitleTextTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<SubtitleTextTrackMixer>.Create(graph, inputCount);
    }
}
