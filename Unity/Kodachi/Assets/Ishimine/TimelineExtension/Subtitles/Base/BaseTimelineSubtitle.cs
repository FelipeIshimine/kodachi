﻿using UnityEngine;

public abstract class BaseTimelineSubtitle : MonoBehaviour
{
    public abstract void SetWeight(float value);
    public abstract void SetText(string text);
    public abstract void SetColor(Color color);
}