﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackBindingType(typeof(TimelinePlayer))]
[TrackColor(230f / 255, 140f / 255f, 100f / 255f)]
public class MarkerSequenceTrack : MarkerTrack
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        foreach (LoopDestinyMarker marker in GetMarkers())
            marker.name = "D:" + marker.time.ToString($"0.00");
        return base.CreateTrackMixer(graph, go, inputCount);
    }
}
