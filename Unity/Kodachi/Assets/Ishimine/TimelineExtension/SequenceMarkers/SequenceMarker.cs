﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class SequenceMarker : Marker, INotification, INotificationOptionProvider
{
    [SerializeField] public SequenceMarkerType type;

    public SequenceMarkerType Type => type;

    public PropertyName id => new PropertyName();

    [SerializeField] private bool retroactive = false;
    [SerializeField] private bool emitOnce = true;

    public NotificationFlags flags =>
        (retroactive ? NotificationFlags.Retroactive : default) |
        (emitOnce ? NotificationFlags.TriggerOnce : default);

    public enum SequenceMarkerType
    {
        SKIP,
        START,
        END,
    }

    public override string ToString()
    {
        return $"{Type.ToString()} => {name} ";
    }

    internal void Initialize(string name, SequenceMarkerType type)
    {
        this.name = name;
        this.type = type;
    }
}