﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using System;

public class AnimatedContainerBehaviour : PlayableBehaviour
{
    public Color color = Color.white;
    public AnimationCurve movementCurve = AnimationCurve.EaseInOut(0,0,1,1);
    public AnimationCurve scaleCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
}
