﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class AnimatedContainerClip : PlayableAsset
{
    public Color color = Color.white;
    public AnimationCurve movementCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public AnimationCurve scaleCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AnimatedContainerBehaviour>.Create(graph);
        AnimatedContainerBehaviour animatedContainerBehaviour = playable.GetBehaviour();

        animatedContainerBehaviour.color = color;
        animatedContainerBehaviour.movementCurve = movementCurve;
        animatedContainerBehaviour.scaleCurve = scaleCurve;

        return playable;
    }
}
