﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class AnimatedContainerTrackMixer : PlayableBehaviour
{
    private Color previousColor = Color.white;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (!(playerData is AnimatedContainer animatedContainer)) return;

        Color currentColor = Color.white;
        float currentFill = 0;
        bool first = true;
        int inputCount = playable.GetInputCount();
        AnimationCurve movementCurve = AnimationCurve.Linear(0, 0, 1, 1);
        AnimationCurve scaleCurve = AnimationCurve.Linear(0, 0, 1, 1);

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            if(inputWeight > 0)
            {
                ScriptPlayable<AnimatedContainerBehaviour> inputPlayable = (ScriptPlayable<AnimatedContainerBehaviour>)playable.GetInput(i);
                AnimatedContainerBehaviour input = inputPlayable.GetBehaviour();

                currentFill = inputWeight;

                if (first)
                {
                    first = false;
                    previousColor = input.color;
                }
                else
                {
                    currentColor = Color.Lerp(previousColor, input.color, currentFill);
                }
                movementCurve = input.movementCurve;
                scaleCurve = input.scaleCurve;
            }
        }

        animatedContainer.SetColor(currentColor);
        animatedContainer.SetFill(currentFill, movementCurve, scaleCurve);
    }
}
