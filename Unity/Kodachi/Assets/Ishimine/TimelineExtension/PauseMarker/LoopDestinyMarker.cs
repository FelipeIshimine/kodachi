﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[CustomStyle("LoopPauseMarker_Destiny")]
public class LoopDestinyMarker : Marker, INotification, INotificationOptionProvider
{
    public PropertyName id => new PropertyName();

    [SerializeField] private bool retroactive = false;
    [SerializeField] private bool emitOnce = false;


    void OnValidate()
    {
        this.hideFlags &= ~HideFlags.HideInHierarchy;
        name = $"D: " + time.ToString("0.00");
    }

    void OnInitialize()
    {
        this.hideFlags &= ~HideFlags.HideInHierarchy;
        name = $"D: " + time.ToString("0.00");
    }

    public NotificationFlags flags =>
        (retroactive ? NotificationFlags.Retroactive : default) |
        (emitOnce ? NotificationFlags.TriggerOnce : default);
}