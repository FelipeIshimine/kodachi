﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[CustomStyle("SkipMarker")]
public class SkipMarker : Marker, INotification, INotificationOptionProvider
{
    public PropertyName id => new PropertyName();

    [SerializeField] private bool retroactive = false;
    [SerializeField] private bool emitOnce = true;

    public NotificationFlags flags =>
        (retroactive ? NotificationFlags.Retroactive : default) |
        (emitOnce ? NotificationFlags.TriggerOnce : default);
}