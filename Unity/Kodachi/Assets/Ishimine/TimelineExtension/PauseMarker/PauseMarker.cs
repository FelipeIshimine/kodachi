﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[CustomStyle("PauseMarker")]
public class PauseMarker : Marker, INotification, INotificationOptionProvider
{
    public PropertyName id => new PropertyName();

    public bool resumeUsingPlayerInput = true;

    [SerializeField] private bool retroactive = false;
    [SerializeField] private bool emitOnce = false;

    public NotificationFlags flags =>
        (retroactive ? NotificationFlags.Retroactive : default) |
        (emitOnce ? NotificationFlags.TriggerOnce : default);
}