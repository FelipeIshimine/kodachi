﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Sirenix.OdinInspector;

[CustomStyle("LoopPauseMarker_Origin")]
public class LoopOriginMarker : Marker, INotification, INotificationOptionProvider
{
    public PropertyName id => new PropertyName();

    [SerializeField] private bool retroactive = false;
    [SerializeField] private bool emitOnce = false;

    public Marker destiny;

    public NotificationFlags flags =>
        (retroactive ? NotificationFlags.Retroactive : default) |
        (emitOnce ? NotificationFlags.TriggerOnce : default);

    public double GetDestinyTime()
    {
        return destiny.time;
    }

}