﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class PauseMarkerReceiver : MonoBehaviour, INotificationReceiver
{
    [SerializeField] private UnitySequenceMarkerEvent OnNotification = null;

    public void OnNotify(Playable origin, INotification notification, object context)
    {
        if(notification is SequenceMarker sequenceMarker && sequenceMarker != null)
        {
            OnNotification?.Invoke(sequenceMarker);
        }
    }
}

[System.Serializable]
public class UnityPauseMarkerEvent : UnityEvent<PauseMarker>
{

}
