﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class CinemachineTargetGroupRadar : MonoBehaviour
{
    [SerializeField] private CinemachineTargetGroup targetGroup;

    private void Awake()
    {
        if(targetGroup == null)
            targetGroup = GetComponent<CinemachineTargetGroup>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CinemachineTarget cinemachineTarget = collision.attachedRigidbody.GetComponent<CinemachineTarget>();
        if (cinemachineTarget != null) Add(cinemachineTarget);
    }

    public void Add(CinemachineTarget cinemachineTarget)
    {
        targetGroup.AddMember(cinemachineTarget);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CinemachineTarget cinemachineTarget = collision.attachedRigidbody.GetComponent<CinemachineTarget>();
        if (cinemachineTarget != null) Remove(cinemachineTarget);
    }

    public void Remove(CinemachineTarget cinemachineTarget)
    {
        targetGroup.RemoveMember(cinemachineTarget);
    }

    internal void Clear()
    {
        targetGroup.transform.SetParent(transform);
    }

    private void OnDestroy()
    {
        Destroy(targetGroup.gameObject);
    }

    internal void Initialize()
    {
        targetGroup.transform.SetParent(null);
    }
}
