﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CinemachineShake : MonoBehaviour
{
    public CinemachineCameraOffset cinemachineCameraOffset;
    public ShakeSettings shakeSettings;
    public IEnumerator rutine;

    [Button]
    public void Shake()
    {
        Shake(shakeSettings);
    }

    public void Shake(ShakeSettings shakeSettings)
    {
        Debug.Log("Shake");
        if (rutine != null) StopCoroutine(rutine);
        rutine = ShakeRutine(shakeSettings);
        StartCoroutine(rutine);
    }

    IEnumerator ShakeRutine(ShakeSettings shakeSettings)
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / shakeSettings.duration;
            cinemachineCameraOffset.m_Offset.Set(
                Mathf.PerlinNoise(t * shakeSettings.turbulence.x, 0) * shakeSettings.intensity.x,
                Mathf.PerlinNoise(t * shakeSettings.turbulence.y, 1) * shakeSettings.intensity.y,
                Mathf.PerlinNoise(t * shakeSettings.turbulence.z, 2) * shakeSettings.intensity.z);
            yield return null;
        } while (t<1);
        cinemachineCameraOffset.m_Offset.Set(0, 0, 0);
    }
}

[System.Serializable]
public struct ShakeSettings
{
    public Vector3 intensity;
    public Vector3 turbulence;
    public float duration;

    public ShakeSettings(Vector2 intensity, Vector2 turbulence, float duration)
    {
        this.intensity = intensity;
        this.turbulence = turbulence;
        this.duration = duration;
    }
}