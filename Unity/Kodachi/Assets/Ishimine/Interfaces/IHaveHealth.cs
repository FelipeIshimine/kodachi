﻿public interface IHaveHealth
{
    int Current { get; }
    int Max { get; }
    int Damage(int ammount);
    int Heal(int ammount);
}