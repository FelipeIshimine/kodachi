﻿using UnityEngine;

[System.Serializable]
public class WindSettings
{
    public float force = 3;
    public float forceRange = 3;
    public float forceCycleSpeed = 5;

    public Vector2 direction = Vector2.right;
    public Vector2 directionRange = Vector2.one;
    public Vector2 directionCycleSpeed = Vector2.one;

    public WindSettings()
    {
        force = 3;
        forceRange = 3;
        
    }

    public WindSettings(float force, Vector2 direction)
    {
        this.force = force;
        this.direction = direction;
    }

    public WindSettings(float force, float forceRange, AnimationCurve cycleForceVariationCurve, float forceCycleDuration, Vector2 direction, AnimationCurve cycleDirectionVariationCurveX, AnimationCurve cycleDirectionVariationCurveY, float directionCycleDuration)
    {
        this.force = force;
        this.forceRange = forceRange;
        this.direction = direction;
    }
}
