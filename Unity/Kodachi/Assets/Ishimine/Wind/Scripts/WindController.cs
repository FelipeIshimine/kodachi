﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WindController 
{
    [SerializeField] private static WindSettings Settings { get; set; }

    private static WindUpdate windUpdate;

    private static Vector2 current;
    public static Vector2 Current => current;

    private static bool initialized = false;

    private static float forceTime = 0;
    private static Vector2 directionTime;

    public static void Initialize(WindSettings settings)
    {
        Settings = settings;
        if (initialized)
        {
            Debug.LogWarning("WindController already initialized");
            return;
        }

        initialized = true;
        windUpdate = new GameObject().AddComponent<WindUpdate>();
        UnityEngine.Object.DontDestroyOnLoad(windUpdate);
        windUpdate.OnFixedUpdate += FixedUpdate;
    }

    private static void FixedUpdate()
    {
        forceTime +=  Time.fixedDeltaTime * Settings.forceCycleSpeed;
        directionTime += Time.fixedDeltaTime * Settings.directionCycleSpeed;

        current = Settings.direction + new Vector2(Mathf.PerlinNoise(directionTime.x,0) * Settings.directionRange.x, Mathf.PerlinNoise(0, directionTime.y) * Settings.directionRange.y);
        current *= Settings.force + Mathf.PerlinNoise(forceTime, forceTime) * Settings.forceRange;
    }
   
}
