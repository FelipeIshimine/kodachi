﻿using System;
using UnityEngine;

public class WindUpdate : MonoBehaviour
{
    public Action OnFixedUpdate;
    private void FixedUpdate()
    {
        OnFixedUpdate?.Invoke();
    }

    private void OnDestroy()
    {
        OnFixedUpdate = null;
    }
}