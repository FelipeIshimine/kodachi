﻿using UnityEngine;

[CreateAssetMenu]
public class WindSettingsSO : ScriptableObject
{
    public WindSettings Value;
}
