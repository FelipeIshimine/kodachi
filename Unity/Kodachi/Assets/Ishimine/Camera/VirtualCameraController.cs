﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VirtualCameraController 
{
    public static CinemachineVirtualCamera DefaultVirtualCamera;
    public static CinemachineVirtualCamera CurrentVirtualCamera;
    public const int ActiveCameraPriority = 100;

    private static int oldPriority;
    private static CinemachineVirtualCamera oldCamera;

    public static void SetCamera(CinemachineVirtualCamera nVCam)
    {
        if (oldCamera != null)
            oldCamera.Priority = oldPriority;

        oldCamera = CurrentVirtualCamera;
        CurrentVirtualCamera = nVCam;

        if (CurrentVirtualCamera != null)
        {
            oldPriority = CurrentVirtualCamera.Priority;
            CurrentVirtualCamera.Priority = ActiveCameraPriority;
        }
    }

    public static void DefaultCamera()
    {
        SetCamera(DefaultVirtualCamera);
    }

}

public abstract class Singleton<T> where T: new()
{
    public static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
                instance = new T();
            return instance;
        }
    }
}