﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedTextFill : MonoBehaviour
{
    public Text targetText;

    private Action OnDoneCallback;

    IEnumerator rutine;
    public enum DeltaTimeType { deltaTime, fixedDeltaTime, unscaledDeltaTime, fixedUnscaledDeltaTime }
    public DeltaTimeType TimeType;

    public float charectersPerSecond = 5;

    delegate float GetFloat();
    GetFloat GetDelta;

    private bool skip = false; 
    public bool IsAnimationRunning { get; private set; }
    CanvasScaler canvasScaler;
    private void Awake()
    {
        canvasScaler = GetComponentInParent<CanvasScaler>();
        switch (TimeType)
        {
            case DeltaTimeType.deltaTime:
                GetDelta = ()=> Time.deltaTime;
                break;
            case DeltaTimeType.fixedDeltaTime:
                GetDelta = ()=> Time.fixedDeltaTime;
                break;
            case DeltaTimeType.unscaledDeltaTime:
                GetDelta = ()=> Time.unscaledDeltaTime;
                break;
            case DeltaTimeType.fixedUnscaledDeltaTime:
                GetDelta = ()=> Time.fixedUnscaledDeltaTime;
                break;
            default:
                break;
        }
    }

    internal void Clear()
    {
        targetText.text = string.Empty;
    }

    internal void Skip()
    {
        skip = true;
    }

    public void StartTextAnimation(string text, float endWait, Action OnDoneCallback)
    {
        this.OnDoneCallback = OnDoneCallback;
        if (rutine != null) StopCoroutine(rutine);
        rutine = TextFillRutine(text, endWait);
        StartCoroutine(rutine);
    }

    private IEnumerator TextFillRutine(string targetText, float endWait)
    {
        IsAnimationRunning = true;
        float t = 0;
        float totalDuration = targetText.Length / charectersPerSecond;
        string currentText = string.Empty;
        skip = false;
        this.targetText.text = string.Empty;
        do
        {
            t += GetDelta.Invoke() / totalDuration;
            int characterCount = Mathf.RoundToInt(targetText.Length * t);
            this.targetText.text = targetText.Substring(0, characterCount) + new string(' ', Mathf.Max(0,targetText.Length - characterCount));
            yield return null;
        } while (t < 1 && !skip);
        this.targetText.text = targetText;

        skip = false;
        t = 0;
        while (t < 1 && !skip)
        {
            t += GetDelta.Invoke() / endWait;
            yield return null;
        }
        OnDoneCallback?.Invoke();
        IsAnimationRunning = false;
    }


    private void OnDestroy()
    {
        OnDoneCallback = null;
    }

    private void OnDisable()
    {
        targetText.text = string.Empty;
    }
}
