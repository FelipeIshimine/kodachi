﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AnimatedContainer : MonoBehaviour
{
    public UnityColorEvent unityColorEvent;
    public enum Direction { Right, Left, Up, Down }
    public enum TimeType { scaled, unscaled }

    public float durationIn = .3f;
   
    public float durationOut = .3f;
    IEnumerator rutine;
    RectTransform parent;
    RectTransform Parent
    {
        get
        {
            if (parent == null)
                parent = transform.parent as RectTransform; 
            return parent;
        }
    }

    RectTransform rectTransform;
    RectTransform RectTransform
    {
        get
        {
            if (rectTransform == null)
                rectTransform = transform as RectTransform;
            return rectTransform;
        }
    }

    public TimeType timeType = TimeType.unscaled;

    [TabGroup("Alpha")] public bool useAlpha = true;
    [EnableIf("useAlpha"), TabGroup("Alpha")] public CanvasGroup canvasGroup;
    [EnableIf("useAlpha"), TabGroup("Alpha")] public AnimationCurve alphaCurveIn = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [EnableIf("useAlpha"), TabGroup("Alpha")] public AnimationCurve alphaCurveOut = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [TabGroup("Scale")] public bool useScale = true;
    [EnableIf("useScale"), TabGroup("Scale")] public AnimationCurve curveInScale = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [EnableIf("useScale"), TabGroup("Scale")] public Vector2 targetScale = new Vector2(0, 1);
    [EnableIf("useScale"), TabGroup("Scale")] public AnimationCurve curveOutScale = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [TabGroup("Movement")] public bool useMovement = true;
    [EnableIf("useMovement"), TabGroup("Movement")] public Direction direction = Direction.Down;
    [EnableIf("useMovement"), TabGroup("Movement")] public AnimationCurve curveIn = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [EnableIf("useMovement"), TabGroup("Movement")] public AnimationCurve curveOut = AnimationCurve.EaseInOut(0, 0, 1, 1);
    private Vector2 targetPosition;

    private bool initialized = false;


    private void Awake()
    {
        Initialize();
    }

    private void OnValidate()
    {
        if(useAlpha && canvasGroup == null)
        {
            canvasGroup = GetComponent<CanvasGroup>();
            if(canvasGroup == null)
                canvasGroup = gameObject.AddComponent<CanvasGroup>();
        }
    }
    public void Initialize()
    {
        if (initialized) return;
        initialized = true;
        if (RectTransform == null)
            rectTransform = transform as RectTransform;

        parent = transform.parent as RectTransform;

        SetDirection(direction);

        RectTransform.anchoredPosition = Vector3.zero;
        RectTransform.localScale = Vector3.one;
            
        Hide();
    }


    public void SetDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.Right:
                targetPosition = Vector2.right * Parent.rect.width;
                break;
            case Direction.Left:
                targetPosition = Vector2.left * Parent.rect.width;
                break;
            case Direction.Up:
                targetPosition = Vector2.up * Parent.rect.height;
                break;
            case Direction.Down:
                targetPosition = Vector2.down * Parent.rect.height;
                break;
            default:
                break;
        }
    }

    public Vector2 GetTargetPosition()
    {
        switch (direction)
        {
            case Direction.Right:
                return targetPosition = Vector2.right * Parent.rect.width;
            case Direction.Left:
                return targetPosition = Vector2.left * Parent.rect.width;
            case Direction.Up:
                return targetPosition = Vector2.up * Parent.rect.height;
            case Direction.Down:
                return targetPosition = Vector2.down * Parent.rect.height;
            default:
                break;
        }
        return targetPosition = Vector2.zero;
    }


    [Button]
    public void Close()
    {
        Close(null);
    }

    public void Close(Action postAction = null)
    {
        if (rutine != null) StopCoroutine(rutine);
        rutine = CloseRutine(postAction);
        StartCoroutine(rutine);
    }

    [Button]
    public void Open()
    {
        Open(null);
    }

    public void Open(Action postAction)
    {
        gameObject.SetActive(true);
        if (rutine != null) StopCoroutine(rutine);
        rutine = OpenRutine(postAction);
        StartCoroutine(rutine);
    }

    [Button]
    public void Hide()
    {
        if(useMovement)
            RectTransform.anchoredPosition = targetPosition;

        if(useScale)
            RectTransform.localScale = targetScale;
    }

    [Button]
    public void Show()
    {
        gameObject.SetActive(true);
        RectTransform.anchoredPosition = Vector3.zero;
        SetFill(1, curveIn, curveInScale);
    }

    private IEnumerator CloseRutine(Action postAction = null)
    {
        yield return AnimationRutine(durationOut, targetPosition, curveOut, targetScale, curveOutScale, 0, alphaCurveOut, postAction);
    }

    private IEnumerator OpenRutine(Action postAction = null)
    {
        yield return AnimationRutine(durationIn, Vector2.zero, curveIn, Vector2.one, curveInScale, 1, alphaCurveIn, postAction);
    }

    private IEnumerator AnimationRutine(float duration,
        Vector2 targetPosition, AnimationCurve movementCurve,
        Vector2 targetScale, AnimationCurve scaleCurve,
        float targetAlpha, AnimationCurve alphaCurve,
        Action postAction)
    {
        float t = 0;
        Vector2 startPosition = rectTransform.anchoredPosition;
        Vector2 startScale = rectTransform.localScale;
        float startAlpha = 1 - targetAlpha;

        Action step = null;

        if (useMovement) step += () => rectTransform.anchoredPosition = Vector2.Lerp(startPosition, targetPosition, movementCurve.Evaluate(t));
        if (useScale) step += () => rectTransform.localScale = Vector2.Lerp(startScale, targetScale, scaleCurve.Evaluate(t));
        if (useAlpha) step += () => canvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, alphaCurve.Evaluate(t));

        do
        {
            t += ((timeType == TimeType.scaled) ? Time.deltaTime : Time.unscaledDeltaTime) / duration;
            /*if(useMovement) rectTransform.anchoredPosition = Vector2.Lerp(startPosition, targetPosition, movementCurve.Evaluate(t));
            if(useScale) rectTransform.localScale = Vector2.Lerp(startScale, targetScale, scaleCurve.Evaluate(t));
            if (useAlpha) canvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, alphaCurve.Evaluate(t));*/
            step?.Invoke();
            yield return null;
        } while (t < 1);
        postAction?.Invoke();
    }

    [Button]
    internal void SetFill(float currentFill, AnimationCurve movementCurve, AnimationCurve scaleCurve)
    {
        Vector2 startPosition = GetTargetPosition();
        if (useMovement) RectTransform.anchoredPosition = Vector2.Lerp(startPosition, Vector2.zero, movementCurve.Evaluate(currentFill));
        if (useScale) RectTransform.localScale = Vector2.Lerp(Vector2.zero, Vector2.one, scaleCurve.Evaluate(currentFill));
        if (useAlpha) canvasGroup.alpha = Mathf.Lerp(0, 1, alphaCurveIn.Evaluate(currentFill)); 
    }

    internal void SetColor(Color currentColor)
    {
        unityColorEvent?.Invoke(currentColor);
    }

}
    
