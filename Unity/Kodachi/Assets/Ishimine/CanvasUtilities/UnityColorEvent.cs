﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityColorEvent : UnityEvent<Color>
{

}
    
