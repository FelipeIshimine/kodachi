﻿using UnityEngine;

public class InfiniteBackgroundLayer : MonoBehaviour
{
    private InfiniteSpriteBackground owner;
    private SpriteRenderer[] spriteRenderer;
    BackgrounLayerSettings settings;
    Vector2 size;

    public void Initialize(InfiniteSpriteBackground owner, BackgrounLayerSettings settings, int layer, string sortingLayerName)
    {
        this.owner = owner;
        this.settings = settings;
        size = new Vector2(
            (settings.sprite.rect.width / settings.sprite.pixelsPerUnit) * gameObject.transform.localScale.x,
            (settings.sprite.rect.height / settings.sprite.pixelsPerUnit) * gameObject.transform.localScale.y) / 2;

        if (spriteRenderer == null)
        {
            spriteRenderer = new SpriteRenderer[4];
            for (int i = 0; i < spriteRenderer.Length; i++)
            {
                GameObject go = new GameObject();
                go.transform.SetParent(transform);
                spriteRenderer[i] = go.AddComponent<SpriteRenderer>();
                spriteRenderer[i].sortingLayerName = sortingLayerName;
                spriteRenderer[i].sprite = settings.sprite;
                spriteRenderer[i].sortingOrder = layer;
            }
        }

        spriteRenderer[0].transform.localPosition = new Vector3(-size.x, -size.y);
        spriteRenderer[1].transform.localPosition = new Vector3(-size.x, size.y);
        spriteRenderer[2].transform.localPosition = new Vector3(size.x, -size.y);
        spriteRenderer[3].transform.localPosition = new Vector3(size.x, size.y);
    }

    internal void UpdatePosition(Vector2 deltaDisplacement)
    {
        transform.position += (Vector3)deltaDisplacement * settings.parallax + (Vector3)settings.displacementSpeed * Time.deltaTime;

        Vector2 difference = transform.position - owner.transform.position;
        if (Mathf.Abs(difference.x) > size.x)
            Displace(Vector2.left * size.x*2 * Mathf.Sign(difference.x));

        if(Mathf.Abs(difference.y) > size.y)
            Displace(Vector2.down * size.y*2 * Mathf.Sign(difference.y));
    }

    private void Displace(Vector2 displacement)
    {
        transform.position += (Vector3)displacement;
    }

   
}
