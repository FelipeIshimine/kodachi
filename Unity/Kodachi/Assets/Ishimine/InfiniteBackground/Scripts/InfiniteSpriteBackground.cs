﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class InfiniteSpriteBackground : MonoBehaviour
{
    private Camera cam;
    public float distanceToCam = 10;

    private InfiniteBackgroundLayer[] layers;
    public BackgrounLayerSettings[] spriteLayers;
    private Vector2 lastPosition;

    [Button]
    public void Initialize(Camera cam, float distanceToCam = 10)
    {
        Initialize(cam, spriteLayers, distanceToCam);
    }

    public void Initialize(Camera cam, BackgrounLayerSettings[] spriteLayers, float distanceToCam = 10)
    {
        if (spriteLayers == null || spriteLayers.Length == 0)
            Debug.LogError("SpriteLayers VACIO o NULO");

        if(layers != null)
            Clear();
        this.distanceToCam = distanceToCam;
        this.cam = cam;
        transform.position = cam.transform.position + Vector3.forward * distanceToCam;
        this.spriteLayers = spriteLayers;
        layers = new InfiniteBackgroundLayer[spriteLayers.Length];
        for (int i = 0; i < layers.Length; i++)
        {
            layers[i] = new GameObject().AddComponent<InfiniteBackgroundLayer>();
            layers[i].gameObject.name = $"Background Layer {i}";
            layers[i].transform.position = transform.position;
            layers[i].Initialize(this, spriteLayers[i], i, "BG");
        }
    }

    public void LateUpdate()
    {
        transform.position = cam.transform.position + Vector3.forward * distanceToCam;
        Vector2 delta = ((Vector2)transform.position - lastPosition);
        lastPosition = transform.position;
        foreach (InfiniteBackgroundLayer item in layers)
            item.UpdatePosition(delta);
    }

    private void OnDestroy()
    {
        Clear();
    }

    private void Clear()
    {
        for (int i = 0; i < layers.Length; i++)
            Destroy(layers[i].gameObject);
    }
}