﻿using UnityEngine;

[System.Serializable]
public struct BackgrounLayerSettings
{
    public Vector2 displacementSpeed;
    public Sprite sprite;
    public float parallax;

    public BackgrounLayerSettings(Sprite sprite, float parallax, Vector2 displacementSpeed)
    {
        this.displacementSpeed = displacementSpeed;
        this.sprite = sprite;
        this.parallax = parallax;
    }
}