﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailIK_WPhysics : TailIk
{
    SegmentPhys[] segmentPhys;
    private float resistence;

    public TailIK_WPhysics(float segmentLength, int segmentCount, float resistence, float gravity)
    {
        this.segmentLength = segmentLength;
        this.segmentCount = segmentCount;
        this.resistence = resistence;
        segments = new SegmentPhys[segmentCount];
        segmentPhys = new SegmentPhys[segmentCount];

        for (int i = 0; i < segmentCount; i++)
        {
            segmentPhys[i] = new SegmentPhys(Vector2.zero, 0, SegmenthLength, resistence, gravity);
            segments[i] = segmentPhys[i];
        }
    }

    public void UpdatePhysics()
    {
        for (int i = 0; i < segments.Length; i++)
            segmentPhys[i].UpdatePhysics();
    }

    internal void ApplyPositionOffset(Vector3 offset)
    {
        for (int i = 0; i < segmentPhys.Length; i++)
            segmentPhys[i].ApplyPositionOffset(offset);
    }

    public void ApplyWind()
    {
        Vector2 wind = WindController.Current * Time.fixedDeltaTime * resistence;
        for (int i = 0; i < segments.Length; i++)
            segmentPhys[i].AddExternalForce(wind);
    }
}
