﻿using System;
using UnityEngine;

public class TailIk
{
    protected float segmentLength;
    protected int segmentCount;
    public float SegmenthLength => segmentLength;
    public int SegmentCount => segmentCount;

    public Segment[] segments;

    public TailIk()
    {

    }

    public TailIk(float segmentLength, int segmentCount)
    {
        this.segmentLength = segmentLength;
        this.segmentCount = segmentCount;
        segments = new Segment[segmentCount];
        for (int i = 0; i < segmentCount; i++)
            segments[i] = new Segment(Vector2.zero, 0, SegmenthLength);
    }

    internal Vector3[] GetPoints()
    {
        Vector3[] points = new Vector3[segmentCount + 1];
        for (int i = 0; i < segmentCount; i++)
            points[i] = segments[i].End;

        points[segmentCount] = segments[segmentCount-1].Start;
        return points;
    }

    public void UpdateTarget(Vector2 target)
    {
        segments[0].Follow(target);
        for (int i = 1; i < segmentCount; i++)
            segments[i].Follow(segments[i-1].Start);
    }
}

public class Segment
{
    protected Vector2 start = new Vector2();
    protected Vector2 end = new Vector2();

    public Vector2 Start => start;
    public Vector2 End => end;

    float radianAngle;
    float length;

    public Segment(Vector2 start, float angle, float length)
    {
        this.start.Set(start.x, start.y);
        this.radianAngle = angle;
        this.length = length;
        CalculateEnd();
    }

    public virtual void CalculateEnd()
    {
        float dx = length * Mathf.Cos(radianAngle);
        float dy = length * Mathf.Sin(radianAngle);
        end.Set(start.x + dx, start.y + dy);
    }

    public virtual void Follow(Vector2 target)
    {
        Vector2 dir = start.GetDirection(target);
        radianAngle = Mathf.Deg2Rad * dir.AsAngle();
        dir *= length * -1;
        start = target + dir;
        CalculateEnd();
    }

    internal void OverrideAngle(float angle)
    {
        this.radianAngle = angle;
    }

    public virtual void ApplyPositionOffset(Vector2 offset)
    {
        start += offset;
        end += offset;
    }
}

public class SegmentPhys : Segment
{
    private Vector2 velocity;
    private float resistence = 0;
    private float gravity = 0;
    private Vector2 lastPhysPosition;
    private Vector2 nStart;

    Vector2 vel;

    public SegmentPhys(Vector2 start, float angle, float length, float resistence, float gravity) : base(start, angle, length)
    {
        this.resistence = resistence;
        this.gravity = gravity;
    }

    public void UpdatePhysics()
    {
        nStart = start + (velocity) * Time.fixedDeltaTime;
        velocity += (nStart - lastPhysPosition);
        start = nStart;

        velocity += Physics2D.gravity * gravity * (1-resistence) * Time.fixedDeltaTime;
        //velocity -= velocity * resistence * Time.fixedDeltaTime;

        velocity = Vector2.SmoothDamp(velocity, Vector2.zero, ref vel, .75f, float.MaxValue, Time.fixedDeltaTime);
        lastPhysPosition = start;
    }

    public override void ApplyPositionOffset(Vector2 offset)
    {
        base.ApplyPositionOffset(offset);
        lastPhysPosition += offset;
    }

    public void AddExternalForce(Vector2 extraForce) => velocity += extraForce;
}