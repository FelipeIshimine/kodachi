﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScarfRendering : MonoBehaviour
{
    [Header("Rendering")]
    public AnimationCurve widthCurve = AnimationCurve.Linear(1, 1, 1, 1);
    public float widthMultiplier = .2F;
    public Gradient colorGradient;
    public LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer.widthMultiplier = widthMultiplier;
        lineRenderer.widthCurve = widthCurve;
        lineRenderer.colorGradient = colorGradient;
    }

    internal void Render(Vector3[] points)
    {
        lineRenderer.positionCount = points.Length;
        lineRenderer.SetPositions(points);
    }
}
