﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

[RequireComponent(typeof(ScarfRendering))]
public class Scarf : MonoBehaviour
{
    public WindSettingsSO windSettingsSO;

    [Header("Structure")]
    public float length = 1.5f;
    public int density = 10;

    [Range(0,1)]
    public float airResistence = .9f;

    [Range(0,2)]
    public float gravity = 1;

    public TailIK_WPhysics tailIk;
    public ScarfRendering rendering;
    public bool useWind = true;

    private void Awake() => Initialize();

    [Button]
    private void Initialize()
    {
        WindController.Initialize(windSettingsSO.Value);

        if (rendering == null)
            rendering = GetComponent<ScarfRendering>();

        int segmentCount = Mathf.RoundToInt(length * density);
        float segmentLength = length / segmentCount;

        tailIk = new TailIK_WPhysics(segmentLength, segmentCount, airResistence, gravity);
    }

    public void FixedUpdate()
    {
        tailIk.UpdatePhysics();
        if (useWind)
            tailIk.ApplyWind();
    }

    internal void Follow(Vector3 position)
    {
        tailIk.UpdateTarget(position);
        rendering.Render(tailIk.GetPoints());
    }

    internal void ApplyPositionOffset(Vector3 offset)
    {
        tailIk.ApplyPositionOffset(offset);
    }


}
