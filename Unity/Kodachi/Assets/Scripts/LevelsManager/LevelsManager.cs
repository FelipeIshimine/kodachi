﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets;
#endif
using UnityEngine;
using UnityEngine.AddressableAssets;
using Sirenix.OdinInspector;
using System;

[CreateAssetMenu]
public class LevelsManager : ScriptableSingleton<LevelsManager>
{


#if UNITY_EDITOR
    [MenuItem("GameManagement/LevelsManager")]
    public static void SelectMe()
    {
        Selection.SetActiveObjectWithContext(Instance, Instance);
    }
#endif

    public override LevelsManager Myself => this;

    [SerializeField] private List<LevelSettings> stages = null;
    public static List<LevelSettings> Levels => Instance.stages;

    [System.Serializable]
    public struct LevelSettings
    {
        public string ID;
        [FoldoutGroup("Rewards")] public float TimeForReward;
        [FoldoutGroup("Rewards")] public bool HasKillsReward;
        [FoldoutGroup("Rewards")] public bool HasAlarmReward;
        [FoldoutGroup("Moveset")] public bool CanJump;
        [FoldoutGroup("Moveset")] public bool CanAirDash;
        [FoldoutGroup("Moveset")] public bool CanWallDash;
        [FoldoutGroup("Moveset")] public bool HasSword;
        [FoldoutGroup("Moveset")] public bool HasHooks;
        public AssetReferenceGameObject StageReference;
    }

    private string[] nameList;
    public static string[] NameList
    {
        get
        {
            if (Instance.nameList == null)
                Instance.UpdateNameList();
            return Instance.nameList;
        }
    }

    private void OnEnable()
    {
        UpdateNameList();
    }

    private void OnValidate()
    {
        UpdateNameList();
    }

    private void UpdateNameList()
    {
        nameList = new string[stages.Count];
        for (int i = 0; i < stages.Count; i++)
            nameList[i] = $"{i}-{stages[i].StageReference.editorAsset.name}";
    }


#if UNITY_EDITOR
    [Button]
    public void CollectStages()
    {
        #region Cargar path de archivos
        string path = Application.dataPath + "/Prefab/Levels/Stages";
        Debug.Log($"Buscando niveles en path:{path} con el patron 'Stage_*.prefab'");
        string[] files = Directory.GetFiles(path, "Stage_*.prefab", SearchOption.AllDirectories);
        #endregion

        #region Modificar Path para utilizar con assetDatabase
        int removeCount = Application.dataPath.Replace("Assets", string.Empty).Length;
        for (int i = 0; i < files.Length; i++)
        {
            Debug.Log($"File {i}:{files[i]}");
            files[i] = files[i].Remove(0, removeCount);
            Debug.Log($"File {i}:{files[i]}");
        }
        #endregion

        #region Agregar referencias faltantes en la lista de Stages
        GameObject[] stagesGo = new GameObject[files.Length];
        int dif = files.Length - stages.Count;
        if (dif > 0)
            for (int i = 0; i < dif; i++)
                stages.Add(new LevelSettings());
        #endregion

        #region Cargar GameObject
        for (int i = 0; i < files.Length; i++)
            stagesGo[i] = AssetDatabase.LoadAssetAtPath<GameObject>(files[i]);
        #endregion

        #region Convertir en Addressables
        AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
        AddressableAssetGroup group = settings.FindGroup("Stages");
        for (int i = 0; i < files.Length; i++)
        {
            Debug.Log($"files[{i}] => {files[i]}");
            string guid = AssetDatabase.AssetPathToGUID(files[i]);
            //This is the function that actually makes the object addressable
            AddressableAssetEntry entry = settings.CreateOrMoveEntry(guid, group);
            entry.labels.Add("Stage");
            entry.address = $"{Path.GetFileNameWithoutExtension(files[i])}";
            settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
        }
        //You'll need these to run to save the changes!
        AssetDatabase.SaveAssets();
        #endregion

        #region Asignar al Level Manager
        for (int i = 0; i < stages.Count; i++)
            stages[i].StageReference.SetEditorAsset(stagesGo[i]);
        #endregion
        UpdateNameList();
    }
#endif
}


/*
private static LevelsManager instance;
public static LevelsManager Instance
{
    get
    {
        if (instance == null)
            InitializeInstance();
        return instance;
    }
}

#if UNITY_EDITOR
[ProgressBar(0, 100, ColorMember = "BarColor")] public float StagesDoneProgress;
Color BarColor => Color.Lerp(Color.red, Color.green, StagesDoneProgress / 100);
#endif

private const string InstancePath = "LevelsManager";

private static void InitializeInstance()
{
#if UNITY_EDITOR
    string folderPath = Application.dataPath + "/Resources/";
    Debug.Log("folderPath: " + folderPath);
    if (!Directory.Exists(folderPath))
    {
        Debug.Log("Creating folder at: " + folderPath);
        Directory.CreateDirectory(folderPath);
    }
#endif
    instance = Resources.Load<LevelsManager>(InstancePath);
#if UNITY_EDITOR
    if (instance == null)
        instance = MakeScriptableObject.CreateAsset<LevelsManager>("Assets/Resources/LevelsManager.asset");
#endif
}

internal static LevelSettings GetLevelSettings(int chapterIndex, int localLevelIndex)
{
    return Instance.stages[Instance.chapters[chapterIndex][localLevelIndex]];
}

[SerializeField] private List<AssetReference> loopedModules = null;
public static List<AssetReference> LoopedModules => Instance.loopedModules;

[SerializeField] private List<LevelSettings> stages = null;
public static List<LevelSettings> Levels => Instance.stages;

[SerializeField] private List<Chapter> chapters;
public static List<Chapter> Chapters => Instance.chapters;

[System.Serializable]
public struct Chapter
{
    public string chapterName;
    public string spriteBackground;
    public int[] LevelsIds;

    internal int GetNextLevelIndex(int currentLevelLocalIndex)
    {
        if (currentLevelLocalIndex < LevelsIds.Length - 1)
            return currentLevelLocalIndex + 1;
        else
            return -1;
    }

    public int this[int localIndex] => LevelsIds[localIndex];

    internal bool HasStage(int v)
    {
        return v < LevelsIds.Length;
    }
}

[System.Serializable]
public struct LevelSettings
{
    public string ID;
    [FoldoutGroup("Rewards")] public float TimeForReward;
    [FoldoutGroup("Rewards")] public bool HasKillsReward;
    [FoldoutGroup("Rewards")] public bool HasAlarmReward;
    [FoldoutGroup("Moveset")] public bool CanJump;
    [FoldoutGroup("Moveset")] public bool CanAirDash;
    [FoldoutGroup("Moveset")] public bool CanWallDash;
    [FoldoutGroup("Moveset")] public bool HasSword;
    [FoldoutGroup("Moveset")] public bool HasHooks;
    public AssetReferenceGameObject StageReference;
}

internal static bool DoesNextLevelExist(int chapterIndex, int localLevelIndex) => Instance._DoesNextLevelExist(chapterIndex, localLevelIndex);
private bool _DoesNextLevelExist(int chapterIndex, int localLevelIndex)
{
    Vector2Int nextLevelIds = _GetNextChapterAndStageIds(chapterIndex, localLevelIndex);
    return (nextLevelIds.x != -1 && nextLevelIds.y != -1);
}

internal static string GetStageTitle(int chapterIndex, int levelLocalIndex)
{
    return $"{Instance.chapters[chapterIndex].chapterName} \n {chapterIndex}-{levelLocalIndex} ";
}

private int GetLevelIndexFromLocalIndex(int chapterIndex, int levelLocalIndex)
{
    return Instance.chapters[chapterIndex].LevelsIds[levelLocalIndex];
}

internal static Vector2Int GetNextLevelIds(int currentChapter, int currentLevel) => Instance._GetNextChapterAndStageIds(currentChapter, currentLevel);

private Vector2Int _GetNextChapterAndStageIds(int currentChapter, int currentStage)
{
    if (chapters[currentChapter].HasStage(currentStage + 1))
        return new Vector2Int(currentChapter, currentStage+1); //Seguimos dentro del capitulo
  else
    return new Vector2Int(currentChapter+1, 0); //Pasamos al siguiente capitulo
}

private int GetNextChapter(int currentChapter)
{
    if (currentChapter < chapters.Count - 1)
        return currentChapter + 1;
    else
        return -1;
}

internal static LevelSettings GetStage(int chapterIndex, int localIndex) => Instance._GetStage(chapterIndex, localIndex);
private LevelSettings _GetStage(int chapterIndex, int localIndex)
{
    Debug.Log($"_GetStage({chapterIndex}=> [{localIndex}]");
    return stages[chapters[chapterIndex].LevelsIds[localIndex]];
}

internal static bool DoesLevelExist(int uniqueLevelId) => Instance._DoesLevelExist(uniqueLevelId);
private bool _DoesLevelExist(int uniqueLevelId) => uniqueLevelId < stages.Count && stages[uniqueLevelId].StageReference != null;

#if UNITY_EDITOR
[Button]
public void CollectStages()
{
    #region Cargar path de archivos
    string path = Application.dataPath + "/Prefab/Levels/Stages";
    Debug.Log($"Buscando niveles en path:{path} con el patron 'Stage_*.prefab'");
    string[] files = Directory.GetFiles(path, "Stage_*.prefab", SearchOption.AllDirectories);
    #endregion

    #region Modificar Path para utilizar con assetDatabase
    int removeCount = Application.dataPath.Replace("Assets", string.Empty).Length;
    for (int i = 0; i < files.Length; i++)
    {
        Debug.Log($"File {i}:{files[i]}");
        files[i] = files[i].Remove(0, removeCount);
        Debug.Log($"File {i}:{files[i]}");
    }
    #endregion

    #region Agregar referencias faltantes en la lista de Stages
    GameObject[] stagesGo = new GameObject[files.Length];
    int dif = files.Length - stages.Count;
    if (dif > 0)
        for (int i = 0; i < dif; i++)
            stages.Add(new LevelSettings());
    #endregion

    #region Cargar GameObject
    for (int i = 0; i < files.Length; i++)
        stagesGo[i] = AssetDatabase.LoadAssetAtPath<GameObject>(files[i]);
    #endregion

    #region Convertir en Addressables
    AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
    AddressableAssetGroup group = settings.FindGroup("Stages");
    for (int i = 0; i < files.Length; i++)
    {
        Debug.Log($"files[{i}] => {files[i]}");
        string guid = AssetDatabase.AssetPathToGUID(files[i]);
        //This is the function that actually makes the object addressable
        AddressableAssetEntry entry = settings.CreateOrMoveEntry(guid, group);
        entry.labels.Add("Stage");
        entry.address = $"Stage_{i}";
        settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
    }
    //You'll need these to run to save the changes!
    AssetDatabase.SaveAssets();
    #endregion

    #region Asignar al Level Manager
    for (int i = 0; i < stages.Count; i++)
        stages[i].StageReference.SetEditorAsset(stagesGo[i]);
    #endregion

    StagesDoneProgress = ((float)stages.Count / TotalStagesInChapters()) * 100;
}
#endif
}*/
