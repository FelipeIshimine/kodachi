﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogTrigger : MonoBehaviour
{
    public UnityEvent OnDialogEndEvents;
    public Action OnDialogEnd;
    public Canvas_GrandMasterDialog Canvas_GrandMasterDialog;
    public ExternalTrigger externalTrigger;
    public bool destroyOnDone = true;

    public Dialog[] dialogs;
    Player player;
    int dialogIndex = 0;
    Canvas_MobileInput Canvas_MobileInput;

    private void Awake()
    {
        externalTrigger.IsActive = true;
        externalTrigger.OnEnter += OnExternalTriggerEnter;
    }
    private void OnDestroy()
    {
        externalTrigger.OnEnter -= OnExternalTriggerEnter;
    }

    private void OnExternalTriggerEnter(Collider2D obj)
    {
        if(obj.gameObject.CompareTag("Player"))
            StartDialog(obj.GetComponentInParent<Player>());
    }

    private void StartDialog(Player player)
    {
        Canvas_MobileInput = FindObjectOfType<Canvas_MobileInput>();
        Canvas_MobileInput?.Close();
        this.player = player;
        player.SetEnable(false, false, false);
        dialogIndex = -1;
        Canvas_GrandMasterDialog.Open(NextDialog);
    }

    private void NextDialog()
    {
        dialogIndex++;
        Canvas_GrandMasterDialog.SetSprite(dialogs[dialogIndex].image);
        Canvas_GrandMasterDialog.ShowText(dialogs[dialogIndex].texts, dialogs[dialogIndex].waitBetween, dialogs[dialogIndex].endWait, DialogDone);
    }

    private void DialogDone()
    {
        if (dialogIndex == dialogs.Length-1)
            Canvas_GrandMasterDialog.Close(OnDialogClosed);
        else
            NextDialog();
    }

    private void OnDialogClosed()
    {
        player.SetEnable(true, false, false);
        Canvas_MobileInput?.Open();
        OnDialogEnd?.Invoke();
        externalTrigger.IsActive = false;
        OnDialogEndEvents?.Invoke();
        externalTrigger.enabled = false;

        if (destroyOnDone)
            Destroy(transform.parent.gameObject, 1);
    }
}

[System.Serializable]   
public struct Dialog
{
    public Sprite image;
    public string[] texts;
    public float waitBetween;
    public float endWait;
}
