﻿public class Unhookable_Timed : Unhookable
{
    public void Initialize(float duration)
    {
        Destroy(this, duration);
    }
}
