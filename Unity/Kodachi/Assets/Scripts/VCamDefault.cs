﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VCamDefault : MonoBehaviour
{
    public void OnEnable()
    {
        VirtualCameraController.DefaultVirtualCamera = GetComponent<CinemachineVirtualCamera>();
    }
}
