﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    public LayerMask layerMask;
    public Sprite[] sprites;
    public SpriteRenderer render;

    private void Awake()
    {
        render.sprite = sprites.GetRandom();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!layerMask.Contains(collision.gameObject.layer))
            return;

        Health h = collision.GetComponentInParent<Health>();
        if(h != null)
            h.Damage(1);
    }

}

