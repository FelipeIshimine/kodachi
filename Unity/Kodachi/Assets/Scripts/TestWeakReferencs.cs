﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class TestWeakReferencs : MonoBehaviour
{
    [Button]
    public void Print()
    {
        RootState.PrintWeakReferences();
    }

    [Button]
    public void ForceGC()
    {
        GC.Collect();
    }
}
