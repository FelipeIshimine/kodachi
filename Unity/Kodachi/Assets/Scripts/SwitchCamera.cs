﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Sirenix.OdinInspector;

public class SwitchCamera : MonoBehaviour
{
    public bool changeOnPlayerEnterTrigger = true;
    public bool disableTriggerAfterChange = true;
    public bool switchToDefaultCamera = true;

    [HideIf("switchToDefaultCamera")]  public CinemachineVirtualCamera targetVirtualCamera;

    public void ChangeCamera()
    {
        ChangeCamera(targetVirtualCamera);
    }

    public void ChangeCamera(CinemachineVirtualCamera virtualCamera)
    {
        VirtualCameraController.SetCamera(virtualCamera);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (changeOnPlayerEnterTrigger && collision.attachedRigidbody != null && collision.attachedRigidbody.gameObject.CompareTag("Player"))
        {
            if (switchToDefaultCamera)
                VirtualCameraController.DefaultCamera();
            else
                ChangeCamera();

            if (disableTriggerAfterChange)
                gameObject.SetActive(false);
        }
    }
}
