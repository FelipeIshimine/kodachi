﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit_AttackTrigger : MonoBehaviour
{
    private Collider2D col;
    public Action<Collider2D, bool> OnAttackDone;
    public LayerMask layerMask;
    public GameObject slashVfxPrefab;
    IUseAttackTrigger owner;

    public bool canSwordClash = true; 

    public void SetActive(bool value)
    {
        col.enabled = value;
    }
    private void Awake()
    {
        owner = GetComponentInParent<IUseAttackTrigger>();
        if(owner != null)        OnAttackDone += owner.OnAttackDone;
        col = GetComponent<Collider2D>();
    }

    private void OnDestroy()
    {
        if (owner != null) OnAttackDone -= owner.OnAttackDone;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!col.enabled) return;

        Debug.LogWarning($"<color=red> other:{other.name} </color>");
        if (layerMask.Contains(other.gameObject.layer))
        {
            Debug.LogWarning($"<color=red> other:{other.name} </color>");
            AttackTrigger attackTrigger = other.gameObject.GetComponent<AttackTrigger>();
            Player player;
            if (canSwordClash && attackTrigger != null)
            {
                Debug.LogWarning( "<color=red> SwordClash </color>");
                player = attackTrigger.GetComponentInParent<Player>();
                if(player != null)
                {
                    SwordClash(player);
                    return;
                }
            }

            Health health = other.gameObject.GetComponentInParent<Health>();
            if (health != null)
            {
                attackTrigger = health.GetComponentInChildren<AttackTrigger>();
                if (canSwordClash &&  attackTrigger.Collider.IsTouching(col))
                {
                    SwordClash(attackTrigger.Owner as Player);
                    return;
                }

                bool isLethal = health.Damage(1);
                OnAttackDone?.Invoke(other, isLethal);

                if (slashVfxPrefab != null)
                {
                    GameObject go = Instantiate(slashVfxPrefab, null);
                    go.transform.position = other.transform.position;
                    go.transform.rotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360));
                    Destroy(go, 5);
                }
            }
            HookKunai hookKunai = other.gameObject.GetComponent<HookKunai>();
            if (hookKunai != null) hookKunai.Cancel();
        }
    }

    private void SwordClash(Player player)
    {
        player.AttackKnockbackFrom(transform.position, true);
        if (owner is SamuraiUnit samurai)
            samurai.SwordsClash(player);
        col.enabled = false;
    }

    private bool IsSwordClash(Collider2D trigger1, Collider2D trigger2)
    {
        return trigger1.IsTouching(trigger2);
    }
}
