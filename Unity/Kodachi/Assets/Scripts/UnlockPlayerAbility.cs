﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockPlayerAbility : MonoBehaviour
{
    public bool canJump;
    public bool canAirDash;
    public bool canWallDash;
    public bool hasSword;
    public bool hasHooks;

    public void Unlock()
    {
        Player p = FindObjectOfType<Player>();
        p.Initilize(true, hasSword, canAirDash, canWallDash, hasHooks, true);
    }
}
