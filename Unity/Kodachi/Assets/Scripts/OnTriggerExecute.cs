﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D ))]
public class OnTriggerExecute : MonoBehaviour
{
    public LayerMask layerMask;
    public UnityEvent OnTriggerEnterExecute;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(layerMask.Contains(collision.gameObject.layer))
            OnTriggerEnterExecute.Invoke();
    }
}
