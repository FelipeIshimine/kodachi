﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class CyclicTimer : MonoBehaviour
{
    public float cycleDuration = 4;
    public bool resetOnDisable = false;
    public UnityEvent OnCycleDone;

    [Range(0,1)]
    public float startValue;

   [ShowInInspector] private float t = 0;

    private void Awake()
    {
        t = startValue;
    }

    private void FixedUpdate()
    {
        t += Time.fixedDeltaTime / cycleDuration;
        if (t >= 1)
        {
            OnCycleDone.Invoke();
            t = 0;
        }
    }

    private void OnDisable()
    {
        if(resetOnDisable)
            t = startValue;
    }
}
