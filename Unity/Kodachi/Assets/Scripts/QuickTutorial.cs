﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class QuickTutorial : MonoBehaviour, IUseOnTriggerEnter2D
{
    public Text text;
    public AnimatedContainer animatedContainer;
    public ExternalTrigger externalTrigger;
    public Button btn;

   [TextArea(3,10)] public string tutorialText;

    private GameStateLink<GamePlayState> GamePlayStateLink;
    private GamePlayState GamePlayState => GamePlayStateLink.GameState;

    private Canvas_MobileInput mobileInput;
    private Canvas_MobileInput MobileInput
    {
        get
        {
            if (mobileInput == null)
                mobileInput = FindObjectOfType<Canvas_MobileInput>();
            return mobileInput;
        }
    }

    private Player player;
    private Player Player
    {
        get
        {
            if (playerInput == null)
                player = FindObjectOfType<Player>();
            return player;
        }
    }

    private PlayerInput playerInput;
    private PlayerInput PlayerInput
    {
        get
        {
            if (playerInput == null)
            {
                player = FindObjectOfType<Player>();
                playerInput = player.GetComponent<PlayerInput>();
            }
            return playerInput;
        }
    }


    [Flags] public enum ResumeOption { Default = 0, Jump = 1, Hook = 2}
    [Flags] public enum DirectionalOption { None = 0, Left = 1, Right = 2}
    [Flags] public enum InputOption { None = 0, Jump = 1, Hook = 2, Squash = 4, AirDash = 8}

    public ResumeOption resumeBy;
    public DirectionalOption directionOfInput;
    public InputOption ActivateInput;

    private void Awake()
    {
        externalTrigger.Initialize(this);   
        GamePlayStateLink = new GameStateLink<GamePlayState >(this);
    }

    private void OnValidate()
    {
        transform.position = Vector3.zero;
        text.text = tutorialText;
        externalTrigger.name = "Trigger-" + tutorialText;
    }

#if UNITY_EDITOR
    [ExecuteInEditMode]
    private void Update()
    {
        transform.position = Vector3.zero;
    }
#endif

    public void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log($"<color=blue> OnTriggerEnter2D</color>");
        if(col.attachedRigidbody.gameObject.CompareTag("Player"))
        {
            GamePlayState.Pause();
            animatedContainer.Open();
            btn.gameObject.SetActive(false);

            Debug.Log($"resumeBy:{resumeBy}");
            if(resumeBy == ResumeOption.Default)
            {
                btn.gameObject.SetActive(true);
                MobileInput.Close();
            }
            else
            {
                if (resumeBy.HasFlag(ResumeOption.Jump)) PlayerInput.OnJumpInput += Resume;
                if (resumeBy.HasFlag(ResumeOption.Hook)) PlayerInput.OnHookInput += Resume;
                MobileInput.Open();
            }

            Debug.Log($"ActivateInput:{ActivateInput}");
            if (ActivateInput != InputOption.None)
            {
                mobileInput.ShowButtons(
                    ActivateInput.HasFlag(InputOption.Jump),
                    ActivateInput.HasFlag(InputOption.Hook));

                Canvas_MobileInput.Buttons shineFlags = Canvas_MobileInput.Buttons.None;

                if(resumeBy.HasFlag(ResumeOption.Jump))
                {
                    if (directionOfInput.HasFlag(DirectionalOption.Left))
                        shineFlags = shineFlags | Canvas_MobileInput.Buttons.LeftJump;
                    if (directionOfInput.HasFlag(DirectionalOption.Right))
                        shineFlags = shineFlags | Canvas_MobileInput.Buttons.RightJump;
                }

                if (resumeBy.HasFlag(ResumeOption.Hook))
                {
                    if (directionOfInput.HasFlag(DirectionalOption.Left))
                        shineFlags = shineFlags | Canvas_MobileInput.Buttons.LeftHook;
                    if (directionOfInput.HasFlag(DirectionalOption.Right))
                        shineFlags = shineFlags | Canvas_MobileInput.Buttons.RightHook;
                }

                mobileInput.PlayShine(shineFlags);

                if (ActivateInput.HasFlag(InputOption.Hook))
                    player.SetUseHook(true);

                if (ActivateInput.HasFlag(InputOption.Squash))
                    player.SetUseSquash(true);

                if (ActivateInput.HasFlag(InputOption.AirDash))
                    player.SetUseAirDash(true);
            }
            externalTrigger.gameObject.SetActive(false);
        }
    }

    public void Resume(int dir)
    {
        if (directionOfInput == DirectionalOption.None)
            Resume();
        else
        {
            if (dir == -1 && directionOfInput.HasFlag(DirectionalOption.Left))
                Resume();
            else if (dir == 1 && directionOfInput.HasFlag(DirectionalOption.Right))
                Resume();
        }
    }

    public void Resume()
    {
        GamePlayState.Resume();

        PlayerInput.OnAnyInput -= Resume;
        PlayerInput.OnJumpInput -= Resume;
        PlayerInput.OnHookInput -= Resume;

        if (resumeBy == ResumeOption.Default) 
                MobileInput.Close();

        animatedContainer.Close();
        mobileInput.StopShine();
    }

}

public static class GameStateExtensionFroMonoBehaviour
{
    public static void FindGameStateInHierarchy<T>(this T source) where T : MonoBehaviour, IUseGameState
    {
        source.GamePlayState = source.GetComponentInParent<GameStateProxy>().GameState;
    }
}

