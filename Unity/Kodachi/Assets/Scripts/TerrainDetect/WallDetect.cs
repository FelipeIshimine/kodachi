﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDetect : MonoBehaviour
{
    public Rigidbody2D rb;
    public BoxCollider2D boxCollider2D;
    public LayerMask layerMask;

    public int contactPointsNeededToContact = 3;
    public int rayCount = 3;
    public float distance = .2f;

    public bool OnWall => (inLeftWall || inRightWall);

    public bool inLeftWall = false;
    public bool inRightWall = false;

    public  float vel;
    public float smoothTime = .2f;

    public float upperCorrection = .5f;
    public Vector2 startCorrection = Vector2.up;

    private float stopTime = 1;

    public bool detectWhenGoingUp = false;

    public Action<bool,int> OnWallHit { get; set; }

    public void FixedUpdate()
    {
        inLeftWall = false;
        inRightWall = false;

        /*if (stopTimePassed < stopTime)
        {
            stopTimePassed += Time.fixedDeltaTime;
            return;
        }*/
        if (!detectWhenGoingUp && rb.velocity.y > 0) return;

        float offset = ((boxCollider2D.bounds.size.y + boxCollider2D.edgeRadius) * upperCorrection ) / (rayCount - 1);
        for (int x = 0; x < 2; x++)
        {
            Vector3 startPoint = (Vector3)startCorrection + ((x == 0) ?
                (boxCollider2D.bounds.min + Vector3.up * boxCollider2D.bounds.size.y + Vector3.left * boxCollider2D.edgeRadius) :
                boxCollider2D.bounds.max + Vector3.right * boxCollider2D.edgeRadius);
            int contacts = 0;

            if ((x == 0 && rb.velocity.x > 0.3f) || (x == 1 && rb.velocity.x < -0.3f))
            {
                OnWallHit?.Invoke(false, (x == 0) ? -1 : 1);
                //Debug.Log($"Continue X:{x}");
                continue;
            }

            for (int y = 0; y < rayCount; y++)
            {
                Vector2 point = startPoint + Vector3.down * y * offset;
                Vector2 dir = (x == 0) ? Vector2.left : Vector2.right;
                RaycastHit2D hit = Physics2D.Raycast(point, dir, distance, layerMask);
                if (hit)
                {
                    //Debug.Log($"X:{x} Contact with {hit.collider.gameObject.name}");
                    contacts++;
                    if(contacts == contactPointsNeededToContact)
                    {
                        OnWallHit?.Invoke(true, (x == 0) ? -1 : 1);
                        Debug.DrawLine(point, hit.point, Color.green);
                        //Debug.Log($"Contact  X:{x}");

                        if (x == 0)
                            inLeftWall = true;
                        else
                            inRightWall = true;
                        break;
                    }
                }
                else
                {
                    Debug.DrawRay(point, dir * distance, Color.red, .1f);
                }

                if (y == rayCount - 1)
                {
                    OnWallHit?.Invoke(false, (x == 0) ? -1 : 1);
                    //Debug.Log($"No contact X:{x}");
                }
            }
        }
    }

    internal void StopDetect(float v)
    {
        stopTime = v;
    }
}
