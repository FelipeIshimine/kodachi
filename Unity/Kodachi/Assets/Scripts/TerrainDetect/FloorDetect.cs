﻿using System;
using UnityEngine;

public class FloorDetect : MonoBehaviour
{
    public Rigidbody2D rb;
    public BoxCollider2D boxCollider2D;
    public LayerMask layerMask;

    public int rayCount = 3;
    public float distance = .2f;

    public bool OnFloor = false;

    public float vel;

    public Action<bool> OnFloorContact { get; set; }

    public void FixedUpdate()
    {
        OnFloor = false;
        if (rb.velocity.y > 0) return;

        float offset = (boxCollider2D.bounds.size.x + boxCollider2D.edgeRadius*2) / (rayCount-1);

        Vector3 startPoint = boxCollider2D.bounds.min - Vector3.one * boxCollider2D.edgeRadius;
        for (int x = 0; x < rayCount; x++)
        {
            Vector2 point = startPoint + Vector3.right * x * offset;
            RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, distance, layerMask);
            if (hit)
            {
                FloorTouched();
                Debug.DrawLine(point, hit.point, Color.green);
                return;
            }
            else
                Debug.DrawRay(point, Vector3.down * distance, Color.red, .1f);
        }
        OnFloor = false;
        OnFloorContact?.Invoke(false);
    }

    private void FloorTouched()
    {
        OnFloorContact?.Invoke(true);
        OnFloor = true;
    }
}