﻿using System;
using System.Collections;
using UnityEngine;

public class ChargeAndExecute
{
    MonoBehaviour owner;
    public Action<float> OnBegin;
    public Action<float> OnStep;
    public Action<float> OnEnd;

    public enum TimeScale { Unscaled, Scaled}

    IEnumerator rutine;

    public ChargeAndExecute(MonoBehaviour owner)
    {
        this.owner = owner;
    }

    public void StartCharge(float chargeDuration, TimeScale timeScale, Action<float> stepRutine, Action callback)
    {
        owner.PlayCoroutine(ref rutine,()=> ChargeAndExecuteRutine(chargeDuration, timeScale, stepRutine, callback));
    }

    IEnumerator ChargeAndExecuteRutine(float chargeDuration, TimeScale timeScale, Action<float> stepRutine, Action callback)
    {
        float t = 0;
        Func<float> currentTimeScale;
        if (timeScale == TimeScale.Scaled)
            currentTimeScale = ()=> Time.deltaTime / chargeDuration;
        else
            currentTimeScale = () => Time.unscaledDeltaTime / chargeDuration;

        OnBegin?.Invoke(t);
        do
        {
            stepRutine?.Invoke(t);
            OnStep?.Invoke(t);
            yield return null;
            t += currentTimeScale.Invoke();
        } while (t<1);

        callback?.Invoke();
        OnEnd?.Invoke(t);
    }

    internal void Stop()
    {
        if(rutine != null) owner.StopCoroutine(rutine);
    }
}