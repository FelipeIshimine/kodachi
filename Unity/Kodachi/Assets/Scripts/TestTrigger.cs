﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTrigger : MonoBehaviour
{
    public BoxCollider2D BoxCollider2D;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log($"collision {collision.gameObject}");
    }

    private void OnDrawGizmos()
    {
        if(BoxCollider2D != null)
            Gizmos.DrawCube(transform.position, BoxCollider2D.size);
        else
            Gizmos.DrawCube(transform.position, Vector3.one);

    }
}
