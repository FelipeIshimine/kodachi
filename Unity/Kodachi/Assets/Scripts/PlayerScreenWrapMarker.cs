﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class PlayerScreenWrapMarker : MonoBehaviour
{
    private Camera cam;
    private Transform camT;
    private PixelPerfectCamera pixelPerfectCamera;
    private float horizontalSize = 3;
    public Transform from;
    public Transform to;
    public SpriteRenderer fromRenderer;
    public SpriteRenderer toRenderer;
    private Transform player;
    public AnimationCurve alphaCurve = AnimationCurve.EaseInOut(0,0,1,1);
    IEnumerator rutine;

    private void Start()
    {
        this.PlayCoroutine(ref rutine, WaitAndInitialize);
    }

    IEnumerator WaitAndInitialize()
    {
        yield return new WaitForSeconds(1);
        Initialize();
    }

    private void Initialize()
    {
        cam = Camera.main;
        pixelPerfectCamera = cam.GetComponent<PixelPerfectCamera>();
        horizontalSize = cam.GetHorizontalSize() / 2;
        camT = cam.transform;
        player = FindObjectOfType<Player>().transform;
    }

    public void Update()
    {
        if (player == null)
            return;

        int dir = (int)Mathf.Sign(camT.position.x - player.position.x);

        float rawDistance = player.position.x - camT.position.x;
        float distance = Mathf.Abs(rawDistance);

        from.position = new Vector2(camT.position.x + horizontalSize * dir, player.position.y);
        to.position = new Vector2(camT.position.x + horizontalSize * -dir, player.position.y);

        float alpha = alphaCurve.Evaluate(distance / horizontalSize);
        fromRenderer.color = new Color(1, 1, 1, alpha);
        toRenderer.color = new Color(1, 1, 1, alpha);
    }

}
