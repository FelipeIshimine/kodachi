﻿using Pooling.Poolers;
using System;
using UnityEngine;

public class Arrow : MonoBehaviour
{

    /// <summary>
    /// ARREGLAR BUG DONDE SI SE CAMBIA DE ESTADO Y LA FLECHA ESTA PEGADA A UN OBJETO SE GENERA UN BUG PORQUE LA FLECHA SE ELIMINA EN VEZ DE GUARDARSE EN EL POOL
    /// </summary>
    public SetPooler brokenArrowPool;
    public SelfPoolable selfPoolable;
    public LayerMask layerMask;
    public float speed;

    public GameObject owner;
    public SpriteRenderer render;
    public Sprite stickedSprite;
    public Sprite fullArrowSprite;

    public Collider2D col;
    private bool isSticked = false;
    public Health health;
    private Rigidbody2D rb;

    const float LifeTime = 5;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        selfPoolable.WaitAndPool(LifeTime);
    }

    private void OnDead(Health obj)
    {
        PlayBrokenArrow();
        selfPoolable.Enqueue();
    }

    public void Initialize(float speed, float angle, LayerMask arrowLayerMask, GameObject owner, float lifeTime = LifeTime)
    {
        gameObject.SetActive(true);
        transform.SetParent(null);
        this.owner = owner;
        layerMask = arrowLayerMask;
        this.speed = speed;
        transform.rotation = Quaternion.Euler(0, 0, angle);

        col.enabled = true;
        rb.simulated = true;
        isSticked = false;
        render.sprite = fullArrowSprite;
    }

    public void FixedUpdate()
    {
        if (isSticked) return;  
        transform.Translate(Vector3.right * speed * Time.fixedDeltaTime);
        transform.rotation = Quaternion.Euler(0, 0, transform.right.AsAngle2D());
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (isSticked) return;
        if (collision.gameObject == owner) return;

        if (layerMask.Contains(collision.gameObject.layer))
        {
            isSticked = true;
            transform.SetParent(collision.transform);
            transform.position = transform.position + transform.right * speed * Time.fixedDeltaTime;
            render.sprite = stickedSprite;
            col.enabled = false;
            rb.simulated = false;
            Health h = collision.GetComponentInParent<Health>();
            if (h != null)
            {
                h.Damage(1);
                selfPoolable.Enqueue();
            }
        }
    }

    private void PlayBrokenArrow()
    {
        Debug.LogWarning("PlayBrokenArrow");
        BrokenArrow brokenArrow = brokenArrowPool.Dequeue().GetComponent< BrokenArrow>();
        brokenArrow.Play(transform.position);
    }
}
