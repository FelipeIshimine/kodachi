﻿using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(SelfPoolable))]
public class BrokenArrow : MonoBehaviour
{
    private SelfPoolable selfPoolable;
    public float force = 5;
    public Rigidbody2D partA;
    public Rigidbody2D partB;

    [Button]
    internal void Play(Vector2 position)
    {
        gameObject.transform.SetParent(null);
        gameObject.SetActive(true);
        transform.position = position;
        partA.velocity = Vector2.zero;
        partB.velocity = Vector2.zero;
        partA.transform.position = position;
        partB.transform.position = position;
        partA.AddForce(new Vector2(UnityEngine.Random.Range(-1f,1f),1)* force, ForceMode2D.Impulse);
        partB.AddForce(new Vector2(UnityEngine.Random.Range(-1f,1f),1)* force, ForceMode2D.Impulse);
        partA.AddTorque(Random.Range(600, 800) * ((Random.Range(0, 2) == 0) ? 1 : -1));
        partB.AddTorque(Random.Range(600, 800) * ((Random.Range(0, 2) == 0) ? 1 : -1));
        StartCoroutine(WaitAndExecute());
    }

    IEnumerator WaitAndExecute()
    {
        yield return new WaitForSeconds(5);
        selfPoolable.Enqueue();
    }

    private void Awake()
    {
        selfPoolable = GetComponent<SelfPoolable>();
    }

}