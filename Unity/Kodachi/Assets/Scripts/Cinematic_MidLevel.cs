﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class Cinematic_MidLevel : MonoBehaviour
{
    public ExternalTrigger externalTrigger;
    public Cinematic_Controller Cinematic_Controller;
    public Transform playerEndPosition;
    Player player;
    Canvas_MobileInput mobileInput;

    public bool moveCamera = true;

    public GameObject[] disableObjects;
    public GameObject[] enableObjects;

    IEnumerator rutine;

    public float targetCameraSize = 3;
    public float originalCameraSize;
    private Vector2 originalCameraPosition;

    public Transform targetCameraPosition;
    private Vector2 camStartPos;

    public float cameraMoveDuration = .5f;
    public AnimationCurve cameraAnimCurveIn;
    public AnimationCurve cameraAnimCurveOut;
    public Camera cam;
    private CameraFollow cameraFollow;
    private PixelPerfectCamera pixelPerfectCamera;

    public GameObject cameraLetterBoxPrefab;
    public Canvas_LetterBox canvas_LetterBox;

    //private Rect targetCameraRect = new Rect(0, .3f, 1, .4f);

    private void Awake()
    {
        externalTrigger.OnEnter += OnExternalTriggerEnter;
        Cinematic_Controller.OnEndingReached += OnCinematicDone;
        canvas_LetterBox = Instantiate(cameraLetterBoxPrefab, null).GetComponent<Canvas_LetterBox>();
    }

    private void OnDestroy()
    {
        externalTrigger.OnEnter -= OnExternalTriggerEnter;
        Cinematic_Controller.OnEndingReached -= OnCinematicDone;
        Destroy(canvas_LetterBox);
    }

    private void OnExternalTriggerEnter(Collider2D obj)
    {
        if(obj.gameObject.CompareTag("Player"))
        {
            TimeManager.Pause();
            player = obj.GetComponentInParent<Player>();
            StartCinematic_Prepare(obj);
        }
    }

    private void StartCinematic_Prepare(Collider2D obj)
    {
        mobileInput = FindObjectOfType<Canvas_MobileInput>();
        player.visualControl.HideAll();
        player.SetEnable(false, true, true);
        obj.gameObject.SetActive(false);
        mobileInput?.Close();
        TimeManager.Resume();
        player.transform.position = playerEndPosition.position;
        if (moveCamera)
            MoveCameraToPosition(StartCinematic_Execute);
        //UI_ScreenTransitionSystem.TransitionIn();
    }

    private void StartCinematic_Execute()
    {
        Cinematic_Controller.Play();
    }

    private void OnCinematicDone()
    {
        mobileInput?.Open();
        player.visualControl.ShowAll();
        player.SetEnable(true, false, false);
        player.gameObject.SetActive(true);

        for (int i = 0; i < enableObjects.Length; i++)
            enableObjects[i].SetActive(true);
        for (int i = 0; i < disableObjects.Length; i++)
            enableObjects[i].SetActive(false);

        if (moveCamera)
            ResetCameraPosition();

        externalTrigger.enabled = false;

        Destroy(gameObject,1);
    }

    private void MoveCameraToPosition(Action callback)
    {
        canvas_LetterBox.Open();
        cam = Camera.main;
        cameraFollow = cam.gameObject.GetComponent<CameraFollow>();
        pixelPerfectCamera = cam.gameObject.GetComponent<PixelPerfectCamera>();

        EnableCameraBehaviours(false);
        originalCameraSize = cam.orthographicSize;
        originalCameraPosition = cam.transform.position;
        if (rutine != null) StopCoroutine(rutine);
        rutine = MoveCameraRutine(targetCameraPosition.position, targetCameraSize, cameraAnimCurveIn, callback);
        StartCoroutine(rutine);
    }
   
    private void EnableCameraBehaviours(bool value)
    {
        pixelPerfectCamera.enabled = value;
        cameraFollow.enabled = value;
    }

    private void ResetCameraPosition()
    {
        canvas_LetterBox.Close();
        if (rutine != null) StopCoroutine(rutine);
        rutine = MoveCameraRutine(originalCameraPosition, originalCameraSize, cameraAnimCurveOut, () => EnableCameraBehaviours(true));
        StartCoroutine(rutine);
    }

    IEnumerator MoveCameraRutine(Vector3 targetPositions, float targetCameraSize, AnimationCurve curve, Action postAction)
    {
        Vector3 startPosition = cam.transform.position;
        float startCameraSize = cam.orthographicSize;
        float t = 0;
        Rect startRect = cam.rect;
        targetPositions.z = startPosition.z;
        do
        {
            t += Time.deltaTime / cameraMoveDuration;
            cam.transform.position = Vector3.Lerp(startPosition, targetPositions, t);
            cam.orthographicSize = Mathf.Lerp(startCameraSize, targetCameraSize, t);
          //  cam.rect = new Rect(startRect.x, Mathf.Lerp(startRect.y, targetCameraRect.y, t), 1, Mathf.Lerp(startRect.height, targetCameraRect.height, t));
            yield return null;
        } while (t < 1);
        postAction?.Invoke();
    }
}
