﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaitAndExecuteEvent : MonoBehaviour
{
    public bool isRealtime = false;
    public float defaulWait = 1;

    public UnityEvent OnWaitStart;
    public UnityEvent OnWaitDone;
    public UnityFloatEvent OnWaitProgress;

    public bool canBeOverriten = false;

    public bool IsReady { get; private set; } = true;

    public void StartTimer(float t = -1)
    {
        if (!canBeOverriten && !IsReady) return;

        if (t == -1)
            t = defaulWait;
        OnWaitStart?.Invoke();
        StartCoroutine((isRealtime)? WainAndExecuteRealTime(t): WainAndExecute(t));
    }

    IEnumerator WainAndExecute(float waitDuration)
    {
        float t = 0;
        IsReady = false;
        do
        {
            t += Time.deltaTime/ waitDuration;
            OnWaitProgress?.Invoke(t);
            yield return null;
        } while (t<1);
        IsReady = true;
        OnWaitDone?.Invoke();
    }

    IEnumerator WainAndExecuteRealTime(float waitDuration)
    {
        float t = 0;
        do
        {
            t += Time.unscaledDeltaTime / waitDuration;
            OnWaitProgress?.Invoke(t);
            yield return null;
        } while (t < 1);
        OnWaitDone?.Invoke();
    }
}

[System.Serializable]
public class UnityFloatEvent : UnityEvent<float>
{

}
