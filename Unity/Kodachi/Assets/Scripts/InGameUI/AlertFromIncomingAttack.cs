﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertFromIncomingAttack : MonoBehaviour
{
    public Sprite[] alertSprites;
    public Sprite[] arrowSprites;

    public AnimationCurve alertSpritesCurve;
    public AnimationCurve sizeCurve;
    public AnimationCurve alphaCurve;

    public AnimationCurve arrowSpritesCurve;
    public SpriteRenderer alertRender;
    public SpriteRenderer arrowRender;
    public float scaleMultiplicator = 2;

    private void Awake() => ClearRenders();

    public void OnAlertFill(float t)
    {
        Debug.Log($"t:{t}");

        int index = Mathf.RoundToInt(alertSpritesCurve.Evaluate(t) * (alertSprites.Length - 1));
        Debug.Log($"index:{index}");

        alertRender.sprite = alertSprites[index];
        alertRender.color = new Color(1, 1, alphaCurve.Evaluate(t));

        index = Mathf.RoundToInt(arrowSpritesCurve.Evaluate(t) * (arrowSprites.Length - 1));
        Debug.Log($"index:{index}");
        arrowRender.sprite = arrowSprites[index];
        arrowRender.color = new Color(1, 1, alphaCurve.Evaluate(t));

        transform.localScale = Vector3.one * sizeCurve.Evaluate(t) * scaleMultiplicator;
        arrowRender.transform.localScale = Vector3.one * sizeCurve.Evaluate(t) * scaleMultiplicator;

        if (t >= 1)
            ClearRenders();
    }

    private void ClearRenders()
    {
        alertRender.sprite = null;
        arrowRender.sprite = null;
    }

}
