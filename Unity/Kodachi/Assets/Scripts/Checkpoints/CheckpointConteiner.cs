﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointConteiner : MonoBehaviour
{
    private List<Checkpoint> checkpoints = null;
    internal List<Checkpoint> GetSortedCheckpoints()
    {
        if (checkpoints == null)
        {
            checkpoints = new List<Checkpoint>();
            GetComponentsInChildren(checkpoints);
            if(checkpoints.Count > 0)
                checkpoints.Sort(CompareCheckpoints);
        }
        return checkpoints; 
    }

    private int CompareCheckpoints(Checkpoint x, Checkpoint y)
    {
        if (x.transform.position.y > y.transform.position.y) return 1;
        else if (x.transform.position.y < y.transform.position.y) return -1;
        return 0;
    }
}
