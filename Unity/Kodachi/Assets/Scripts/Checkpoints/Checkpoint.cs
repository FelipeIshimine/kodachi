﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Checkpoint : MonoBehaviour
{
    public Action<Checkpoint> OnActivated;
    private bool isActive = false;
    public ConstantFloating constantFloating;

    public Color activeColor;
    public Color inactiveColor;
    public SpriteRenderer render;
    public StudioEventEmitter sound;
    public Shine shine;

    private void Awake()
    {
        Deactivate();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isActive) return;

        Player p = collision.GetComponentInParent<Player>();
        if(p != null)
            Activate();
    }

    public void Activate()
    {
        isActive = true;
        OnActivated?.Invoke(this);
        shine.Go();
        render.color = activeColor;
        constantFloating.enabled = true;

        //Audio
        sound.Play();
    }

    public void Deactivate()
    {
        render.color = inactiveColor;
        constantFloating.enabled = false;
    }

    private void OnDestroy()
    {
        OnActivated = null;
    }
}
