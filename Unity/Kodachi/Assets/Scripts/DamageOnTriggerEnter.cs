﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DamageOnTriggerEnter : MonoBehaviour
{
    public bool disableAfter = false;
    public LayerMask layerMask;
    private void OnTriggerEnter2D(Collider2D obj)
    {
        if (layerMask.Contains(obj.gameObject.layer))
        {
            Health h = obj.GetComponentInParent<Health>();
            h?.Damage(1);
            if (disableAfter)
                gameObject.SetActive(false);
        }
    }
}
