﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shine_Img : MonoBehaviour
{
    [Range(0,1)]
    public float shineValue = .7f;
    public Image img;
    public AnimationCurve curve;
    public float cycleDration = 1.5f;
    ChargeAndExecute chargeAndExecute;
    public ChargeAndExecute.TimeScale timeScale = ChargeAndExecute.TimeScale.Unscaled;

    private void Awake()
    {
        img.color = Color.clear;
        chargeAndExecute = new ChargeAndExecute(this);
    }

    public void Shine()
    {
        chargeAndExecute.StartCharge(cycleDration, timeScale, StepRutine, OnDone);
    }

    private void OnDone()
    {
        Shine();
    }

    private void StepRutine(float t)
    {
        img.color = new Color(1, 1, 1, curve.Evaluate(t)* shineValue);
    }

    public void Stop()
    {
        img.color = Color.clear;
        chargeAndExecute.Stop();
    }

}
