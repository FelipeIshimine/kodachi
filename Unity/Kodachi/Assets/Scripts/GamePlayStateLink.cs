﻿using UnityEngine;

internal class GameStateLink<T> where T : GameState
{
    public readonly T GameState;
    public GameStateLink(MonoBehaviour owner)
    {
        GameStateProxy gameStateProxy = owner.GetComponentInParent<GameStateProxy>();
        GameState = gameStateProxy.GameState as T;
    }
}