﻿public class MonoBehaviourState : GameState
{
    private MonoBehaviourState CurrentMonoBehaviourState = null;
    public override void SwitchState(IState nState)
    {
        base.SwitchState(nState);
        if (CurrentState is MonoBehaviourState monoBehaviourState) CurrentMonoBehaviourState = monoBehaviourState;
    }

    public virtual void OnDisable()     => CurrentMonoBehaviourState?.OnDisable(); 
    public virtual void OnEnable()      => CurrentMonoBehaviourState?.OnEnable();
    public virtual void Update()        => CurrentMonoBehaviourState?.Update();
    public virtual void LateUpdate()    => CurrentMonoBehaviourState?.LateUpdate();
    public virtual void FixedUpdate()   => CurrentMonoBehaviourState?.FixedUpdate();
    public virtual void OnDestroy()     => CurrentMonoBehaviourState?.OnDestroy();

    public override void Enter() { }
}