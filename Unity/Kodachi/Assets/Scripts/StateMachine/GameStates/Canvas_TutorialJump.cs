﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_TutorialJump : MonoBehaviour
{
    public Text txt;
    [TextArea(5,10)]  public string firstDialog;
    [TextArea(5, 10)] public string secondDialog;
  
    Action onDialogClosedCallback;

    public AnimatedContainer conteiner;

    public void Initialize(Action onDialogClosedCallback)
    {
        this.onDialogClosedCallback = onDialogClosedCallback;
        conteiner.Hide();
    }

    public void DisplayFirstDialog()
    {
        conteiner.Open();
        txt.text = firstDialog;
    }

    internal void DisplaySecondDialog()
    {
        conteiner.Open();
        txt.text = secondDialog;
    }

    public void CloseText()
    {
        onDialogClosedCallback?.Invoke();
        conteiner.Close();
    }

    internal void Open() => conteiner.Open();
}