﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using ScreenTransitionSystem;

internal class TutorialJump_State : GamePlayState
{
    TutorialStage_Jump tutorialStage;
    Canvas_GrandMasterDialog Canvas_GrandMasterDialog;

    public TutorialJump_State(Action OnBackTOMenuCallback) : base(OnBackTOMenuCallback)
    {
    }

    public override void OnEnter()
    {
        base.OnEnter();
        AssignPrefabTo(out tutorialStage);
        AssignPrefabTo(out Canvas_GrandMasterDialog);
        tutorialStage.Initialize(FirstCheckPoint, OnPlayerReachedEnd);
        Canvas_MobileInput.ShowButtons(true, false);
        Player.SetEnable(false, false, false);
        Canvas_PauseManu.gameObject.SetActive(false);
        ScreenTransition.In(() => Canvas_GrandMasterDialog.Open(StartFirstDialog));
    }

    private void StartTutorial()
    {
        Canvas_PauseManu.gameObject.SetActive(true);
        Player.transform.position = Stage.PlayerSpawnPoint.position;
        Player.gameObject.SetActive(true);
        StartGamePlay();
        tutorialStage.DissapearFirst();
        Canvas_MobileInput.Open();

        Player.Initilize(true, false, false, false, false);
        Player.gameObject.SetActive(true);
    }

    public void FirstCheckPoint()
    {
        Canvas_MobileInput.Close();
        Pause();
        Canvas_GrandMasterDialog.Open(StartSecondDialog);
    }

    private void ContinueTutorial()
    {
        tutorialStage.DissapearSecond();
        Canvas_MobileInput.Open();
        Resume();
        Player.Initilize(true, false, false, false, false);
    }

    private void OnPlayerReachedEnd()
    {
        Canvas_GrandMasterDialog.Open(StartThirdDialog);
        Pause();
    }

    public override void OnExit()
    {
    }

    #region Dialogs

    private void StartFirstDialog()
    {
        Canvas_GrandMasterDialog.ShowText(
            new string[] 
            {
                "If you want to become part of the Order, you will have to master our ways...",
                "First the way of the Monkey",
                "Reach the top to prove you are worthy",
                "...",
                "What?",
                "That you dont know how to do it?",
                "That's certainly not my problem young one",
                "You are the one who wanted to enter the order",
                "Now earn it"
            },
            4,
            4,
            ()=> Canvas_GrandMasterDialog.Close(StartTutorial));
    }

    private void StartSecondDialog()
    {
        Canvas_GrandMasterDialog.ShowText(
             new string[]
             {
                "So.. It seems that you can really move like a monkey,",
                "not only look like one",
                "Good, really good",
                "Now the Way of the Eagle",
                "Jump like a Monkey, and when you are ready",
                "Channel your inner energy, kick the air!",
                 "and dash through it like an Eagle",
                "Simple enough",
                "See you at the top",
                "[ After you Jump, press the Jump button again to DASH mid air ]",
             },
             4,
             float.MaxValue,
             () => Canvas_GrandMasterDialog.Close(ContinueTutorial));
    }


    private void StartThirdDialog()
    {
        Canvas_GrandMasterDialog.ShowText(
             new string[]
             {
                "Very well young one",
                "You did it, you prove you are not useless",
                "Maybe you do have a bright future in front of you",
                "See you tomorrow morning",
                "Dont make me wait"
             },
             4,
             4,
             () => Canvas_GrandMasterDialog.Close(BackToMenu));
    }


    #endregion

}
