﻿using System;
using UnityEngine;
using System.Collections.Generic;
using ScreenTransitionSystem;

public class RootState : GameStateWithAddressableAssets
{
    public static bool PrintDebug = true;

    private static RootState instance;
    public static RootState Instance
    {
        get 
        {
            if (instance == null)
                instance = new RootState();
            return instance; 
        }
    }

    private static List<WeakReference> weakReferences = new List<WeakReference>();

    private GameSequence currentSequence;

    [RuntimeInitializeOnLoadMethod]
    public static void Initialize()
    {
        if (PrintDebug) Debug.Log("<Color=green> GameManager.Initialize() </color>");

        OnInstantiationProgress = ScreenTransition.SetProgress;
        Instance.SwitchState(Instance);
    }


    public static void PrintWeakReferences()
    {
        for (int i = weakReferences.Count-1; i > 0; i--)
        {
            if(weakReferences[i].IsAlive)
            {
                Debug.Log($"State: {weakReferences[i].Target.ToString()} is still alive");
            }
            else
            {
                Debug.Log($"Element {i} is NOT alive. Removing it");
                weakReferences.RemoveAt(i);
            }
        }
    }

    public override void OnEnter()
    {
        if (PrintDebug) Debug.Log("<Color=green> GameManager State ENTER </color>");
        GoToMenu();
    }

    public override void SwitchState(IState nState)
    {
        base.SwitchState(nState);
        weakReferences.Add(new WeakReference(nState));
    }

    //TEST
    private void GoToStart()
    {
        //ScreenTransition.Out(() => SwitchState(new RaceState(GoToMenu)),.5f);
    }

    //Beta
    private void GoToTutorial_Jump() => ScreenTransition.Out(() => SwitchState(new TutorialJump_State(GoToMenu)));

    //Beta
    private void GoToTutorial_AirDash() => ScreenTransition.Out(() => SwitchState(new TutorialAirDash_State(GoToMenu)));

    //TODO
    private void GoToTutorial_Squash()
    {
        //ScreenTransition.Out(() => SwitchState(new RaceState(GoToMenu)));
    }

    //TODO
    private void GoToTutorial_Hook()
    {
        //ScreenTransition.Out(() => SwitchState(new RaceState(GoToMenu)));
    }

    //TODO
    private void GoToTutorial_Full()
    {
        //ScreenTransition.Out(() => SwitchState(new RaceState(GoToMenu)));
    }

    private void OnRestartSequence()
    {
        currentSequence.GoToState();
    }

    private void GoToLevelSelection()
    {
        ScreenTransition.Out(() => SwitchState(new LevelSelectionState(GoToMenu, GoToSequence)));
    }

    private void GoToSequence(int chapter, int sequence)
    {
        GameStructure.GoTo(chapter,sequence);
    }

    private void GoToTestBossFight()
    {
        ScreenTransition.Out(() => SwitchState(new SamuraiBossFightGameState(GoToMenu)));
    }

    private void GoToLoopStageTest()
    {
    //    ScreenTransition.Out(() => SwitchState(new LoopedStageState(GoToMenu, GoToNextLevel, GoToLoopStageTest, 1, 6)));
    }

    public static void GoToCinematic(GameSequence cinematicSequence)
    {
        Instance.currentSequence = cinematicSequence;
        ScreenTransition.Out(() => Instance.SwitchState(new CinematicState(cinematicSequence, Instance.GoToMenu)));
    }

    public void GoToCinematic(int cinematicId)
    {
        GameStructure.GetCinematic(cinematicId).GoToState();
    }

    internal static void GoToLevel(GameSequence currentSequence)
    {
        Instance.currentSequence = currentSequence;
        ScreenTransition.Out(() => Instance.SwitchState(new ClassicMissionState(currentSequence, Instance.GoToMenu, Instance.OnRestartSequence)));
    }

    //BETA
    private void GoToMenu()
    {
        ScreenTransition.Out(
            () => SwitchState(
                new MenuState(
                    GoToStart, 
                    GoToTutorial_Jump,
                    GoToTutorial_AirDash, 
                    GoToLevelSelection, 
                    GoToTestBossFight, 
                    GoToLoopStageTest,
                    ()=> GoToCinematic(0))));
    }

    public override void OnExit()
    {
    }
}
