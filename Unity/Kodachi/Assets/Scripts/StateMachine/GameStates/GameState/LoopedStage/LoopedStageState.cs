﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LoopedStageState : ClassicMissionState
{
    LoopedStage LoopedStage;

    public LoopedStageState(GameSequence levelSequence, Action OnBackTOMenuCallback, Action restartLevel) : base(levelSequence, OnBackTOMenuCallback, restartLevel)
    {
    }

    public override void OnEnter()
    {
        AssignPrefabTo(out LoopedStage);
        base.OnEnter();
        LoopedStage.Initialize(Player);
    }

}
