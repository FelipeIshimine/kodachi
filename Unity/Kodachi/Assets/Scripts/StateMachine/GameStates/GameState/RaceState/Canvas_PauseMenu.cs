﻿using System;
using UnityEngine;

public class Canvas_PauseMenu : MonoBehaviour
{
    public GameObject pauseButton;
    public AnimatedContainer AnimatedContainer;
    Action OnBackToMenuCallback, OnPauseCallback, OnResumeCallback;

    public void Initialize(Action OnBackTOMenuCallback, Action OnPauseCallback, Action OnResumeCallback)
    {
        this.OnBackToMenuCallback = OnBackTOMenuCallback;
        this.OnPauseCallback = OnPauseCallback;
        this.OnResumeCallback = OnResumeCallback;
    }

    public void Finish()
    {
        this.OnBackToMenuCallback = null;
        this.OnPauseCallback = null;
        this.OnResumeCallback = null;
    }

    public void BackToMenu()
    {
        OnBackToMenuCallback?.Invoke();
    }

    public void Resume()
    {
        AnimatedContainer.Close(OnResume);
    }

    private void OnResume()
    {
        pauseButton.gameObject.SetActive(true);
        OnResumeCallback?.Invoke();
    }

    public void Pause()
    {
        pauseButton.gameObject.SetActive(false);
        OnPauseCallback?.Invoke();
        AnimatedContainer.Open();
    }

    internal void Hide()
    {
        AnimatedContainer.Hide();
    }
}
