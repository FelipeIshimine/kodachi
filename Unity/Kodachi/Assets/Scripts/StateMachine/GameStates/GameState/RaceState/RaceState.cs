﻿using ScreenTransitionSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class RaceState : ClassicMissionState
{
    RaceStage raceStage;

    public RaceState(GameSequence levelSequence, Action OnBackTOMenuCallback, Action restartLevel) : base(levelSequence, OnBackTOMenuCallback, restartLevel)
    {
    }

    public override void OnEnter()
    {
        AssignPrefabTo(out raceStage);
        Stage = raceStage;
        base.OnEnter();

        raceStage.OnPlayerReachedEnd += OnPlayerReachedEnd;
        Canvas_MobileInput.Initialize(Player);
        Canvas_MobileInput.Open();
        Canvas_MobileInput.ShowButtons(true, true);

        ScreenTransition.InAfterWaiting(2, StartGamePlay);
    }

    protected override void StartGamePlay()
    {
        base.StartGamePlay();
        Player.Initilize(true,true, true, true, true, true);
    }

    private void OnPlayerReachedEnd()
    {
        BackToMenu();
    }

    public override void OnExit()
    {
        Canvas_MobileInput.Close();
    }

    protected override void OnPlayerDead()
    {
        gameStateProxy.WaitAndExecute(RespawnWaitTime,RespawnPlayer);
    }

}
