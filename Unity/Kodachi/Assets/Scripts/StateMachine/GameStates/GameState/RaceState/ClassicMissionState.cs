﻿using System;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using Sirenix.OdinInspector;
using UnityEngine;
using ScreenTransitionSystem;
using Cinemachine;

public class ClassicMissionState : GamePlayState
{
    Canvas_LevelComplete canvas_LevelComplete;
    Canvas_StageTitle canvas_StageTitle;
    Canvas_Timer Canvas_Timer;
    Player player;

    private float endTime;
    float time;

    int kills = 0;
    int alarms = 0;

    public float targetTime = 120;
    public int totalEnemies;
    LevelsManager.LevelSettings levelSettings;
    Action OnLoadNextLevel;
    Action restartLevel;
    InfiniteSpriteBackground background;

    GameSequence levelSequence;

    public ClassicMissionState(GameSequence levelSequence, Action OnBackTOMenuCallback, Action restartLevel) : base(OnBackTOMenuCallback)
    {
        this.restartLevel = restartLevel;
        this.levelSequence = levelSequence;
        LoadStage(levelSequence);
    }

    private void LoadStage(GameSequence levelSequence)
    {
        levelSettings = LevelsManager.Levels[levelSequence.Index];
        ExtraAssets.Add(levelSettings.StageReference);
    }

    public override void OnEnter()
    {
        base.OnEnter();

        AssignPrefabTo(out Canvas_Timer);
        AssignPrefabTo(out player);
        AssignPrefabTo(out canvas_LevelComplete);
        AssignPrefabTo(out canvas_StageTitle);
        AssignPrefabTo(out background);

        background.Initialize(Camera.main);

        Canvas_MobileInput.Initialize(Player);
        Canvas_MobileInput.Open();
        Canvas_MobileInput.ShowButtons(true, levelSettings.HasHooks);
        Canvas_Timer.Initialize();

        canvas_LevelComplete.Initialize(BackToMenu, levelSequence.GetNext());

        ScreenTransition.In(StartGamePlay);

        totalEnemies = Stage.GetEnemies().Count;
        Register();
    }

    private void OnUpdate()
    {
        if(Input.GetKeyDown(KeyCode.J))
        {
            OnPlayerReachedEnd();
        }
    }

    private void OnGlobalDead(Health obj)
    {
        BaseEnemyUnitMonoBehaviour enemy = obj.GetComponent<BaseEnemyUnitMonoBehaviour>();
        if (enemy != null)
            kills++;
    }

    private void OnUnitAlarmRaised(BaseEnemyUnitMonoBehaviour obj)
    {
        alarms++;
    }

    private void Update()
    {
        time += Time.deltaTime;
    }

    protected override void StartGamePlay()
    {
        base.StartGamePlay();

        canvas_StageTitle.Open(GetCurrentStageTitle());
        Player.Initilize(true, levelSettings.HasSword, levelSettings.CanAirDash, levelSettings.CanWallDash, levelSettings.HasHooks, true);
        Canvas_Timer.Play();
    }

    private string GetCurrentStageTitle() => levelSequence.name;

    private void OnPlayerReachedEnd()
    {
        Canvas_Timer.Stop();
        endTime = time;
        StopGroupCamera();
        canvas_LevelComplete.Open();
    }

    private void NextLevel()
    {
        OnLoadNextLevel.Invoke();
    }

    private void Done()
    {
        BackToMenu();
    }

    public override void OnExit()
    {
        base.OnExit();
        Unregister();
        Canvas_MobileInput.Close();
    }

    private void Unregister()
    {
        GlobalUpdate.OnUpdateEvent -= OnUpdate;
        Stage.OnPlayerReachedEnd -= OnPlayerReachedEnd;
        OnLoadNextLevel = null;
        restartLevel = null;
        GlobalUpdate.OnUpdateEvent -= Update;
        BaseEnemyUnitMonoBehaviour.OnRaiseGlobalAlarm = OnUnitAlarmRaised;
        Health.OnGlobalDead = OnGlobalDead;
    }

    private void Register()
    {
        GlobalUpdate.OnUpdateEvent += OnUpdate;
        Stage.OnPlayerReachedEnd += OnPlayerReachedEnd;
        GlobalUpdate.OnUpdateEvent += Update;
        BaseEnemyUnitMonoBehaviour.OnRaiseGlobalAlarm += OnUnitAlarmRaised;
        Health.OnGlobalDead += OnGlobalDead;
    }

    protected override void OnPlayerDead()
    {
        restartLevel?.Invoke();
    }

    public override string ToString()
    {
        return base.ToString() + $" - Chapter:{levelSequence.Chapter.Index} Stage:{levelSequence.Index}";
    }
}
