﻿using System;
using UnityEngine;

public class GamePlayCanvas : MonoBehaviour
{
    public Action OnPause;
    public Action OnResume;
    private bool inPause = false;

    public void Initialize(Action OnPause, Action OnResume)
    {
        this.OnResume = OnResume;
        this.OnPause = OnPause;
    }

    public void Pause()
    {
        OnPause?.Invoke();
        inPause = true;
    }

    public void Resume()
    {
        OnResume?.Invoke();
        inPause = false;
    }

    private void OnDestroy()
    {
        OnResume = null;
        OnPause = null;
    }

    public void Swap()
    {
        inPause = !inPause;
        if (inPause)
            Pause();
        else
            Resume();
    }
}