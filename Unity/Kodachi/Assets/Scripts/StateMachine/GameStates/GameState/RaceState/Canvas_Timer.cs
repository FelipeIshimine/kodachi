﻿using UnityEngine;
using UnityEngine.UI;

public class Canvas_Timer : MonoBehaviour
{
    private float currentTime;
    public Text txt;

    public AnimatedContainer AnimatedContainer;

    public bool Counting { get; private set; }

    public void Initialize()
    {
        currentTime = 0;
    }

    public void Play()
    {
        Counting = true;
    }

    public float Stop()
    {
        Counting = false;
        return currentTime;
    }

    public void Hide() => AnimatedContainer.Hide();
    public void Show() => AnimatedContainer.Show();
    public void Open() => AnimatedContainer.Open();
    public void Close() => AnimatedContainer.Close();

    public void Update()
    {
        if (!Counting) return;
        currentTime += Time.deltaTime;
        //txt.text = $"{currentTime / 60}:{currentTime % 60}";
        txt.text = currentTime.ToString("mm:ss:ff");
    }


}