﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class Canvas_StageDone : MonoBehaviour
{
    public AnimatedContainer statisticsConteiner;
    public AnimatedContainer rewardConteiner;

    public float numbersDuration = 4;

    public Text enemiesKilledText;
    public Text alarmsRaisedText;
    public Text timeText;

    public Image rewardIconRender;
    public Text rewardDescription;

    [TabGroup("LethalShadow"), SerializeField] private Sprite allKillsNoAllarmsIcon;
    [TabGroup("LethalShadow"), SerializeField] private  string allKillsAndNoAlarmsRewardDescription = "Strike, quick as Lightning - Kill all enemies without raising any alarms";

    [TabGroup("Lethal"), SerializeField] private string allKillsRewardDescription = "Fight, fierce as Fire - Kill all enemies";
    [TabGroup("Lethal"), SerializeField] private Sprite allKillsIcon;

    [TabGroup("Swift"), SerializeField] private string timeRewardDescription = "Move, swift as the wind - Finish in less than {} seconds";
    [TabGroup("Swift"), SerializeField] private Sprite timeIcon;

    [TabGroup("AllKills"), SerializeField] private string noAlarmsRewardDescription = "Be, undetectable as a Shadow - Complete without raising any alarms and without any unnecesary kills";
    [TabGroup("AllKills"), SerializeField] private Sprite noAlarmsIcon;

    private bool skip = false;

    public Button nextLevelButton;

    internal void Hide()
    {
        statisticsConteiner.Hide();
        rewardConteiner.Hide();
    }

    private bool ready = false;

    Action OnContinued;
    Action OnBackToMenu;
    Action OnNextLevel;

    public void Initialize(float targetTime, Action backToMenuCallback, Action nextLevelCallback, bool showNextLevelButton)
    {
        timeText.text = string.Empty;
        alarmsRaisedText.text = string.Empty;
        enemiesKilledText.text = string.Empty;
        this.OnBackToMenu = backToMenuCallback;
        this.OnNextLevel = nextLevelCallback;
        nextLevelButton.gameObject.SetActive(showNextLevelButton);
    }

    IEnumerator rutine;
    float time,  alarmsRaised,  neutralized;

    public void ShowStatistics(float time, int alarmsRaised, int neutralized, Action callback)
    {
        this.time = time;
        this.alarmsRaised = alarmsRaised;
        this.neutralized = neutralized;
        timeText.text = time.ToString();
        alarmsRaisedText.text = alarmsRaised.ToString();
        enemiesKilledText.text = neutralized.ToString();
        OnContinued = callback;
        statisticsConteiner.Open(AnimateNumbers);
    }

    public void ShowAllKillsReward(Action callback) => ShowReward(allKillsRewardDescription, allKillsIcon, callback);
    public void ShowNoAlarmsReward(Action callback) => ShowReward(noAlarmsRewardDescription, noAlarmsIcon, callback);
    public void ShowTimeReward(Action callback) => ShowReward(timeRewardDescription, timeIcon, callback);
    public void ShowAllKillsNoAlarms(Action callback) => ShowReward(allKillsAndNoAlarmsRewardDescription, allKillsNoAllarmsIcon, callback);

    public void ShowReward(string text, Sprite image, Action callback)
    {
        rewardConteiner.Hide();
        rewardIconRender.sprite = image;
        rewardDescription.text = text;
        ready = false;
        skip = false;
        OnContinued = callback;
        rewardConteiner.Open(()=> ready = true);
    }

    private void AnimateNumbers()
    {
        StartCoroutine(AnimateNumberText(numbersDuration, OnContinued));
    }

    IEnumerator AnimateNumberText(float duration, Action callback)
    {
        skip = false;
        ready = false;
        float t = 0;
        do
        {
            yield return null;
            t += Time.deltaTime / duration;
            if (skip)
                t = 1;
            Step(ref t,ref time, ref timeText, "0:00");
            Step(ref t,ref alarmsRaised, ref alarmsRaisedText, "0.");
            Step(ref t,ref neutralized, ref enemiesKilledText, "0.");
        } while (t < 1);
        ready = true;
        callback?.Invoke();
    }

    IEnumerator WaitAndExecute(float waitTime, Action callback)
    {
        yield return new WaitForSecondsRealtime(waitTime);
        callback.Invoke();
    }

    private void Step(ref float currentT,ref float targetValue,ref Text text, string format = "")
    {
        text.text = Mathf.Lerp(0, targetValue, currentT).ToString(format);
    }

    public void Pressed()
    {
        if (!ready)
            Skip();
        else
            rewardConteiner.Close(OnContinued);
    }

    public void BackToMenu()
    {
        OnBackToMenu?.Invoke();
    }


    public void NextLevel()
    {
        OnNextLevel?.Invoke();
    }

    private void Skip()
    {
        skip = true;
    }

}