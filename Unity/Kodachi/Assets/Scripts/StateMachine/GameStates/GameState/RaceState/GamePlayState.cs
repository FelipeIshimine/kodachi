﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using ScreenTransitionSystem;
using Cinemachine;
using Object = UnityEngine.Object;

public abstract class GamePlayState : GameStateWithAddressableAssets
{
    public static Action<GamePlayState> OnGamePlayStarted;

    protected Player Player;
    protected Stage Stage;

    protected CinemachineVirtualCamera vCam;
    protected CinemachineConfiner confiner; 
    protected CinemachineTargetGroupRadar cinemachineTargetGroupRadar;
    protected CinemachineTargetGroup CinemachineTargetGroup;

    protected Action OnBackToMenuCallback;
    protected Action OnStartGamePlay;

    protected Canvas_PauseMenu Canvas_PauseManu;
    protected Canvas_MobileInput Canvas_MobileInput;
    protected Empty gameStateProxy;

    private List<Checkpoint> checkpoints;
    private Checkpoint CurrentCheckpoint;

    public const float RespawnWaitTime = 2.5f;

    #region StateMachine
    public GamePlayState(Action OnBackTOMenuCallback)
    {
        this.OnBackToMenuCallback = OnBackTOMenuCallback;
    }

    public override void OnEnter()
    {
        AssignPrefabTo(out Canvas_PauseManu);
        AssignPrefabTo(out Canvas_MobileInput);
        AssignPrefabTo(out Stage);
        AssignPrefabTo(out Player);
        AssignPrefabTo(out vCam);
        AssignPrefabTo(out confiner);

        #region Camera
        cinemachineTargetGroupRadar = vCam.GetComponentInChildren<CinemachineTargetGroupRadar>();
        CinemachineTargetGroup = vCam.GetComponentInChildren<CinemachineTargetGroup>();
        CinemachineTarget playerAsTarget = Player.GetComponent<CinemachineTarget>();
        CinemachineTargetGroup.AddMember(playerAsTarget);
        vCam.Follow = CinemachineTargetGroup.gameObject.transform;
        cinemachineTargetGroupRadar.Initialize();
        #endregion

        Stage.Initialize();
        confiner.m_BoundingShape2D = Stage.Confiner;
        confiner.InvalidatePathCache();

        Canvas_PauseManu.Hide();

        Canvas_MobileInput.Initialize(Player);
        RegisterEvents();

        PositionPlayerAtStart();

        gameStateProxy = new GameObject().AddComponent<Empty>();
        gameStateProxy.name = $"_{this}_Proxy";

        InitializeCheckpoints();
        Pause();
    }

    public override void OnExit()
    {
        cinemachineTargetGroupRadar.Clear();
        CinemachineTargetGroup.transform.SetParent(vCam.transform);
        CinemachineTargetGroup.RemoveMember(Player.transform);
        UnregisterEvents();
        Object.Destroy(gameStateProxy.gameObject);
    }

    #endregion

    private void InitializeCheckpoints()
    {
        checkpoints = Stage.GetSortedCheckpoints();
        foreach (Checkpoint checkpoint in checkpoints)
            checkpoint.OnActivated += OnCheckpointActivated;
    }

    private void OnCheckpointActivated(Checkpoint obj)
    {
        obj.OnActivated -= OnCheckpointActivated;
        if (CurrentCheckpoint == null || CurrentCheckpoint.transform.position.y < obj.transform.position.y)
            CurrentCheckpoint = obj;
    }

    protected virtual void OnPlayerDead()
    {
        StopGroupCamera();
        gameStateProxy.WaitAndExecute(RespawnWaitTime,RespawnPlayer);
    }

    protected void RespawnPlayer()
    {
        Player.transform.position = GetPlayerRespawnPosition();
        Player.Initilize(true);
        RestartGroupCamera();
    }

    protected void PositionPlayerAtStart()
    {
        Player.transform.position = Stage.PlayerSpawnPoint.position;
        Player.SetEnable(false, true, true);
    }

    public Vector2 GetPlayerRespawnPosition()
    {
        return (CurrentCheckpoint == null) ? Stage.PlayerSpawnPoint.position : CurrentCheckpoint.transform.position;
    }

    protected virtual void StartGamePlay()
    {
        confiner.InvalidatePathCache();
        Canvas_PauseManu.Initialize(BackToMenu, Pause, Resume);
        OnStartGamePlay?.Invoke();
        Resume();
        OnGamePlayStarted?.Invoke(this);
    }

    public void Pause()
    {
        StopGroupCamera();
        Player.SetEnable(false, false, false);
        SwitchState(new PauseGamePlayState());
    }

    public void Resume()
    {
        RestartGroupCamera();
        confiner.InvalidatePathCache();
        Player.SetEnable(true, false, false);
        SwitchState(new PlayingGamePlayState());
    }

    protected void BackToMenu()
    {
        ScreenTransition.Out(OnBackToMenuCallback, .2f);
    }

    private void RegisterEvents()
    {
        TimelinePlayer.OnStartAny += OnCutsceneStarted;
        TimelinePlayer.OnEndAny += OnCutsceneEnded;
        Player.OnPlayerDead += OnPlayerDead;
    }

    private  void UnregisterEvents()
    {
        TimelinePlayer.OnStartAny -= OnCutsceneStarted;
        TimelinePlayer.OnEndAny -= OnCutsceneEnded;
        Player.OnPlayerDead -= OnPlayerDead;
    }

    #region Cutscenes
    private void OnCutsceneStarted(TimelinePlayer timelinePlayer)
    {
        StopGroupCamera();
        Canvas_MobileInput.gameObject.SetActive(false);
        vCam.enabled = false;
        Player.SetEnable(false, true, true);
        Player.enabled = false;
    }
  
    private void OnCutsceneEnded(TimelinePlayer timelinePlayer)
    {
        Player.transform.SetParent(null);
        Player.enabled = true;
        Player.SetEnable(true, false, false);
        Canvas_MobileInput.gameObject.SetActive(true);
        vCam.enabled = true;
        RestartGroupCamera();
    }

    #endregion

    protected void StopGroupCamera()
    {
        CinemachineTargetGroup.m_Targets = new CinemachineTargetGroup.Target[0];
        cinemachineTargetGroupRadar.gameObject.SetActive(false);
    }

    protected void RestartGroupCamera()
    {
        cinemachineTargetGroupRadar.gameObject.SetActive(true);
        cinemachineTargetGroupRadar.Add(Player.GetComponent<CinemachineTarget>());
    }
}
