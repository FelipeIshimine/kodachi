﻿using System;
using System.Collections;
using UnityEngine;
using ScreenTransitionSystem;
using Cinemachine;
using UnityEngine.U2D;

public class CinematicState : GameStateWithAddressableAssets
{
    public TimelinePlayer tlPlayer;
    private Camera cam;
    private CinemachineBrain cmBrain;
    public PixelPerfectCamera pixelPerfect;
    private int original;
    Action backToMenu;
    GameSequence sequence;

    public CinematicState(GameSequence sequence, Action backToMenu) 
    {
        this.backToMenu = backToMenu;
        this.sequence = sequence;
        ExtraAssets.Add(CinematicManager.Get(sequence.Index));
    }

    public override void OnEnter()
    {
        AssignPrefabTo(out tlPlayer);
        cam = Camera.main;
        cmBrain = cam.GetComponent<CinemachineBrain>();
        pixelPerfect = cam.GetComponent<PixelPerfectCamera>();
        pixelPerfect.enabled = false;
        ScreenTransition.In(StartCinematic);
    }

    private void StartCinematic()
    {
        tlPlayer.OnEnd = OnCinematicEnd;
        tlPlayer.InsertBind(new BindElement("MainCamera", cmBrain));
        tlPlayer.Play();
    }

    private void OnCinematicEnd()
    {
        pixelPerfect.enabled = true;

        GameSequence next = sequence.GetNext();
        if (next == null)
            backToMenu.Invoke();
        else
            next.GoToState();
    }

    public override void OnExit()
    {
        backToMenu = null;
    }
}