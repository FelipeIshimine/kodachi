﻿using UnityEngine;
using Cinemachine;

public class CinemachineTarget : MonoBehaviour
{
    public float width = 1;
    public float radius = 1;
}

public static class CinemachineTargetGroupExtension
{
    public static void AddMember(this CinemachineTargetGroup source, CinemachineTarget target)
    {
        source.RemoveMember(target);
        source.AddMember(target.transform, target.width, target.radius);
    }
    public static void RemoveMember(this CinemachineTargetGroup source, CinemachineTarget target) => source.RemoveMember(target.transform);
}