﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
#if UNITY_EDITOR
using UnityEditor;
#endif
[System.Serializable]
[CreateAssetMenu(menuName ="CinematicManager")]
public class CinematicManager : ScriptableSingleton<CinematicManager>
{
#if UNITY_EDITOR
    [MenuItem("GameManagement/CinematicManager")]
    public static void SelectMe()
    {
        Selection.SetActiveObjectWithContext(Instance, Instance);
    }
#endif

    public override CinematicManager Myself => this;
    [SerializeField] private List<AssetReferenceGameObject> playerPrefabs;
    public List<AssetReferenceGameObject> CinematicReferences => playerPrefabs;

    private string[] cinematicNameList;
    public static string[] NameList
    {
        get
        {
            if (Instance.cinematicNameList == null)
                Instance.UpdateNameList();
            return Instance.cinematicNameList;
        }
    }

    private void OnEnable()
    {
        UpdateNameList();
    }

    private void OnValidate()
    {
        UpdateNameList();
    }

    private void UpdateNameList()
    {
        cinematicNameList = new string[playerPrefabs.Count];
        for (int i = 0; i < playerPrefabs.Count; i++)
            cinematicNameList[i] = $"{i}-{playerPrefabs[i].editorAsset.name}";
    }

    internal static AssetReferenceGameObject Get(int id)
    {
        return Instance.playerPrefabs[id];
    }
}