﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScreenTransitionSystem;
using UnityEngine;

public class MenuState : GameStateWithAddressableAssets
{
    private Canvas_Menu Canvas_Menu;
    private  Action OnStartCallback;
    private  Action onTutorial;
    private  Action onTutorialDash;
    private  Action OnTestMission;
    private  Action onBossFightTest;
    private  Action onLoopStageTest;
    private  Action OnTestCinematic;
    InfiniteSpriteBackground background;

    public MenuState(
        Action OnStartCallback, 
        Action onTutorial,
        Action onTutorialDash,
        Action OnTestMission, 
        Action onBossFightTest, 
        Action onLoopStageTest,
        Action OnTestCinematic)
    {
        this.onTutorial = onTutorial;
        this.OnStartCallback = OnStartCallback;
        this.onTutorialDash = onTutorialDash;
        this.OnTestMission = OnTestMission;
        this.onBossFightTest = onBossFightTest;
        this.onLoopStageTest = onLoopStageTest;
        this.OnTestCinematic = OnTestCinematic;
        Debug.LogWarning("OnTestMission is null: " + (OnTestMission==null));
    }

    public override void OnEnter()
    {
        AssignPrefabTo(out Canvas_Menu);
        AssignPrefabTo(out background);

        Debug.LogWarning("OnTestMission is null: " + (OnTestMission==null));
        Canvas_Menu.Initialize(OnStart, onTutorial, onTutorialDash, OnTestMission, onBossFightTest, onLoopStageTest, OnTestCinematic);
        ScreenTransition.InAfterWaiting(1);
        TimeManager._ResetAll();

        background.Initialize(Camera.main);
    }

    private void OnStart()
    {
        OnStartCallback.Invoke();
    }

    public override void OnExit()
    {
        this.onTutorial = null;
        this.OnStartCallback = null;
        this.onTutorialDash = null;
        this.OnTestMission = null;
        this.onBossFightTest = null;
        this.onLoopStageTest = null;
    }
}
