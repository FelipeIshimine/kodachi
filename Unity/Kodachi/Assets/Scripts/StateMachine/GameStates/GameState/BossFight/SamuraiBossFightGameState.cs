﻿using ScreenTransitionSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
public class SamuraiBossFightGameState : GamePlayState
{
    public SamuraiBoss samuraiBoss;
    public SamuraiBossFightGameState(Action OnBackToMenuCallback) : base(OnBackToMenuCallback)
    {

    }

    public override void OnEnter()
    {
        base.OnEnter();
        AssignPrefabTo(out samuraiBoss);
        ScreenTransition.In(StartGamePlay);
    }

    protected override void StartGamePlay()
    {
        base.StartGamePlay();
        Player.transform.position = Stage.PlayerSpawnPosition;
        samuraiBoss.Play();
        Player.Initilize(true, true, false, false, false);
    }

    public void OnWin()
    {

    }

    public void OnDefeat()
    {

    }


}
