﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGamePlayState : GameState
{
    public override void Enter()
    {
        TimeManager.Pause();
    }
}
