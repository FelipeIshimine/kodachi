﻿using System;
using UnityEngine;

public class TutorialStage_Jump : MonoBehaviour
{
    public ExternalTrigger haftTrigger;
    public ExternalTrigger endTrigger;
    Action onPlayerReachedHaft, onPlayerReachedEnd;

    public Character_GrandMaster firstMaster;
    public Character_GrandMaster secondMaster;
    public Character_GrandMaster thirdMaster;

    public void Initialize(Action onPlayerReachedHaft, Action onPlayerReachedEnd)
    {
        this.onPlayerReachedHaft = onPlayerReachedHaft;
        this.onPlayerReachedEnd = onPlayerReachedEnd;
        haftTrigger.OnEnter += OnHaft;
        endTrigger.OnEnter += OnEnd;
        firstMaster.Idle();
        secondMaster.Idle();
        thirdMaster.Idle();
    }

    public void DissapearFirst()
    {
        firstMaster.Dissapear();
    }

    public void DissapearSecond()
    {
        secondMaster.Dissapear();
    }

    public void DissapearThird()
    {
        thirdMaster.Dissapear();
    }

    private void OnEnd(Collider2D obj)
    {
        Player player = obj.GetComponentInParent<Player>();
        if (player != null)
        {
            onPlayerReachedEnd.Invoke();
            endTrigger.OnEnter = null;
        }
    }

    private void OnHaft(Collider2D obj)
    {
        Player player = obj.GetComponentInParent<Player>();
        if (player != null)
        {
            onPlayerReachedHaft.Invoke();
            haftTrigger.OnEnter = null;
        }
    }

    private void Close()
    {
        haftTrigger.OnEnter -= OnHaft;
        endTrigger.OnEnter -= OnEnd;
    }

    private void OnDestroy()
    {
        Close();
    }

   
}
