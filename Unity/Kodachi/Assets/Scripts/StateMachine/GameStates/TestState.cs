﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestState : GameStateWithAddressableAssets
{
    public Canvas_MobileInput canvas_MobileInput;
    public Player player;
    private CameraFollow camera;

    public override void OnEnter()
    {
        AssignPrefabTo(out canvas_MobileInput);
        AssignPrefabTo(out player);
        Register();

        camera = GameObject.FindObjectOfType<CameraFollow>();
        camera.Initialize(player.GetComponent<Controller2D>());
    }

    private void Register()
    {
        canvas_MobileInput.Initialize(player);
    }

    public override void OnExit()
    {
        Unregister();
    }

    private void Unregister()
    {
        canvas_MobileInput.Close();
    }

    private void OnHookInput(int dir)
    {
        player.ShootHook(dir);
    }

    private void OnJumpInput(int dir)
    {
        player.Jump(dir);
    }
}
