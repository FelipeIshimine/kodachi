﻿using ScreenTransitionSystem;
using System;

internal class LevelSelectionState : GameStateWithAddressableAssets
{
    private Action goToMenu;
    private Action<int,int> goToLevel;
    private Canvas_LevelSelection Canvas_LevelSelection;

    public LevelSelectionState(Action goToMenu, Action<int,int> goToLevel)
    {
        this.goToMenu = goToMenu;
        this.goToLevel = goToLevel;
    }

    public override void OnEnter()
    {
        AssignPrefabTo(out Canvas_LevelSelection);
        Canvas_LevelSelection.Initialize(OnLevelSelected);
        ScreenTransition.In();
    }

    private void OnLevelSelected(int chapterIndex, int localLevelIndex)
    {
        goToLevel.Invoke(chapterIndex, localLevelIndex);
    }

    public override void OnExit()
    {
    }
}