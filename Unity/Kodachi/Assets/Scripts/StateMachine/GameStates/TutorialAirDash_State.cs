﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using ScreenTransitionSystem;

internal class TutorialAirDash_State : GamePlayState
{
    TutorialStage_Jump tutorialStage;
    Canvas_GrandMasterDialog Canvas_GrandMasterDialog;

    private bool reachedHaft = false;

    public TutorialAirDash_State(Action OnBackToMenuCallback) : base(OnBackToMenuCallback)
    {

    }

    public override void OnEnter()
    {
        base.OnEnter();
        AssignPrefabTo(out tutorialStage);
        AssignPrefabTo(out Canvas_GrandMasterDialog);
        tutorialStage.Initialize(FirstCheckPoint, OnPlayerReachedEnd);
        Canvas_MobileInput.ShowButtons(true, false);
        Player.SetEnable(false, false, false);
        Canvas_PauseManu.gameObject.SetActive(false);
        ScreenTransition.In(() => Canvas_GrandMasterDialog.Open(StartFirstDialog));
    }

    private void StartTutorial()
    {
        Canvas_PauseManu.gameObject.SetActive(true);
        Player.transform.position = Stage.PlayerSpawnPoint.position;
        Player.gameObject.SetActive(true);
        StartGamePlay();
        tutorialStage.DissapearFirst();
        Canvas_MobileInput.Open();
        Player.Initilize(true, false, false, true, false);
        Player.gameObject.SetActive(true);
    }

    public void FirstCheckPoint()
    {
        Canvas_MobileInput.Close();
        Pause();
        Canvas_GrandMasterDialog.Open(StartSecondDialog);
    }

    private void ContinueTutorial()
    {
        tutorialStage.DissapearSecond();
        Canvas_MobileInput.Open();
        Resume();
        Player.Initilize(true, false, true, true, false);
    }

    private void OnPlayerReachedEnd()
    {
        Canvas_GrandMasterDialog.Open(StartThirdDialog);
        Pause();
    }

    public override void OnExit()
    {
    }

    #region Dialogs
    private void StartFirstDialog()
    {
        Canvas_GrandMasterDialog.ShowText(
            new string[] 
            {
                "Well, time for your second lesson",
                "Just like yesterday, you will have to reach the top",
                "But beware, this mountain is full of dangers",
                "So being a Monkey will not be enought",
                "Today, you learn the way of the Eagle",
                "Today you will learn how to FLY...",
                "...",
                "Well... Not ACTUALLY flying... More like a short glide",
                "Nevertheless... this will be one of your most important tools",
                "So listen and focus",
                "For this, you just need to jump like before",
                "but now, when you are in mid air",
                "channel your inner energy and realise it while kicking the air!",
                "Easy as that...",
                "See you at the top young one",
                "[ After you Jump, press the Jump button again to DASH mid air ]",
            },
            4,
            4,
            ()=> Canvas_GrandMasterDialog.Close(StartTutorial));
    }

    private void StartSecondDialog()
    {
        Canvas_GrandMasterDialog.ShowText(
             new string[]
             {
                "So.. It seems that you can really move like a monkey,",
                "not only look like one",
                "Good, really good",
                "Now the Way of the Eagle",
                "Jump like a Monkey, and when you are ready",
                "Channel your inner energy, kick the air!",
                 "and dash through it like an Eagle",
                "Simple enough",
                "See you at the top",
                "[ After you Jump, press the Jump button again to DASH mid air ]",
             },
             4,
             float.MaxValue,
             () => Canvas_GrandMasterDialog.Close(ContinueTutorial));
    }


    private void StartThirdDialog()
    {
        Canvas_GrandMasterDialog.ShowText(
             new string[]
             {
                "Nicely done",
                "I was starting to get worry that you died down there",
                "Well, not worry...",
                "More like annoyed.",
                "See you tomorrow morning, and we will pulish those skills",
             },
             4,
             4,
             () => Canvas_GrandMasterDialog.Close(BackToMenu));
    }
    #endregion

    protected override void OnPlayerDead()
    {
        gameStateProxy.WaitAndExecute(3.5f,
          () =>
          {
              if(reachedHaft)
              {
                  Player.Initilize(true, false, true, true, false);
              }
              else
              {
                  Player.transform.position = GetPlayerRespawnPosition();
                  Player.Initilize(true, false, false, true, false);
              }
          });
    }

}
