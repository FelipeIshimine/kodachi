﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookKunai : MonoBehaviour
{
    [SerializeField] private Player player;
    public Player Owner => player;
    public bool slowMoWhenCancelled = true;

    private void Awake()
    {
        if (player == null)
            player = GetComponentInParent<Player>();
    }

    public void Cancel()
    {
        player.CancelHook();
        if (slowMoWhenCancelled)
        {
            TimeManager._StartTimeShift(.1f, .15f, true, false); ;
        }
    }
}
