﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class LoopedStage : MonoBehaviour
{
    [SerializeField] private List<GameObject> DefaultModules;
    private List<GameObject> activeModulesGameObjects = new List<GameObject>();

    [SerializeField, ReadOnly] private Transform currentTopPosition;
    [SerializeField,ReadOnly] private List<LoopedTerrainModule> activeModules;
    [SerializeField, ReadOnly] private List<LoopedTerrainModule> orderedModules;

    private Player player;

    public void Initialize(Player player)
    {
        activeModulesGameObjects.DestroyContent();
        activeModules = new List<LoopedTerrainModule>();
        for (int i = 0; i < DefaultModules.Count; i++)
        {
            activeModulesGameObjects.Add(Instantiate(DefaultModules[i], transform));
            activeModules.Add(activeModulesGameObjects[i].GetComponent<LoopedTerrainModule>());
        }
        Initialize(activeModules, player);
    }

    public void Initialize(List<LoopedTerrainModule> modules, Player player)
    {
        this.player = player;
        activeModules = modules;
        orderedModules = new List<LoopedTerrainModule>(activeModules);
        orderedModules.Reverse();
        activeModules[0].transform.position = Vector2.zero;

        float height = activeModules[0].height;
        for (int i = 1; i < activeModules.Count; i++)
        {
            height += activeModules[i].height;
            activeModules[i].SetPositionAt(activeModules[i - 1].TopPosition, true);
            activeModules[i].transform.SetParent(transform);
        }

        Vector2 startPosition = activeModules[0].GetPlayerStartPosition();
        player.transform.position = startPosition;
        player.Initilize(true, true, true, true, true, true);
    }

    [Button]
    public void LoopNext()
    {
        LoopedTerrainModule lastModule = orderedModules[orderedModules.Count - 1];
        lastModule.SetPositionAt(orderedModules[0].TopPosition, true);
        orderedModules.Insert(0, lastModule);
        orderedModules.RemoveAt(orderedModules.Count - 1);
    }

    public void FixedUpdate()
    {
        if (player == null)
            return;
        if(player.transform.position.y >= orderedModules[1].transform.position.y)
            LoopNext();
    }
}
