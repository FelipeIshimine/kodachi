﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.Tilemaps;
using Sirenix.OdinInspector;
using System;

public class LoopedTerrainModule : MonoBehaviour
{
    public float height;
    [SerializeField] private Transform topPosition;
    [SerializeField] public Transform botPosition;
    [SerializeField] public Transform playerStartPosition;

    public CompositeCollider2D compositeCollider2D;
    public TilemapCollider2D tilemapCollider;

    public Vector2 TopPosition { get => topPosition.position; private set => topPosition.position = value; }

    [Button]
    public void UpdatePosition()
    {
        Debug.Log($"Center: {compositeCollider2D.bounds.center}");
        topPosition.position =  Vector3.up * (compositeCollider2D.bounds.extents.y + compositeCollider2D.bounds.center.y);
        botPosition.position = Vector3.up * (-compositeCollider2D.bounds.extents.y + compositeCollider2D.bounds.center.y);
        height = compositeCollider2D.bounds.size.y;
    }

    internal Vector2 GetPlayerStartPosition()
    {
        return (playerStartPosition == null) ? compositeCollider2D.bounds.center : playerStartPosition.position;
    }

    public void SetPositionAt(Vector2 startPosition, bool setAsActive)
    {
        transform.position = startPosition + Vector2.up * Vector2.Distance(botPosition.position, transform.position);
        if (setAsActive) gameObject.SetActive(true);
    }

    private void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
