﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideSpot : MonoBehaviour
{
    [SerializeField] SquashAnimation squashAnimation = null;
    [SerializeField] Transform hidePosition = null;
    public Vector2 HidePosition => hidePosition.position;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player p = null;
        if(collision.attachedRigidbody != null)
            p = collision.attachedRigidbody.GetComponentInParent<Player>();

        if (p != null && !p.IsHooked && !p.IsHookTravelling)
        {
            p.HideInSpot(HidePosition);
            squashAnimation?.Play();
        }
    }
}
