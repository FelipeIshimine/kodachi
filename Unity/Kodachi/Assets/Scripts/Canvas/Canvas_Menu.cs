﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Canvas_Menu : MonoBehaviour
{
    Action onExit;
    Action onTutorialPressed;
    Action OnDashTutorialPressed;
    Action OnTestMissionPressed;
    Action onBossFightTest;
    Action onTestLoopedStage;
    Action OnTestCinematic;

    public void Initialize(
        Action onExit,
        Action onTutorialPressed, 
        Action OnDashTutorialPressed, 
        Action OnTestMissionPressed, 
        Action onBossFightTest, 
        Action onTestLoopedStage,
        Action OnTestCinematic)
    {
        this.onExit = onExit;
        this.onTutorialPressed = onTutorialPressed;
        this.OnDashTutorialPressed = OnDashTutorialPressed;
        this.OnTestMissionPressed = OnTestMissionPressed;
        this.onBossFightTest = onBossFightTest;
        this.onTestLoopedStage = onTestLoopedStage;
        this.OnTestCinematic = OnTestCinematic;
    }

    public void StartPressed()
    {
        onExit?.Invoke();
    }

    public void Tutorial_JumpPressed()
    {
        onTutorialPressed?.Invoke();
    }

    public void Tutorial_DashPressed()
    {
        OnDashTutorialPressed?.Invoke();
    }

    public void TestMission()
    {
        OnTestMissionPressed.Invoke();
    }

    public void TestBossFight()
    {
        onBossFightTest.Invoke();
    }

    public void TestLoopedStage()
    {
        onTestLoopedStage.Invoke();
    }

    public void TestCinematicStage()
    {
        OnTestCinematic.Invoke();
    }

    private void OnDestroy()
    {
        this.onExit = null;
        this.onTutorialPressed = null;
        this.OnDashTutorialPressed = null;
        this.OnTestMissionPressed = null;
        this.onBossFightTest = null;
        this.onTestLoopedStage = null;
        OnTestCinematic = null;
    }
}
