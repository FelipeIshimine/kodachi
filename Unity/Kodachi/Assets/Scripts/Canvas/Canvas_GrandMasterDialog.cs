﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_GrandMasterDialog : MonoBehaviour
{
    public AnimatedContainer conteiner;
    public AnimatedContainer grandMasterConteiner;
    public AnimatedContainer animatedTextConteiner;
    public AnimatedTextFill animatedText;
    public Button button;
    public Image render;
    public IEnumerator showTextRutine;

    public bool pauseAtDialogStart = true;

    public void Open(Action callback)
    {
        if (pauseAtDialogStart) TimeManager.Pause();

        conteiner.Open();
        grandMasterConteiner.Open(callback);
        animatedTextConteiner.Open();
    }

    public void Close(Action callback)
    {
        animatedText.Clear();
        button.gameObject.SetActive(false);
        conteiner.Close();
        grandMasterConteiner.Close(
            ()=>
            {
                if (pauseAtDialogStart) TimeManager.Resume();
                callback?.Invoke();
            });
        animatedTextConteiner.Close();
    }

    public void ShowText(string nText, float endWait, Action onDoneCallback = null)
    {
        button.gameObject.SetActive(true);
        animatedText.StartTextAnimation(nText, endWait, onDoneCallback);
    }

    public void ShowText(string[] nText, float waitBetweenTexts, float endWait, Action onDoneCallback = null)
    {
        showTextRutine = CreateShowTextRutine(nText, waitBetweenTexts, endWait, onDoneCallback);
        NextText();
    }

    internal void SetSprite(Sprite image)
    {
        render.sprite = image;
        render.color = (image == null)?Color.clear:Color.white;
    }

    private IEnumerator CreateShowTextRutine(string[] nText,float waitBetweenTexts, float endWait, Action onDoneCallback)
    {
        for (int i = 0; i < nText.Length; i++)
        {
            button.gameObject.SetActive(true);
            animatedText.StartTextAnimation(nText[i], (i==nText.Length-1)? endWait: waitBetweenTexts, NextText);
            yield return null;
        }
        onDoneCallback?.Invoke();
    }

    private void NextText()
    {
        showTextRutine.MoveNext();
    }

    public void Pressed()
    {
        animatedText.Skip();
    }
}
