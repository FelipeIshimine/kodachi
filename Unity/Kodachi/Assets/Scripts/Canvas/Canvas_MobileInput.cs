﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Canvas_MobileInput : MonoBehaviour
{
    Action<int> OnJump;
    Action<int> OnHook;
    PlayerInput playerInput;
    public AnimatedContainer AnimatedContainer;

    public GameObject[] jumpButtons;
    public GameObject[] hookButtons;

    public Shine_Img[] jumpShine;
    public Shine_Img[] hookShine;

    [Flags] public enum Buttons { None=0, LeftJump = 1, RightJump = 2, LeftHook = 4, RightHook = 8}

    internal void Initialize(Player player)
    {
        this.playerInput = player.GetComponent<PlayerInput>();
        OnJump += playerInput.Jump;
        OnHook += playerInput.Hook;
    }

    internal void ShowButtons(bool showJumpButtons, bool showHookButtons)
    {
        AnimatedContainer.Hide();
        SetHookButtons(showHookButtons);
        SetJumpButtons(showJumpButtons);
    }

    public void PlayShine(Buttons buttons)
    {
        if (buttons.HasFlag(Buttons.LeftHook))
            hookShine[0].Shine();
        if (buttons.HasFlag(Buttons.RightHook))
            hookShine[1].Shine();
        if (buttons.HasFlag(Buttons.LeftJump))
            jumpShine[0].Shine();
        if (buttons.HasFlag(Buttons.RightJump))
            jumpShine[1].Shine();
    }

    public void StopShine()
    {
        hookShine[0].Stop();
        hookShine[1].Stop();
        jumpShine[0].Stop();
        jumpShine[1].Stop();
    }

    public void Close() => AnimatedContainer.Close();
    public void Open() => AnimatedContainer.Open();

    public void SetHookButtons(bool value)
    {
        for (int i = 0; i < hookButtons.Length; i++)
            hookButtons[i].gameObject.SetActive(value);
    }

    public void SetJumpButtons(bool value)
    {
        for (int i = 0; i < jumpButtons.Length; i++)
            jumpButtons[i].gameObject.SetActive(value);
    }

    public void Jump(int dir)
    {
        OnJump.Invoke(dir);
    }

    public void Hook(int dir)
    {
        OnHook.Invoke(dir);
    }

    internal void Finish()
    {
        OnJump -= playerInput.Jump;
        OnHook -= playerInput.Hook;
    }
}
