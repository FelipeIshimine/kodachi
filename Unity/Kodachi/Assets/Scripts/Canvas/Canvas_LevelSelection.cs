﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_LevelSelection : MonoBehaviour
{
    public Canvas_PopUpMessage canvas_PopUpMessage;
    public ButtonConteiner buttonConteiner;

    public Text title;

    GameChapter CurrentChapter;

    public int Index { get; private set; } = -1;
    Action<int,int> OnLevelSelected;

    public AnimatedContainer masterConteiner;
    public AnimatedContainer AnimatedContainer;

    private bool isLocked = false;

    private void OnEnable()
    {
        if(canvas_PopUpMessage == null)
            canvas_PopUpMessage = FindObjectOfType<Canvas_PopUpMessage>();
    }

    public void Initialize(Action<int, int> OnLevelSelected)
    {
        this.OnLevelSelected = OnLevelSelected;
        Index = -1;
        isLocked = true;
        AnimatedContainer.Hide();
        masterConteiner.Open(
            ()=>
            {
                isLocked = false;
                NextPage();
            });
        
    }
   
    public void NextPage()
    {
        if (isLocked) return;
        Index = Index.NextIndex(GameStructure.Chapters.Count);
        LoadPage(Index);
    }

    public void PreviousPage()
    {
        if (isLocked) return;
        Index = Index.NextIndex(GameStructure.Chapters.Count);
        LoadPage(Index);
    }

    public void LoadPage(int index)
    {
        if (isLocked) return;
        isLocked = true;
        AnimatedContainer.Close(
            ()=>
            {
                CurrentChapter = GameStructure.Chapters[index];
                buttonConteiner.Initialize(
                    CurrentChapter.Sequences.Count,
                    index,
                    NextPage,
                    PreviousPage,
                    OnLevelSelectedCallback);
                title.text = CurrentChapter.name;
                AnimatedContainer.Open(()=> isLocked = false);
            }
            );
    }

    private void OnLevelSelectedCallback(int chapterIndex, int sequenceIndex)
    {
        GameStructure.Chapters[chapterIndex].Sequences[sequenceIndex].GoToState();
    }
}
