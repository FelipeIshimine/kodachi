﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_UnitAlarm : MonoBehaviour
{
    public BaseEnemyUnitMonoBehaviour owner;
    public Action OnRaiseAlarm;

    public Image outlineRender;
    public Image fillRender;

    public Color alarmColor = Color.red;
    public float timeToRaiseAlarm = 2;
    private float currentTime = 0;
    private bool playing = false;

    IEnumerator alarmRaisedRutine;

    public AnimationCurve sizeCurve;
    public AnimationCurve alphaCurve;
    public float animationDuration = .5f;

    private void Awake()
    {
        if (owner == null)
            owner = GetComponentInParent<BaseEnemyUnitMonoBehaviour>();

        Hide();
    }

    public void Show()
    {
        outlineRender.gameObject.SetActive(true);
        fillRender.gameObject.SetActive(true);
        fillRender.fillAmount = 0;
    }

    public void Hide()
    {
        outlineRender.gameObject.SetActive(false);
        fillRender.gameObject.SetActive(false);
    }

    public void StartCounter()
    {
        if (!playing)
        {
            Show();
            playing = true;
            currentTime = 0;
        }
    }

    public void StopCounter()
    {
        playing = false;
    }

    public void FixedUpdate()
    {
        if(playing)
        {
            currentTime += Time.deltaTime / timeToRaiseAlarm;
            fillRender.fillAmount = currentTime;
            if (currentTime >= 1)
                RaiseAlarm();
        }
        else
        {
            currentTime -= Time.deltaTime / timeToRaiseAlarm;
            fillRender.fillAmount = currentTime;
            if (currentTime >= 0)
                Hide();
        }

    }

    private void RaiseAlarm()
    {
        OnRaiseAlarm?.Invoke();
        StartCoroutine(AlarmRaisedRutine());
    }

    IEnumerator AlarmRaisedRutine()
    {
        float t = 0;
        Color startColorFill = alarmColor;
        outlineRender.gameObject.SetActive(false);
        Color clearFill = startColorFill;
        clearFill.a = 0;
        do
        {
            t += Time.deltaTime / animationDuration;
            fillRender.color = Color.Lerp(startColorFill, clearFill, t);
            fillRender.rectTransform.localScale = (1 + sizeCurve.Evaluate(t)) * Vector2.one;
            yield return null;
        } while (t<1);
        Hide();
    }
}
