﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    private int chapterId;
    public int index;
    public Button button;
    public Action<int, int> OnPressed;

    public void Initialize(int chapterId, int index, Action<int, int> OnPressed)
    {
        this.chapterId = chapterId;
        this.index = index;
        this.OnPressed = OnPressed;
    }

    public void Pressed()
    {
        OnPressed.Invoke(chapterId, index);
    }
}
