﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_LoadingBar : MonoBehaviour
{
    public AnimatedContainer AnimatedContainer;
    public static UI_LoadingBar instance;

    public Slider Slider;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("SINGLETON FAIL");
            return;
        }
        instance = this;
    }

    public void Show() => AnimatedContainer.Show();
    public void Hide() => AnimatedContainer.Hide();
    public void Open() => AnimatedContainer.Open();
    public void Close() => AnimatedContainer.Close();

    private void Start()
    {
        Hide();
    }
    public void UpdateBar(float value)
    {
        Slider.value = value;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
