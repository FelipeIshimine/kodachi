﻿using Pooling.Poolers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonConteiner : MonoBehaviour
{
    public SetPooler pool;

    private List<LevelButton> activeButtons = new List<LevelButton>();
    Action OnNextScreen, OnPreviousScreen;
    Action<int, int> OnLevelSelected;

    public void Initialize(int buttonsCount, int chapterId, Action OnNextScreen, Action OnPreviousScreen, Action<int,int> OnLevelSelected)
    {
        this.OnNextScreen = OnNextScreen;
        this.OnPreviousScreen = OnPreviousScreen;
        this.OnLevelSelected = OnLevelSelected;
        CreateButtons(buttonsCount, chapterId);
    }

    private void CreateButtons(int buttonsCount, int chapterId)
    {
        Clear();
        for (int i = 0; i < buttonsCount; i++)
        {
            LevelButton levelButton = pool.Dequeue().GetComponent<LevelButton>();
            activeButtons.Add(levelButton);
            levelButton.gameObject.SetActive(true);
            levelButton.Initialize(chapterId, i, OnPressed);
            levelButton.transform.SetParent(transform);
            levelButton.transform.localScale = Vector3.one;
        }
    }

    private void OnPressed(int chapterId, int localLevelIndex)
    {
        OnLevelSelected.Invoke(chapterId, localLevelIndex);
    }

    private void Clear()
    {
        for (int i = activeButtons.Count-1; i >= 0; i--)
            pool.Enqueue(activeButtons[i].GetComponent<Poolable>());
    }

    private void OnDestroy()
    {
        this.OnNextScreen = null;
        this.OnPreviousScreen = null;
        this.OnLevelSelected = null;
    }
}
