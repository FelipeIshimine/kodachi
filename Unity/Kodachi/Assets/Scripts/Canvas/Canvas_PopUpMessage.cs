﻿using UnityEngine;
using UnityEngine.UI;

public class Canvas_PopUpMessage : MonoBehaviour
{
    public Image bg;
    public AnimatedContainer conteiner;
    public Text text;
    public Button button;

    private void Awake()
    {
        button.interactable = false;
    }
    internal void ShowMessage(string v)
    {
        bg.gameObject.SetActive(true);
        text.text = v;
        conteiner.Open();
        button.interactable = true;
    }

    public void Pressed()
    {
        button.interactable = false;
        conteiner.Close(()=> bg.gameObject.SetActive(false));
    }

}
