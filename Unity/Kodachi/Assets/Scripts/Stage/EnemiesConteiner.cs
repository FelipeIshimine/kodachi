﻿using System.Collections.Generic;
using UnityEngine;

public class EnemiesConteiner : MonoBehaviour
{
    private List<BaseEnemyUnitMonoBehaviour> enemies = null;
    internal List<BaseEnemyUnitMonoBehaviour> GetEnemies()
    {
        if (enemies == null)
        {
            enemies = new List<BaseEnemyUnitMonoBehaviour>();
            GetComponentsInChildren<BaseEnemyUnitMonoBehaviour>(enemies);
        }
        return enemies;
    }
}
