﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceStage : Stage
{
    private void Awake()
    {
        endTrigger.OnEnter += OnEnter;
    }

    private void OnDestroy()
    {
        endTrigger.OnEnter -= OnEnter;
    }

    private void OnEnter(Collider2D obj)
    {
        Player player = obj.GetComponentInParent<Player>();
        if (player != null)
        {
            player.SetEnable(false, false, false);
            OnPlayerReachedEnd.Invoke();
        }
    }


}
