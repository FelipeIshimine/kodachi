﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    [SerializeField] private PolygonCollider2D confiner = null; public PolygonCollider2D Confiner => confiner;
     [SerializeField] private Transform playerSpawnPoint = null;
    [SerializeField] private CheckpointConteiner checkpointConteiner = null;
    [SerializeField] private EnemiesConteiner enemiesConteiner = null;

    public Transform PlayerSpawnPoint => playerSpawnPoint;
    public Vector2 PlayerSpawnPosition => playerSpawnPoint.position;

    public ExternalTrigger endTrigger;
    public Action OnPlayerReachedEnd;
    public float currentHeight;

    private void OnDestroy()
    {
        endTrigger.OnEnter -= OnEnter;
    }

    private void OnEnter(Collider2D obj)
    {
        Player player = null;

        if(obj.attachedRigidbody != null)
            player = obj.attachedRigidbody.GetComponentInParent<Player>();

        if (player != null)
        {
            player.SetEnable(false, false, false);
            OnPlayerReachedEnd.Invoke();
        }
    }

    private void OnValidate()
    {
        if (checkpointConteiner == null)
            checkpointConteiner = GetComponentInChildren<CheckpointConteiner>();
    }

    public List<Checkpoint> GetSortedCheckpoints() => checkpointConteiner.GetSortedCheckpoints();

    internal void Initialize()
    {
        endTrigger.OnEnter += OnEnter;
    }

    public List<BaseEnemyUnitMonoBehaviour> GetEnemies() => enemiesConteiner.GetEnemies();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(playerSpawnPoint.position, new Vector3(1,1));
    }
}
