﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class BossNaginataImpact : MonoBehaviour
{
    public LayerMask targetLayer;
    public Animator anim;
    public Transform visual;
    [System.Serializable]
    public enum AnimationDirection { Left, Right}
    public ExternalTrigger externalTrigger;

    private void Awake()
    {
        externalTrigger.OnEnter += OnExternalTriggerEnter;
    }

    private void OnDestroy()
    {
        externalTrigger.OnEnter -= OnExternalTriggerEnter;
    }

    private void OnExternalTriggerEnter(Collider2D collision)
    {
        if (targetLayer.Contains(collision.gameObject.layer))
        {
            IHaveHealth haveHealth = collision.attachedRigidbody.GetComponent<IHaveHealth>();
            haveHealth.Damage(haveHealth.Max);
        }
    }

    public void PlayAnimation_RightWall() =>PlayWallAnimation(AnimationDirection.Right);
    public void PlayAnimation_LeftWall() => PlayWallAnimation(AnimationDirection.Left);

    [Button]
    public void PlayWallAnimation(AnimationDirection direction)
    {
        if (IsPlaying()) return;

        visual.transform.localScale = new Vector3((direction == AnimationDirection.Left) ? -1 : 1, 1, 1);
        anim.SetTrigger("Wall");
    }

    bool IsPlaying()
    {
        return anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1;
    }

    [Button]
    public void PlayFloorAnimation()
    {
        if (IsPlaying()) return;
        anim.SetTrigger("Floor");
    }
}