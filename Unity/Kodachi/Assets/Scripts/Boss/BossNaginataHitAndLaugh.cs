﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Health))]
public class BossNaginataHitAndLaugh : MonoBehaviour
{
    public TimelinePlayerTrigger playerTrigger;
    private bool ready = true;
    public Action OnDone;
    public Animator animator;
    public Health health;
    public Collider2D col;

    private void OnValidate()
    {
        if (animator == null)
            animator = GetComponent<Animator>();

        if (health == null)
            health = GetComponent<Health>();
    }

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        col.enabled = true;
        health.Initialize(1);
        health.OnDead = OnDead;
        health.OnDamageReceived = OnDamageReceived;
    }

    private void OnDestroy()
    {
        health.OnDead -= OnDead;
        health.OnDamageReceived -= OnDamageReceived;
    }

    private void OnDead(Health h)
    {
        col.enabled = false;
        playerTrigger.BindPlayerAndPlayCutscene(FindObjectOfType<Player>());
    }

    private void OnDamageReceived(float obj)
    {
        Hit();
        col.enabled = false;
    }

    [Button]
    public void Appear()
    {
        if (!ready) return;
        ready = false;
        animator.SetTrigger("Appear");
    }

    public void Disappear()
    {
        animator.SetTrigger("Disappear");
    }

    public void Hit()
    {
        col.enabled = false;
        animator.SetTrigger("Hit");
        TimeManager._StartDefaultSlowMotion();
    }

    public void Done()
    {
        OnDone?.Invoke();
        ready = true;
    }
}
