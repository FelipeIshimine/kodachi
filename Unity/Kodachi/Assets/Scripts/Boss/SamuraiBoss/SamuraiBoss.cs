﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class SamuraiBoss : MonoBehaviourStateMachine
{
    public Health health;

    [TabGroup("Attack_A")] public float aTelegraphDuration = 5;
    [TabGroup("Attack_A")] public float aAttackDuration = 5;
    [TabGroup("Attack_A")] public float aRecoverDuration = 5;
    [TabGroup("Attack_A")] public float aTelegraphDashSpeed = 15;
    [TabGroup("Attack_A")] public float aDashSpeed = 15;
    [TabGroup("Attack_A")] public float waitBeforeAttack = 2;
    [TabGroup("Attack_A")] public float lineMultiplier = .2f;
    [TabGroup("Attack_A")] public float lineSizeTelegraphDuration = .3f;
    [TabGroup("Attack_A")] public AnimationCurve lineTelegraphSizeCurve;

    [TabGroup("Attack_A")] public Transform[] dashPatternConteiners;
    [TabGroup("Attack_A")] public AnimationCurve dashSpeedCurve;
    private FromToPoints[][] dashPatterns;

    [TabGroup("Attack_B")] public float bTelegraphDuration = 5;
    [TabGroup("Attack_B")] public float bAttackDuration = 5;
    [TabGroup("Attack_B")] public float bRecoverDuration = 5;
    [TabGroup("Attack_B")] public float movementPredictionLenght = .15f;
    [TabGroup("Attack_B")] public float bDashLenght = 20;
    [TabGroup("Attack_B")] public float bTargetingDuration = 2;

    [TabGroup("Attack_C")] public float cTelegraphDuration = 5;
    [TabGroup("Attack_C")] public float cAttackDuration = 5;
    [TabGroup("Attack_C")] public float cRecoverDuration = 5;

    [TabGroup("Disappear")] public float disappearDuration = .6f;

    [TabGroup("Damaged")] public float damagedFlashDuration = 1;


    public float jumpSpeed = 30;
    public float jumpHeight = 6;
    public AnimationCurve jumpSpeedCurve = new AnimationCurve();
    public AnimationCurve jumpHeightCurve = new AnimationCurve();
    public Transform[] prepareAttackPositions;

    public LineRenderer[] lineRenders;
    public float dashTelegraphDuration;
    public float prepareAttackFlashesDuration = .5f;
    public AnimationCurve prepareAttackFlashesCurve;
    public AnimationCurve prepareAttackColorCurve;

    public float pauseBetween = 3;

    public Transform characterRoot;

    public Transform[] attackSources_A;

    IEnumerator currentRutine;

    public BoxCollider2D healthCollider;
    public Unit_AttackTrigger unit_AttackTrigger;
    public SpriteRenderer bodyRender;

    [ShowInInspector] private bool Interrupted { get; set; } = false;

    private Player target;
    private Player Target
    {
        get
        {
            if (target == null)
            {
                target = FindObjectOfType<Player>();
                target.OnPlayerDead += OnTargetDead;
            }
            return target;
        }
    }

    private void Awake()
    {
        Initialize();
        health.Initialize(10);
        health.OnDamageReceived += OnDamageReceived;
        health.OnDead += OnDead;
    }

    private void OnDestroy()
    {
        health.OnDamageReceived -= OnDamageReceived;
        health.OnDead -= OnDead;
        if(target != null)
            target.OnPlayerDead -= OnTargetDead;
    }

    private void OnTargetDead()
    {
        Stop();
    }

    private void OnDamageReceived(float obj)
    {
        Interrupted = true;
    }

    public void Initialize()
    {
        dashPatterns = new FromToPoints[dashPatternConteiners.Length][];
        for (int i = 0; i < dashPatternConteiners.Length; i++)
        {
            dashPatternConteiners[i].gameObject.SetActive(true);
            dashPatterns[i] = dashPatternConteiners[i].GetComponentsInChildren<FromToPoints>();
        }
    }

    public void Play()
    {
        StartAttackA();
    }

    public void Stop()
    {
        if (currentRutine != null)
            StopCoroutine(currentRutine);

    }

    [Button]
    public void StartAttackA() => StartRutine(Attack_A_Rutine());

    [Button]
    public void StartAttackB() => StartRutine(Attack_B_Rutine());

    [Button]
    public void Interrupt()
    {
        Interrupted = true;
    }


    private void StartRutine(IEnumerator rutine)
    {
        if (currentRutine != null) StopCoroutine(currentRutine);
        currentRutine = rutine;
        StartCoroutine(currentRutine);
    }

    IEnumerator Attack_A_Rutine()
    {
        Interrupted = false;
        Debug.Log("Prepare");
        SetAttackEnable(false);
        FromToPoints[] dashPattern = dashPatterns.GetRandom();
        bodyRender.color = Color.gray;
        Vector3 preparePosition = GetClosestPreparePosition(dashPattern[0].From);
        yield return JumpToPosition(preparePosition);

        yield return TelegraphAttack_A_Rutine(dashPattern);

        if (Interrupted)
            TelegrapthInterrupted();

        Debug.Log("Attack");
       
        yield return Attack_A_DashAttackRutine(dashPattern);
       
        AttackDone();
    }


    private IEnumerator Attack_B_Rutine()
    {
        Interrupted = false;
        Debug.Log("Prepare Attack B");
        SetAttackEnable(false);
        FromToPoints[] dashPattern = dashPatterns.GetRandom();
        bodyRender.color = Color.gray;
        yield return JumpToPosition(prepareAttackPositions.GetRandom().position);

        yield return PrepareTargetDashAttack(lineRenders[0]);
        Vector2 startPos = characterRoot.position;
        Vector2  targetPos = startPos.GetDirection((Vector2)Target.transform.position) * bDashLenght;
        yield return new WaitForSeconds(waitBeforeAttack);
        SetAttackEnable(true);
        yield return ExecuteDash_Rutine(lineRenders[0], startPos, targetPos);
        SetAttackEnable(false);
        yield return JumpToPosition(prepareAttackPositions.GetRandom().position);
        AttackDone();
    }

    private IEnumerator PrepareTargetDashAttack(LineRenderer lineRenderer)
    {
        float t = 0;
        Vector2 startPos = characterRoot.position;
        Vector2 targetPos;
        lineRenderer.SetPositions(new Vector3[2] { startPos, startPos});
        do
        {
            t += Time.deltaTime / bTargetingDuration;
            targetPos = startPos.GetDirection((Vector2)Target.transform.position) * bDashLenght;
            lineRenderer.SetPosition(1, targetPos);
            yield return null;
        } while (t<1);
    }

    private IEnumerator TelegraphAttack_A_Rutine(FromToPoints[] dashPattern)
    {
        IEnumerator dashTelegraph = DrawDashLines(dashPattern, true);
        IEnumerator characterTelegraph = Character_PrepareToAttackRutine(dashPattern, true);
        bool telegraphRutineActive = false;
        bool chargingEnemyRutineActive = false;
        do
        {
            telegraphRutineActive = dashTelegraph.MoveNext();
            chargingEnemyRutineActive = characterTelegraph.MoveNext();
            yield return null;
        } while (telegraphRutineActive || chargingEnemyRutineActive);
    }

    private IEnumerator Character_PrepareToAttackRutine(FromToPoints[] dashPattern, bool isManual)
    {
        float t = 0;
        float totalTelegraphDuration = 0;
        for (int i = 0; i < dashPattern.Length; i++)
            totalTelegraphDuration += Vector2.Distance(dashPattern[i].From, dashPattern[i].To);
        totalTelegraphDuration /= aTelegraphDashSpeed;

        Color startColor = bodyRender.color;
        do
        {
            t += Time.deltaTime / totalTelegraphDuration;
            bodyRender.color = Color.Lerp(startColor, Color.blue, t);
            yield return null;
        } while (t<1 && !Interrupted);

        t = 0;
        do
        {
            t += Time.deltaTime / waitBeforeAttack;
            yield return null;
        } while (t<1 && !Interrupted);

        if (Interrupted)
        {
            if (isManual)
            {
                IEnumerator damageRutine = DamagedRutine();
                IEnumerator jumpRutine = DamagedRutine();
                bool damageRutineActive = true; 
                bool jumpRutineActive = true;

                while (damageRutineActive || jumpRutineActive)
                {
                    if (damageRutineActive) damageRutineActive = damageRutine.MoveNext();
                    if (jumpRutineActive) jumpRutineActive = jumpRutine.MoveNext();
                }
            }
            else
            {
                yield return DamagedRutine();
                yield return JumpToPosition(dashPattern[0].From);
            }
        }
    }

    private IEnumerator DamagedRutine()
    {
        float t = 0;
        Color origColor = bodyRender.color;
        do
        {
            t += Time.deltaTime / damagedFlashDuration;
            bodyRender.color = Color.Lerp(Color.white, origColor, t);
            yield return null;
        } while (t<1);  
    }

    private void SetAttackEnable(bool value)
    {
        bodyRender.color = (value)?Color.red:Color.gray;
        healthCollider.enabled = !value;
        health.Invulnerable = value;
        unit_AttackTrigger.SetActive(value);
    }

    private Vector3 GetClosestPreparePosition(Vector2 from)
    {
        Vector3 min = prepareAttackPositions[0].position;
        float mDist = float.MaxValue;
        for (int i = 0; i < prepareAttackPositions.Length; i++)
        {
            float distance = Vector2.Distance(from, prepareAttackPositions[i].position);
            if(mDist > distance)
            {
                mDist = distance;
                min = prepareAttackPositions[i].position;
            }
        }   
        return min;
    }

    private IEnumerator JumpToPosition(Vector3 targetPosition)
    {
        Vector3 startPosition = characterRoot.transform.position;
        float t = 0;
        float distance = Vector2.Distance(startPosition, targetPosition);

        if (distance == 0)
            yield break;

        float duration = distance  / jumpSpeed;
        float height = Mathf.Min(distance / 10, 1)* jumpHeight;
        do
        {
            yield return null;
            t += Time.deltaTime / duration;
            characterRoot.transform.position = Vector2.Lerp(startPosition, targetPosition, jumpSpeedCurve.Evaluate(t)) + Vector2.up * jumpHeightCurve.Evaluate(t) * height;
        } while (t < 1);

    }

    private IEnumerator DrawDashLines(FromToPoints[] fromToPoints, bool isManual)
    {
        for (int i = 0; i < fromToPoints.Length; i++)
        {
            if (isManual)
            {
                IEnumerator enumerator = TelegraphDashRutine(fromToPoints[i], lineRenders[i]);
                while (enumerator.MoveNext())
                    yield return null;
            }
            else
                yield return TelegraphDashRutine(fromToPoints[i], lineRenders[i]);
        }
    }

    private IEnumerator TelegraphDashRutine(FromToPoints pos, LineRenderer lineRenderer)
    {
        float t = 0;
        Vector2 from = pos.From;
        Vector2 to = pos.To;
        float duration = Vector2.Distance(from, to) / aTelegraphDashSpeed;
        lineRenderer.SetPositions(new Vector3[2] { from, to  });
        do
        {
            t += Time.deltaTime / duration;
            lineRenderer.SetPosition(1, Vector2.Lerp(from, to, dashSpeedCurve.Evaluate(t)));
            lineRenderer.widthMultiplier = Mathf.Lerp(0, lineMultiplier, t);
            yield return null; 
        } while (t<1);
    }

    private IEnumerator Attack_A_DashAttackRutine(FromToPoints[] dashPattern)
    {
        for (int i = 0; i < dashPattern.Length; i++)
        {
            SetAttackEnable(false);

            IEnumerator jumpRutine = JumpToPosition(dashPattern[i].From);
            IEnumerator lineRenderRutine = LineTelegraphRutine(lineRenders[i]);

            bool jumpActive = true;
            bool lineActive = true;
            do
            {
                jumpActive = jumpRutine.MoveNext();
                lineActive = lineRenderRutine.MoveNext();
                yield return null;
            } while (jumpActive || lineActive);

            SetAttackEnable(true);
            yield return ExecuteDash_Rutine(lineRenders[i], dashPattern[i]);
        }
    }

    private IEnumerator LineTelegraphRutine(LineRenderer lineRenderer)
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / lineSizeTelegraphDuration * lineMultiplier;
            lineRenderer.widthMultiplier = lineTelegraphSizeCurve.Evaluate(t) * lineMultiplier;
            yield return null;
        } while (t<1);
    }

    private IEnumerator ExecuteDash_Rutine(LineRenderer lineRenderer, FromToPoints fromToPoints)
    {
        yield return ExecuteDash_Rutine(lineRenderer, fromToPoints.From, fromToPoints.To);
    }

    private IEnumerator ExecuteDash_Rutine(LineRenderer lineRenderer, Vector2 from, Vector2 to)
    {
        float t = 0;
        float duration = Vector2.Distance(from, to) / aDashSpeed;
        do
        {
            t += Time.deltaTime / duration;
            characterRoot.transform.position = Vector2.Lerp(from, to, dashSpeedCurve.Evaluate(t));
            lineRenderer.SetPosition(0, characterRoot.transform.position);
            yield return null;
        } while (t < 1);
    }

    IEnumerator PrepareAttackFlashRutine(SpriteRenderer render) => PrepareAttackFlashRutine(new SpriteRenderer[1] { render });

    IEnumerator PrepareAttackFlashRutine(SpriteRenderer[] renders)
    {
        Debug.Log("Flash");
        Color[] colors = new Color[renders.Length];
        for (int i = 0; i < colors.Length; i++)
            colors[i] = renders[i].color;

        float t = 0;
        do
        {
            t += Time.deltaTime / prepareAttackFlashesDuration;

            for (int i = 0; i < colors.Length; i++)
                renders[i].color = Color.Lerp(colors[i], Color.white, prepareAttackFlashesCurve.Evaluate(t));

            yield return null;
        } while (t < 1);
    }

    private void TelegrapthInterrupted()
    {
        Interrupted = false;
        Debug.Log(">>>>>>>>>>   TelegrapthInterrupted");
        StartDisappear();
    }

    private void StartDisappear()
    {
    
    }

    private IEnumerator DisappearRutine()
    {
        float t = 0;
        do
        {
            t += Time.deltaTime/ disappearDuration;
            bodyRender.color = Color.Lerp(Color.white, Color.clear, t);
            yield return null;
        } while (disappearDuration < 1);
        AttackDone();
    }

    private void AttackDone() => StartRutine(PauseRutine(RandomAttack));

    private void RandomAttack()
    {
        if(UnityEngine.Random.Range(0,2) == 0)
            StartAttackB();
        else
            StartAttackA();
    }

    IEnumerator PauseRutine(Action callback)
    {
        yield return new WaitForSeconds(pauseBetween);
        PauseDone();
        callback.Invoke();
    }

    private void PauseDone()
    {
    }

    public void Attack_B()
    {

    }

    public void Attack_C()
    {

    }

    public void Attack_D()
    {

    }

    private void OnDead(Health obj)
    {
        Stop();
        Defeated();
    }

    private void Defeated()
    {
        throw new NotImplementedException();
    }
}

public class MonoBehaviourStateMachine : MonoBehaviour, IStateMachine
{
    #region IStateMachine
    public IState CurrentState => MonoBehaviourState.CurrentState;
    public void SwitchState(IState nState) => MonoBehaviourState.SwitchState(nState);
    protected MonoBehaviourState MonoBehaviourState;
    #endregion
}
