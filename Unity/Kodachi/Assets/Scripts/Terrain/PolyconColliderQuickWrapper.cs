﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PolyconColliderQuickWrapper : MonoBehaviour
{
    public LayerMask targetMask;
    public PolygonCollider2D col;

    [Button]
    public void Wrap()  
    {
        if (col == null)
            col = GetComponent<PolygonCollider2D>();

        List<Collider2D> colliders = new List<Collider2D>();
        GetComponentsInChildren(colliders);

        for (int i = 0; i < colliders.Count; i++)
        {
            if (colliders[i].isTrigger || !targetMask.Contains(colliders[i].gameObject.layer))
                colliders.RemoveAt(i);
        }

        if (colliders.Count == 0)
        {
            Debug.LogError("Colliders 2D not found");
            return;
        }

        Bounds nBounds = new Bounds();
        nBounds.Encapsulate(colliders[0].bounds);

        if (colliders.Count > 2)
            for (int i = 1; i < colliders.Count; i++)
                nBounds.Encapsulate(colliders[i].bounds);

        col.SetPath(0, new Vector2[]
        {
            new Vector2(nBounds.min.x, nBounds.min.y),
            new Vector2(nBounds.min.x, nBounds.max.y),
            new Vector2(nBounds.max.x, nBounds.max.y),
            new Vector2(nBounds.max.x, nBounds.min.y)
        });

    }
}
