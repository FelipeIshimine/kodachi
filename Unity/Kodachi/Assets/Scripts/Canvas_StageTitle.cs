﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_StageTitle : MonoBehaviour
{
    public AnimatedContainer animatedContainer;
    public Text txt;
    public float waitTime = 3;

    ChargeAndExecute chargeAndExecute;

    private void Awake()
    {
        chargeAndExecute = new ChargeAndExecute(this);
    }

    public void Open(string text)
    {
        txt.text = text;
        animatedContainer.Open();
        chargeAndExecute.StartCharge(waitTime, ChargeAndExecute.TimeScale.Unscaled, null, Close);
    }

    public void Close()
    {
        animatedContainer.Close();
    }

}
