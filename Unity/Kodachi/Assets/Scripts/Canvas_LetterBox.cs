﻿using UnityEngine;

public class Canvas_LetterBox : MonoBehaviour
{
    public AnimatedContainer top;
    public AnimatedContainer bottom;

    public void Show()
    {
        top.Show();
        bottom.Show();
    }
    public void Hide()
    {
        top.Hide();
        bottom.Hide();
    }
    public void Open()
    {
        top.Open();
        bottom.Open();
    }
    public void Close()
    {
        top.Close();
        bottom.Close();
    }
}