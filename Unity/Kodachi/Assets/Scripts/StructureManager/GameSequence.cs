﻿using UnityEngine;

[System.Serializable]
public class GameSequence
{
    public string name;
    public enum GameSequenceType { Cinematic, Level }
    public GameSequenceType type;

    [System.NonSerialized] public GameChapter Chapter;

    public int Index;
    public int GlobalIndex { get; set; }

    public void GoToState()
    {
        switch (type)
        {
            case GameSequenceType.Cinematic:
                RootState.GoToCinematic(this);
                break;
            case GameSequenceType.Level:
                RootState.GoToLevel(this);
                break;
            default:
                throw new System.Exception("Tipo de GameSequence invalida");
        }
    }
    internal GameSequence GetNext() => GameStructure.GetSequenceAfter(this);
}


