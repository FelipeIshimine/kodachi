﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameChapter
{
    public string name;
    [SerializeField] private List<GameSequence> sequences;
    [HideInInspector] public IReadOnlyList<GameSequence> Sequences => sequences;

    public GameSequence this[int index] => sequences[index];

    public object Index { get; internal set; }

    internal void LinkSequences()
    {
        for (int i = 0; i < sequences.Count; i++)
        {
            sequences[i].GlobalIndex = i;
            sequences[i].Chapter = this;
        }
    }
}
