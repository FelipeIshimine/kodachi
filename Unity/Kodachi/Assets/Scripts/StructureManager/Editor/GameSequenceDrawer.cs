﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(GameSequence))]
public class GameSequenceDrawer : PropertyDrawer
{


    // Draw the property inside the given rect
    public override void OnGUI(Rect mainRect, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(mainRect, label, property);

        EditorGUILayout.BeginHorizontal();

        // Draw label
        mainRect = EditorGUI.PrefixLabel(mainRect, GUIUtility.GetControlID(FocusType.Passive), label);

        mainRect.x /= 3;

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        Vector2 offset = Vector2.zero;
        Vector2 size;

        size = new Vector2(mainRect.width * .26f, mainRect.height);
        Rect nameRect = new Rect(mainRect.position + offset, size);
        offset += Vector2.right * size.x;
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);

        size = new Vector2(mainRect.width * .2f, mainRect.height);
        Rect typeRect = new Rect(mainRect.position + offset, size);
        offset += Vector2.right * size.x;

        SerializedProperty serializedType = property.FindPropertyRelative("type");
        GameSequence.GameSequenceType gameSequenceType = (GameSequence.GameSequenceType)serializedType.enumValueIndex;
        EditorGUI.PropertyField(typeRect, serializedType, GUIContent.none);

        size = new Vector2(mainRect.width * .54f, mainRect.height);
        Rect indexRect = new Rect(mainRect.position + offset, size);
        offset += Vector2.right * size.x;
        int index = property.FindPropertyRelative("Index").intValue;

        switch (gameSequenceType)
        {
            case GameSequence.GameSequenceType.Cinematic:
                index = EditorGUILayout.Popup(index, CinematicManager.NameList);
                break;
            case GameSequence.GameSequenceType.Level:
                index = EditorGUILayout.Popup(index, LevelsManager.NameList);
                break;
        }

        property.FindPropertyRelative("Index").intValue = index;

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUILayout.EndHorizontal();


        EditorGUI.EndProperty();
    }
}
