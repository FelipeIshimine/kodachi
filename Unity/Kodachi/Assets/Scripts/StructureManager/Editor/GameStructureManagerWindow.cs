﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameStructureManagerWindow : EditorWindow
{
    static GameStructure structureManager;

  //  [MenuItem("GameManagement/GameStoryStructure")]
    public static void ShowWindow()
    {
        GetWindow<GameStructureManagerWindow>("GameStructure");
    }

    protected  void OnEnable()
    {
        structureManager = GameStructure.Instance;
    }

    protected  void OnGUI()
    {
        Editor editor = Editor.CreateEditor(structureManager);
        editor.DrawDefaultInspector();
    }

}
