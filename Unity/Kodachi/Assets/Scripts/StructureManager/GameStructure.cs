﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameStructure : ScriptableSingleton<GameStructure>
{

#if UNITY_EDITOR
    [MenuItem("GameManagement/Structure")]
    public static void SelectMe()
    {
        Selection.SetActiveObjectWithContext(Instance, Instance);
    }
#endif

    public override GameStructure Myself => this;
    public static IReadOnlyList<GameChapter> Chapters => Instance.chapters;

    private List<GameSequence> sequences;
    public static IReadOnlyList<GameSequence> Sequences => Instance.sequences;

    public List<GameChapter> chapters;
    private List<GameSequence> cinematics;
    private List<GameSequence> levels;

    public GameChapter this[int index] => Chapters[index];

    [RuntimeInitializeOnLoadMethod]
    public static void UpdateSequences()
    {
        Instance._UpdateSequences();
    }

    private void _UpdateSequences()
    {
        sequences = new List<GameSequence>();
        cinematics = new List<GameSequence>();
        levels = new List<GameSequence>();

        for (int i = 0; i < chapters.Count; i++)
        {
            sequences.AddRange(chapters[i].Sequences);
            chapters[i].LinkSequences();
        }

        for (int i = 0; i < sequences.Count; i++)
        {
            switch (sequences[i].type)
            {
                case GameSequence.GameSequenceType.Cinematic:
                         cinematics.Add(sequences[i]);
                    break;
                case GameSequence.GameSequenceType.Level:
                        levels.Add(sequences[i]);
                    break;
            }
        }
    }

    internal static GameSequence GetSequenceAfter(GameSequence currentSequence) => Instance._GetSequenceAfter(currentSequence);

    private GameSequence _GetSequenceAfter(GameSequence currentSequence)
    {
        int index = sequences.IndexOf(currentSequence);
        if (index < sequences.Count-1)
            return sequences[index+1];
        else
            return null;
    }

    internal static void GoTo(int chapter, int sequence) => Instance[chapter][sequence].GoToState();

    internal static GameSequence GetCinematic(int cinematicId)
    {
        return Instance.cinematics[cinematicId];
    }


}

