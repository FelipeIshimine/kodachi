﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_LevelComplete : MonoBehaviour
{
    private Action OnMenuCallback;
    [SerializeField]  private Button nextSequenceButton;

    public AnimatedContainer animatedContainer;
    GameSequence nextGameSequence;

    internal void Initialize(Action OnMenuCallback, GameSequence nextGameSequence)
    {
        this.nextGameSequence = nextGameSequence;
        this.OnMenuCallback = OnMenuCallback;
        nextSequenceButton.gameObject.SetActive(nextGameSequence != null);
    }

    public void Open()
    {
        animatedContainer.Open();
    }

    public void Hide() => animatedContainer.Hide();

    public void GoToNextLevel()
    {
        nextGameSequence.GoToState();
    }

    public void GoToMenu()
    {
        OnMenuCallback.Invoke();
    }

    private void OnDestroy()
    {
        OnMenuCallback = null;
    }
}
