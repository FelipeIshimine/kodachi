﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [Range(-1,1)]
    public float ammount = .5f;


    public Camera mainCamera;

    public void OnEnable()
    {
        if (mainCamera == null)
            mainCamera = Camera.main;
    }


    public void Update()
    {
        transform.localPosition = transform.InverseTransformPoint(mainCamera.transform.position) * ammount;
    }

}
