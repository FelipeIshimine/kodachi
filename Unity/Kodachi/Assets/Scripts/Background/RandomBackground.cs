﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBackground : MonoBehaviour
{
    public List<GameObject> backgroundPrefabs;

    private void Awake()
    {
        GameObject go = Instantiate(backgroundPrefabs.GetRandom(), transform);
        go.transform.SetParent(transform);
    }
}
