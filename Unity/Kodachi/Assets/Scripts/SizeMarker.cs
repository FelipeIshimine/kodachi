﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeMarker : MonoBehaviour
{
    public float width = 1;
    public float height = 600;
    public Vector2 size = new Vector2(12, 50);
    public int markersCount = 5;

    public int startPosition = -20;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        float currentH = startPosition;
        for (int i = 0; i < markersCount; i++)
        {
            Gizmos.DrawWireCube(Vector3.up * (currentH + size.y/2), size);
            currentH += size.y;
        }
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(Vector3.up * height/2 + Vector3.up * startPosition, new Vector3(width, height));
    }
}