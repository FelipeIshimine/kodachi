﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ManualAddressableAsyncInstantiator : MonoBehaviour
{
    public List<GameObject> activeObjects = new List<GameObject>();

    public void Instantiate(AssetReference reference)
    {
        Addressables.InstantiateAsync(reference).Completed += AsyncInstantiationComplete;
    }

    private void AsyncInstantiationComplete(AsyncOperationHandle<GameObject> obj)
    {
        if (obj.Status == AsyncOperationStatus.Failed)
        {
            Debug.LogError("Fail");
            return;
        }

        Debug.Log($"Instanciacion de {obj.Result.name} completa");
        activeObjects.Add(obj.Result);
    }

    public void ReleseObjectInstance(GameObject go)
    {
        int index = activeObjects.IndexOf(go);
        if (index != -1)
        {
            Debug.Log($"SelfRelease of {go.name} from {this} Successfull ");
            activeObjects.RemoveAt(index);
            Addressables.ReleaseInstance(go);
        }
        else
            Debug.LogError($"Objeto no pertenece a {this}");
    }
}
