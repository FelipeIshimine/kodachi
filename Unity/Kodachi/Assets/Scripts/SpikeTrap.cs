﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class SpikeTrap : MonoBehaviour
{
    public AnimationCurve curveIn;
    public AnimationCurve curveOut;

    public float openDuration = .3f;
    public float closeDuration = .3f;

    public Transform spikePivot;

    public float l;

    IEnumerator rutine;

    public bool startOpen = true;
    public bool IsOpen { get; private set; } = false;

    private void Start()
    {
        if (startOpen)
            Open();
        else
            Close();
    }

    [Button]
    public void TestOpen(float t)
    {
        spikePivot.transform.localPosition = l * curveIn.Evaluate(t) * Vector3.up;
    }

    [Button]
    public void Open()
    {
        if (IsOpen) return;
        if (rutine != null) StopCoroutine(rutine);
        rutine = OpenRutine();
        StartCoroutine(rutine);
        IsOpen = true;
    }

    [Button]
    public void Close()
    {
        if (!IsOpen) return;
        if (rutine != null) StopCoroutine(rutine);
        rutine = CloseRutine();
        StartCoroutine(rutine);
        IsOpen = false;
    }

    public void Swap()
    {
        if (IsOpen) Close();
        else Open();
    }

    IEnumerator OpenRutine()
    {
        float t = 0;
        do
        { 
            t += Time.deltaTime / openDuration;
            spikePivot.transform.localPosition = l * curveIn.Evaluate(t) * Vector3.up;
            yield return null;
        } while (t < 1);
    }

    IEnumerator CloseRutine()
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / closeDuration;
            spikePivot.transform.localPosition = l * (1-curveOut.Evaluate(t)) * Vector3.up;
            yield return null;
        } while (t < 1);
    }

}
