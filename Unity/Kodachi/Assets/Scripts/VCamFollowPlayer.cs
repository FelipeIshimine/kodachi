﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class VCamFollowPlayer : MonoBehaviour
{
    public CinemachineVirtualCamera virtualCamera;

    private void OnValidate()
    {
        if (virtualCamera == null)
            virtualCamera = GetComponent<CinemachineVirtualCamera>();
    }

    private void OnEnable()
    {
        virtualCamera.Follow = FindObjectOfType<Player>().transform;
    }
}
