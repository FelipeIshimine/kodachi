﻿using Pooling.Poolers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfPoolable : MonoBehaviour
{
    public Poolable poolable;
    public SetPooler pool;
    IEnumerator currentRutine;

    public void Enqueue()
    {
        pool.Enqueue(poolable);
    }

    public void WaitAndPool(float waitTime)
    {
        if (currentRutine != null) StopCoroutine(currentRutine);
        currentRutine = WaitAndPoolRutine(waitTime);
        StartCoroutine(currentRutine);
    }

    IEnumerator WaitAndPoolRutine(float t )
    {
        yield return new WaitForSeconds(t);
        Enqueue();
    }


    private void OnDisable()
    {
        if (currentRutine != null) StopCoroutine(currentRutine);
    }

    private void OnDestroy()
    {
        pool.Remove(poolable);
    }
}


public static class SelfPoolableGameobjectExtension
{
    public static void ClearSelfPoolables(this GameObject go)
    {
        SelfPoolable[] poolables = go.GetComponentsInChildren<SelfPoolable>();
        foreach (SelfPoolable item in poolables)
        {
            item.Enqueue();
        }
    }
}