﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

public class ScreenWrappable : MonoBehaviour
{
    private const float OVERLAP_AREA_EXTRA_SIZE = 4;
    public LayerMask overlapMask;

    [Tooltip("Will get the velocity from the attached rigid body if not will search for an IHaveVelocity interface in the herarchy")]
    public bool velocityFromRigidBody = true;
    [ShowIf("velocityFromRigidBody")] public Rigidbody2D rb;

    public Collider2D col;
    public Vector2 Velocity
    {
        get => GetVelocity.Invoke();
        set => SetVelocity.Invoke(value);
    }

    private Func<Vector2> GetVelocity;
    private Action<Vector2> SetVelocity;
    public Action OnScreenWrapPrepare;
    public Action OnScreenWrapPerformed;
    public void ScreenWrapPrepare() => OnScreenWrapPrepare?.Invoke();
    public void PostScreenPerformed() => OnScreenWrapPerformed?.Invoke();

    private void Awake()
    {
        if (rb != null)
        {
            GetVelocity = () => rb.velocity;
            SetVelocity = (x) => rb.velocity = x;
        }
        else
        {
            IHaveVelocity2D haveVelocity2D = GetComponent<IHaveVelocity2D>();
            GetVelocity = () => haveVelocity2D.Velocity;
            SetVelocity = (x) => haveVelocity2D.Velocity = x;
        }
    }


    private void OnEnable()
    {
        Register();
    }

    private void OnDisable()
    {
        Unregister();
    }

    private void Register()
    {
        CameraWrap.Register(this);
    }

    private void Unregister()
    {
        CameraWrap.Unregister(this);
    }

    [Button]
    internal bool OverlapsAt(Vector3 displacement)
    {
        Vector2 extraSize = new Vector2((displacement.x != 0) ? OVERLAP_AREA_EXTRA_SIZE : -col.bounds.size.x - .25f, (displacement.y != 0) ? OVERLAP_AREA_EXTRA_SIZE : -col.bounds.size.y - .25f);

        Rect area = new Rect();
        area.size = (Vector2)col.bounds.size + new Vector2(
                     col.bounds.size.x + Mathf.Abs(Velocity.x) * Time.fixedDeltaTime,
                     col.bounds.size.y + Mathf.Abs(Velocity.y) * Time.fixedDeltaTime) + extraSize;
        area.SetCenter(transform.position + displacement + new Vector3(area.size.x * Mathf.Sign(displacement.x),0,0) / 2);

        Debug.DrawLine(new Vector3(area.xMin, area.yMin), new Vector3(area.xMin, area.yMax));
        Debug.DrawLine(new Vector3(area.xMin, area.yMax), new Vector3(area.xMax, area.yMax));
        Debug.DrawLine(new Vector3(area.xMax, area.yMax), new Vector3(area.xMax, area.yMin));
        Debug.DrawLine(new Vector3(area.xMax, area.yMin), new Vector3(area.xMin, area.yMin));

        bool value = Physics2D.OverlapArea(area.min, area.max, overlapMask) != null;
        Debug.Log($"OverlapsAt({ displacement}): {value}");
        return value;
    }

}