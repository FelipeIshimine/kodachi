﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class CameraWrap : MonoBehaviour
{
    public bool BounceIfOverlapOnWrap = true;
    public Vector2 OverLapAreaSize = new Vector2(6, 6);

    [SerializeField] private bool wrapX = true;
    [SerializeField] private bool wrapY = true;

    PixelPerfectCamera perfectCamera;

    private static CameraWrap instance;
    public static CameraWrap Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CameraWrap>();
                if(instance == null)
                    Debug.LogError("No se encontro una instancia de CameraWrap estas pidiendo gilada");
            }
            return instance;
        }
    }

    internal static void Register(ScreenWrappable screenWrappable)
    {
        Instance.targets.Add(screenWrappable);
    }

    internal static void Unregister(ScreenWrappable screenWrappable)
    {
        Instance.targets.Remove(screenWrappable);
    }

    private HashSet<ScreenWrappable> targets = new HashSet<ScreenWrappable>();
    public Camera targetCamera;
    public float extraSpace = 1;
    private Vector2 cameraSize;

    private void Awake()
    {
        if (instance != null)
            Debug.LogError($" {this} = instance == null");
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void Start()
    {
        cameraSize = targetCamera.GetOrtographicSize();
        StartCoroutine(LateInitialization());
    }

    IEnumerator LateInitialization()
    {
        yield return new WaitForEndOfFrame();
        cameraSize = targetCamera.GetOrtographicSize();
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        ScreenWrappable screenWrap = collision.GetComponent<ScreenWrappable>();
        if(screenWrap != null)
            targets.Add(screenWrap);
    }*/

    public void FixedUpdate()
    {
        foreach (ScreenWrappable item in targets)
        {
         /*   Debug.Log($"{item.gameObject.name}.transform.position: {item.transform.position}");
            Debug.Log($"{gameObject.name}.position: {transform.position}");
            Debug.Log(
                $"RightLimit: {transform.position + Vector3.right * (cameraSize.x / 2 - extraSpace)}" +
                $"LeftLimit: { transform.position + Vector3.left * (cameraSize.x / 2 - extraSpace)}" +
                $"Bottom: {transform.position + Vector3.down * (cameraSize.y / 2 - extraSpace)}" +
                $"Top: {transform.position + Vector3.up * (cameraSize.y / 2 - extraSpace)}");*/

            if (wrapX)
                WrapHorizontal(item);

            if (wrapY)
                WrapVertical(item);
        }
    }

    private void WrapVertical(ScreenWrappable item)
    {
        Vector2 otherPos = item.transform.position;
        Vector2 otherVel = item.Velocity;
        Vector2 pos = transform.position;

        int sign = (int)Mathf.Sign(item.Velocity.y);
        Vector3 displacement = Vector2.zero;

        if (sign > 0 && (otherPos.y > pos.y + cameraSize.y / 2 + extraSpace))
            displacement = Vector3.down * (cameraSize.y + extraSpace);
        else if (sign < 0 && (otherPos.y < pos.y - cameraSize.y / 2 - extraSpace))
            displacement = Vector3.up * (cameraSize.y + extraSpace);

        if(displacement != Vector3.zero)
        {
            if (BounceIfOverlapOnWrap && item.OverlapsAt(displacement))
                item.Velocity = new Vector2(item.Velocity.x, item.Velocity.y * -1);
            else
                item.transform.position += displacement;
        }
    }

    private void WrapHorizontal(ScreenWrappable item)
    {
        Vector2 otherPos = item.transform.position;
        Vector2 otherVel = item.Velocity;
        Vector2 pos = transform.position;

        int sign = (int)Mathf.Sign(item.Velocity.x);
        Vector3 displacement = Vector2.zero;

        float[] limits = new float[2]
        {
            pos.x - cameraSize.x / 2 - extraSpace,
            pos.x + cameraSize.x / 2 + extraSpace
        };

        if (sign < 0 && (otherPos.x < limits[0]))
            displacement = Vector3.right * (cameraSize.x + extraSpace);
        else if (sign > 0 && otherPos.x > limits[1])
            displacement = Vector3.left * (cameraSize.x + extraSpace);

        if (displacement != Vector3.zero)
        {
            if (BounceIfOverlapOnWrap && item.OverlapsAt(displacement))
                item.Velocity = new Vector2(item.Velocity.x * -1, item.Velocity.y);
            else
                item.transform.position += displacement;
        }
    }
}
