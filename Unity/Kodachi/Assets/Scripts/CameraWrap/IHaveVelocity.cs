﻿using UnityEngine;

internal interface IHaveVelocity2D
{
    Vector2 Velocity { get; set; }
}