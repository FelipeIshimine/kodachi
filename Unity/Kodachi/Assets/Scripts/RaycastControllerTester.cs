﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastControllerTester : MonoBehaviour
{
    public RaycastController raycastController;

    public Vector2 vel;
    public int faceDir;

    public float skinWidth = .2f;

    public void FixedUpdate()
    {
        PreCalculatePosition(vel * Time.fixedDeltaTime);
    }

    public Vector2 PreCalculatePosition(Vector2 moveAmount)
    {
        raycastController.CalculateRaySpacing();
        raycastController.UpdateRaycastOrigins();
        //  collisions.Reset();
        //  collisions.moveAmountOld = moveAmount;

        /*  if (moveAmount.y < 0)
              //     DescendSlope(ref moveAmount);*/

        if (moveAmount.x != 0)
            faceDir = (int)Mathf.Sign(moveAmount.x);

        HorizontalCollisions(ref moveAmount);

        if (moveAmount.y != 0)
            VerticalCollisions(ref moveAmount);

        return moveAmount;
    }

    private void VerticalCollisions(ref Vector2 moveAmount)
    {

    }

    void HorizontalCollisions(ref Vector2 moveAmount)
    {
        float directionX = faceDir;
        float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

        if (Mathf.Abs(moveAmount.x) < skinWidth)
        {
            rayLength = 2 * skinWidth;
        }


        for (int i = 0; i < raycastController.rayCount.x; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? raycastController.raycastOrigins.bottomLeft : raycastController.raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (raycastController.raySpacing.x * i);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, raycastController.collisionMask);
            Debug.Log($"Vector2.right * directionX:{Vector2.right * directionX}");
            Debug.Log($"rayLength: {rayLength}");

            if (hit)
            {
                Debug.DrawLine(rayOrigin, hit.point, Color.green);
                Debug.Log($"HIT  rayOrigin:{rayOrigin} => {hit.point}");
                if (hit.distance == 0)
                    continue;
            }
            else
            {
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);
                Debug.Log($"NO HIT   rayOrigin:{rayOrigin} => {Vector2.right * directionX * rayLength}");
            }
        }
    }
}
