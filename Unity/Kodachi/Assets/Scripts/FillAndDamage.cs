﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillAndDamage : MonoBehaviour
{
    public SpriteRenderer fill;
    public void UpdateProgress(float t)
    {
        fill.gameObject.transform.localScale = Vector3.one*t;
    }
}
