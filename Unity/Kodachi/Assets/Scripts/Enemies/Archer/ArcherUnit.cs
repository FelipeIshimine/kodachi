﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using FMOD;
using FMODUnity;
using Pooling.Poolers;

public class ArcherUnit : BaseEnemyUnitMonoBehaviour
{
    [TabGroup("Dependencies")] public Canvas_UnitAlarm canvas_UnitAlarm;
    [TabGroup("Dependencies")] public ConstantSizeChange squashAnimation;
    [TabGroup("Dependencies")] public FieldOfView fieldOfView;
    [TabGroup("Dependencies")] public ArcherVisual visual;
    [TabGroup("Dependencies")] public SetPooler arrowPool;
    [TabGroup("Dependencies")] public Transform firePoint;

    public bool isLooped = true;

    [Header("FieldOfView")]
    public float radius = 1;
    public float angle = 80;
    public float lookingRecentlyLostTargetDuration = 2;

    [TabGroup("Aiming")] public float aimDuration;
    [TabGroup("Aiming")] public AnimationCurve aimFocusCurve;
    [TabGroup("Aiming")] public LayerMask arrowTargetLayerMask;
    [TabGroup("Aiming")] public float movementPredictionLength = .2f;
    [TabGroup("Aiming")] public float arrowSpeed = 20;
    [TabGroup("Aiming")] public float shootWaitTime = .5f;
    [TabGroup("Aiming")] public AnimationCurve shootFlashCurve;

    [TabGroup("Audio")] public StudioEventEmitter shootSound;
    [TabGroup("Audio")] public StudioEventEmitter aimSound;

    private Vector2[] waypoints;

    private bool alarmRaised = false;

    [Range(-1,1)]
    public int startLookDir = 1;

    protected override void Awake()
    {
        base.Awake();
        waypoints = CollectPositions().ToArray();
        Initialize();
        if (startLookDir == -1)
            FlipArcher(false);
    }

    public void Initialize()
    {
        fieldOfView.viewAngle = angle;
        fieldOfView.viewRadius = radius;
        canvas_UnitAlarm.OnRaiseAlarm -= RaiseAlarm;
        canvas_UnitAlarm.OnRaiseAlarm += RaiseAlarm;
        GoToNoAlertPatrolState();
    }

    private void OnTargetFoundWhenPatrolling(Player p)
    {
        if(!alarmRaised)
            canvas_UnitAlarm.StartCounter();

        GoToAimForTargetState(p);
    }

  

    [Button]
    private void GoToNoAlertPatrolState()
    {
        fieldOfView.CanSeeHiddenUnits = false;
        visual.Idle(true);
        SwitchState(new PatrolUnitState(this, squashAnimation, waypoints, fieldOfView, noAlertColor, isLooped, patrolSpeed, waitTimeOnPointNoAlert, waitTimeOnPointAlerted, OnTargetFoundWhenPatrolling));
    }

    [Button]
    private void GoToAimForTargetState(Player player)
    {
        fieldOfView.CanSeeHiddenUnits = true;
        aimSound.Play();
        visual.NockArrow();
        SwitchState(new AimingForTarget(this, visual, fieldOfView, player, highAlertColor, aimDuration, movementPredictionLength, aimFocusCurve, OnTargetLostWhenAiming, OnShot));
    }

    private void OnShot(Vector2 targetPosition)
    {
        //Audio
        shootSound.Play();
        aimSound.Stop();

        SwitchState(
            new ShotState(
                this,
                fieldOfView,
                visual,
                firePoint,
                targetPosition,
                arrowSpeed,
                arrowPool.Dequeue().GetComponent<Arrow>(), 
                shootWaitTime, 
                shootFlashCurve, 
                arrowTargetLayerMask,
                GoToAlertedPatrol));
    }

    private void OnTargetLostWhenAiming(Transform targetLost)
    {
        //Audio
        aimSound.Stop();

        visual.Idle(false);
        if (IsBehindMe(targetLost.position))
            FlipArcher(false);
        else
        {
            if (!alarmRaised)
                canvas_UnitAlarm.StopCounter();

            SwitchState(new AlertLookingForTargetState(this, fieldOfView, highAlertColor, lowAlertColor, lookingRecentlyLostTargetDuration, OnTargetFound, OnTargetNotFound));
        }
    }

    private void OnTargetNotFound()
    {
        GoToAlertedPatrol();
    }

    private void GoToAlertedPatrol()
    {
        fieldOfView.ClearAll();
        fieldOfView.CanSeeHiddenUnits = true;
        visual.Idle(true);
        SwitchState(new PatrolUnitState(this, squashAnimation, waypoints, fieldOfView, lowAlertColor, isLooped, highAlertPatrolSpeed, waitTimeOnPointAlerted, waitTimeOnPathDoneAlerted, OnTargetFoundWhenPatrolling));
    }

    private void OnTargetFound(Transform obj)
    {
        Player p = obj.GetComponentInParent<Player>();
        if (p != null)
            GoToAimForTargetState(p);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        canvas_UnitAlarm.OnRaiseAlarm -= RaiseAlarm;
    }

    public override void RaiseAlarm()
    {
        base.RaiseAlarm();
        canvas_UnitAlarm.OnRaiseAlarm = null;
    }


    public override void Hook()
    {
        base.Hook();    
        SwitchState(new FearState(this, fieldOfView));
    }

    protected override void OnAlarmRaised(BaseEnemyUnitMonoBehaviour source)
    {
        if (Vector2.Distance(transform.position, source.transform.position) <= AlarmDistance && !(CurrentState is AimingForTarget))
            GoToAlertedPatrol();
    }

    public void FlipArcher(bool aimCorrection)
    {
        base.Flip();
        fieldOfView.Flip();
        visual.Flip(aimCorrection);
    }
}
