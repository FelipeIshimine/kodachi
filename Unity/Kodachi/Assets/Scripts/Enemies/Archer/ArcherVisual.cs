﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherVisual : MonoBehaviour
{
    public Animator bowAnimator;
    public SpriteRenderer bodyRender;
    public SpriteRenderer headRender;
    public SpriteRenderer bowRender;
    public Transform bowPivot;
    public Transform headPivot;

    private int lookDir = 1;

    IEnumerator rutine;

    public float smooth = .2f;
    private Vector2 currentLookPosition;
    private float vel;

    public bool isAiming = false;
    public float bowAimingDistance = .75f;

    public Transform defaultBowPosition;

    public void FixedUpdate()
    {
        if (!isAiming)
            currentLookPosition = headPivot.position + Vector3.right * lookDir * 3;

        Vector3 dir = headPivot.position.GetDirection(currentLookPosition);
        headPivot.rotation = Quaternion.Euler(0, 0, Mathf.SmoothDampAngle(headPivot.eulerAngles.z, dir.AsAngle2D() + ((lookDir==1)?0:180), ref vel, smooth));

        if (isAiming)
        {
            bowPivot.transform.localPosition = dir * bowAimingDistance;
            bowPivot.transform.rotation = Quaternion.Euler(0, 0, dir.AsAngle2D());
        }
        else
        {
            bowPivot.transform.position = defaultBowPosition.transform.position;
            bowPivot.transform.rotation = defaultBowPosition.transform.rotation;
        }
    }

    public void SetTargetPosition(Vector2 currentTargetPosition)
    {
        isAiming = true;
        this.currentLookPosition = currentTargetPosition;
    }

    internal void Flip(bool aimCorrection)
    {
        lookDir *= -1;
        bodyRender.transform.localScale = new Vector3(lookDir, 1, 1);
        headRender.transform.localScale = new Vector3(lookDir, 1, 1);
        bowRender.transform.localScale = new Vector3(1, lookDir, 1);

        vel = 0;

        currentLookPosition = headPivot.position + Vector3.right * lookDir * 3;
        
        if(aimCorrection)
        {
            bowPivot.transform.rotation = Quaternion.Euler(0, 0, bowPivot.eulerAngles.z + 180);
            headPivot.transform.rotation = Quaternion.Euler(0, 0, headPivot.eulerAngles.z + 180);
        }
    }

    internal void NockArrow()
    {
        isAiming = true;
        bowAnimator.SetTrigger("Nock");
    }

    internal void Idle(bool resetAimming)
    {
        if(resetAimming)
            isAiming = false;
        bowAnimator.SetTrigger("Idle");
    }

    internal void ShotArrow()
    {
        isAiming = false;
        bowAnimator.SetTrigger("Release");
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(defaultBowPosition.position, .5f);
        Gizmos.DrawLine(defaultBowPosition.position, defaultBowPosition.position + defaultBowPosition.right * 1.5f);
    }
}
