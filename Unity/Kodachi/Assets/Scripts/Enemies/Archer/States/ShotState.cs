﻿using System;
using UnityEngine;

internal class ShotState : BaseEnemyUnitState
{
    private ArcherUnit archerUnit;
    private ArcherVisual visual;
    private Arrow arrow;
    private float shootWaitTime;
    private AnimationCurve shootFlashCurve;
    private Action onDoneCallback;
    private float t;
    FieldOfView fieldOfView;
    Transform firePoint;
    Vector2 targetPosition;
    LayerMask arrowLayerMask;
    private Color startColor;
    float arrowSpeed;

    public ShotState(ArcherUnit archerUnit, FieldOfView fieldOfView, ArcherVisual visual, Transform firePoint, Vector2 targetPosition, float arrowSpeed, Arrow arrow, float shootWaitTime, AnimationCurve shootFlashCurve, LayerMask arrowLayerMask, Action onDoneCallback) : base(archerUnit)
    {
        this.arrowSpeed = arrowSpeed;
        this.fieldOfView = fieldOfView;
        this.arrowLayerMask = arrowLayerMask;
        this.targetPosition = targetPosition;
        this.archerUnit = archerUnit;
        this.visual = visual;
        this.arrow = arrow;
        this.shootWaitTime = shootWaitTime;
        this.shootFlashCurve = shootFlashCurve;
        this.onDoneCallback = onDoneCallback;
        this.firePoint = firePoint;
    }

    public override void Enter()
    {
        base.Enter();
        t = 0;
        startColor = fieldOfView.areaColor;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        t += Time.fixedDeltaTime / shootWaitTime;
        fieldOfView.areaColor = Color.Lerp(startColor, Color.white, shootFlashCurve.Evaluate(t));

        if (t >= 1)
            Shoot();
    }

    private void Shoot()
    {
        visual.ShotArrow();
        Debug.Log("Shot");
        arrow.transform.position = firePoint.transform.position;
        arrow.Initialize(arrowSpeed, firePoint.position.GetDirection(targetPosition).AsAngle2D(), arrowLayerMask, Owner.gameObject);
        onDoneCallback.Invoke();
    }
}