﻿using UnityEngine;
using System;

public class AimingForTarget : BaseEnemyUnitState
{
    Action<Transform> OnTargetLostCallback;
    Action<Vector2> OnShootCallback;

    FieldOfView fieldOfView;
    Color color;
    Player target;
    public float aimDuration;
    private float t =0;
    AnimationCurve focusCurve;
    ArcherVisual visual;
    private float movementPredictionLength;
    ArcherUnit archerUnit;

    public AimingForTarget(
        ArcherUnit archerUnit,
        ArcherVisual visual,
        FieldOfView fieldOfView,
        Player target,
        Color color,
        float aimDuration,
        float movementPredictionLength,
        AnimationCurve focusCurve,
        Action<Transform> OnTargetLostCallback,
        Action<Vector2> OnShootCallback) : base(archerUnit)
    {
        this.archerUnit = archerUnit;
        this.movementPredictionLength = movementPredictionLength;
        this.visual = visual;
        this.OnShootCallback = OnShootCallback;
        this.focusCurve = focusCurve;
        this.OnTargetLostCallback = OnTargetLostCallback;
        this.target = target;
        this.fieldOfView = fieldOfView;
        this.color = color;
        this.aimDuration = aimDuration;
    }

    public override void Enter()
    {
        base.Enter();
        hide = target.GetComponent<IHide>();
        Register();
        fieldOfView.areaColor = color;
        aimDuration *= fieldOfView.drawPercentage;
    }

    public override void Exit()
    {
        base.Exit();
        Unregister();
    }

    private void Register()
    {
        fieldOfView.OnTargetLost += OnTargetLost;
    }

    private void Unregister()
    {
        fieldOfView.OnTargetLost -= OnTargetLost;
    }

    private void OnTargetLost(Transform obj)
    {
        if (obj == target.transform)
            OnTargetLostCallback.Invoke(obj);
    }
    IHide hide;
    Vector2 targetPosition;

    public override void Update()
    {
        base.Update();
        t += Time.deltaTime / aimDuration;
        fieldOfView.drawPercentage = 1 - focusCurve.Evaluate(t);

        if (hide != null && hide.IsHidden)
            targetPosition = target.transform.position;
        else
            targetPosition = (Vector2)target.transform.position + target.Velocity * (movementPredictionLength  * Mathf.Clamp01(Vector2.Distance(Owner.transform.position, target.transform.position) / 5));

        visual.SetTargetPosition(targetPosition);
            
        if (archerUnit.IsBehindMe(target.transform.position))
            archerUnit.FlipArcher(true);

        if (t > 1)
            OnShootCallback.Invoke(targetPosition);
    }
}
