﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Pooling.Poolers;

public class AutoShoot : MonoBehaviour
{
    public SetPooler arrowPool;
    public Animator animator;
    public float waitDuration = 2;
    public float nockDuration = .5f;
    private float currentTime = 0;
    private bool playing = false;
    [SerializeField] private Transform firePoint = null;
    public float arrowSpeed = 40;
    public LayerMask layerMask;
    public SquashAnimation squashAnimation;
    public Transform visualRoot;

    private bool isArrowNock = false;
    private int dir = 1;

    private void Awake()
    {
        currentTime = waitDuration;
    }

    public void Update()
    {
        if (!playing) return;
        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
        {
            if (isArrowNock)
                Shoot();
            else
                Nock();
        }
    }

    [Button]
    public void Play()
    {
        playing = true;
        currentTime = waitDuration;
        isArrowNock = false;
    }

    [Button]
    public void Stop()
    {
        playing = false;
        animator.SetTrigger("Idle");
    }

    public void Shoot()
    {
        Arrow arrow = arrowPool.Dequeue().GetComponent<Arrow>();
        arrow.transform.position = firePoint.position;
        arrow.Initialize(arrowSpeed, transform.position.GetAngle(firePoint.position), layerMask, gameObject);
        Play();
        animator.SetTrigger("Release");
    }

    public void Nock()
    {
        currentTime = nockDuration;
        isArrowNock = true;
        animator.SetTrigger("Nock");
        squashAnimation.duration = currentTime;
        squashAnimation.Play();
    }

    [Button]
    public void Flip()
    {
        dir *= -1;
        visualRoot.localScale = new Vector3(dir, 1, 1);
    }
}
