﻿using System;
using UnityEngine;

internal class AlertLookingForTargetState : BaseEnemyUnitState
{
    private FieldOfView fieldOfView;
    private Color highAlertAreaColor;
    private Color lowAlertColor;
    private Action<Transform> onTargetFound;
    private Action onTargetNotFound;
    private float lookingDuration;
    private float t = 0;
    
    private float startPercentage;

    public AlertLookingForTargetState(BaseEnemyUnitMonoBehaviour owner, FieldOfView fieldOfView, Color highAlertColor, Color lowAlertColor, float lookingDuration, Action<Transform> onTargetFound, Action onTargetNotFound) : base(owner)
    {
        this.lookingDuration = lookingDuration;
        this.fieldOfView = fieldOfView;
        this.highAlertAreaColor = highAlertColor;
        this.lowAlertColor = lowAlertColor;
        this.onTargetFound = onTargetFound;
        this.onTargetNotFound = onTargetNotFound;
    }

    public override void Enter()
    {
        base.Enter();
        startPercentage = fieldOfView.drawPercentage;
        fieldOfView.OnTargetFound += OnTargetFound;

        fieldOfView.drawPercentage = 1;
    }

    public override void Exit()
    {
        fieldOfView.OnTargetFound -= OnTargetFound;
        onTargetNotFound = null;
        onTargetFound = null;
        base.Exit();
    }

    private void OnTargetFound(Transform obj)
    {
        Player p = obj.GetComponentInParent<Player>();
        if (p != null)
            onTargetFound?.Invoke(p.transform);
    }

    public override void Update()
    {
        base.Update();
        t += Time.deltaTime / lookingDuration;
        fieldOfView.areaColor = Color.Lerp(highAlertAreaColor, lowAlertColor, t);
        if (t >= 1)
            onTargetNotFound?.Invoke();
    }
}