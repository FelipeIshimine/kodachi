﻿public class FearState : BaseEnemyUnitState
{
    private ArcherUnit archerUnit;
    private FieldOfView fieldOfView;

    public FearState(ArcherUnit archerUnit, FieldOfView fieldOfView) : base(archerUnit)
    {
        this.archerUnit = archerUnit;
        this.fieldOfView = fieldOfView;
    }

    public override void Enter()
    {
        base.Enter();
        fieldOfView.enabled = false;
    }

    public override void Exit()
    {
        base.Exit();
        fieldOfView.enabled = true;
    }
}