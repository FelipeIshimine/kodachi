﻿using UnityEngine;
using System;

public class PatrolUnitState : BaseEnemyUnitState
{
    private Action<PatrolUnitState> OnPositionReached = null;
    private Action<PatrolUnitState> OnPathDoneReached = null;

    readonly protected Vector2[] patrolPath;
    readonly bool isLooped;
    readonly float speed;
    
    private Vector2 targetPosition;
    private readonly float stopDistance = .15f;
    protected int index = -1;
    private readonly float waitTimeOnPoint;
    private float currentWaitTime = 0;
    private readonly float waitTimeOnPathDone;
    private int dir = 1;
    private readonly Action<Player> onTargetFoundWhenPatrolling;
protected    FieldOfView fieldOfView;
    Color color;
    ConstantSizeChange squashAnimation;

    private readonly float walkSquashSpeed;

    public PatrolUnitState(BaseEnemyUnitMonoBehaviour owner, ConstantSizeChange squashAnimation, Vector2[] patrolPath, FieldOfView fieldOfView, Color color, bool isLooped, float speed, float waitTimeOnPoint, float waitTimeOnPathDone, Action<Player> onTargetFoundWhenPatrolling) : base(owner)
    {
        this.color = color;
        this.fieldOfView = fieldOfView;
        this.patrolPath = patrolPath;
        this.isLooped = isLooped;
        this.speed = speed;
        this.onTargetFoundWhenPatrolling = onTargetFoundWhenPatrolling;
        this.waitTimeOnPoint = waitTimeOnPoint;
        this.waitTimeOnPathDone = waitTimeOnPathDone;
        this.squashAnimation = squashAnimation;
        this.walkSquashSpeed = 2 / speed;
    }

    public override void Enter()
    {
        fieldOfView.areaColor = color;
        fieldOfView.drawPercentage = 1;
        fieldOfView.OnTargetFound += OnTargetFound;

        if(patrolPath != null && patrolPath.Length > 0)
            NextTargetPosition();

        if (patrolPath != null && patrolPath.Length > 0)
        {
            squashAnimation.NextSettings();
            squashAnimation.Play(true);
        }
        else
            squashAnimation.Stop();
    }

    public override void Exit()
    {
        base.Exit();
        fieldOfView.OnTargetFound -= OnTargetFound;
        squashAnimation.Stop();
    }

    protected virtual void OnTargetFound(Transform obj)
    {
        Debug.Log($"TarguetFound: {obj}");
        Player p = obj.GetComponentInParent<Player>();
        if (p != null)
            onTargetFoundWhenPatrolling.Invoke(p);
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (patrolPath == null || patrolPath.Length == 0) return;

        if (currentWaitTime > 0) //Esperando
        {
            currentWaitTime -= Time.fixedDeltaTime;
            if (currentWaitTime <= 0) //Termina espera
            {
                squashAnimation.PreviousSettings();
                squashAnimation.Play(true);
                TryFlip();
            }
        }
        else //Caminando
        {
            Owner.transform.Translate(Owner.transform.position.GetDirection(targetPosition) * speed * Time.fixedDeltaTime, Space.World);
            if (Vector2.Distance(Owner.transform.position, targetPosition) <= stopDistance)
                PositionReached();
        }
    }

    protected void TryFlip()
    {
        if (Owner.IsBehindMe(targetPosition)) //Objetivo esta detras
        {
            if (Owner is ArcherUnit archer)
                archer.FlipArcher(false);
            else
                Owner.Flip();
        }
    }

    private void PositionReached()
    {
        bool isPathEnd = ((dir == 1 && index == patrolPath.Length - 1) || (dir == -1 && index == 1));
        NextTargetPosition();
        OnPositionReached?.Invoke(this);
        if (isPathEnd)
        {
            OnPathDoneReached?.Invoke(this);
            currentWaitTime = waitTimeOnPathDone;
            if (!isLooped)
                FlipPatrol();
        }
        else
            currentWaitTime = waitTimeOnPoint;

        if (currentWaitTime != 0)
        {
            squashAnimation.NextSettings();
            squashAnimation.SetCicleDuration(currentWaitTime, true);
            squashAnimation.Play(true);
        }
    }

    private void FlipPatrol()
    {
        dir *= -1;
    }

    private void NextTargetPosition()
    {
        index = (index + dir + patrolPath.Length) % patrolPath.Length;
        targetPosition = patrolPath[index];
    }

}
