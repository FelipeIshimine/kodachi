﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SamuraiUnit : BaseEnemyUnitMonoBehaviour, IUseAttackTrigger
{
    [TabGroup("Dependencies")] public Canvas_UnitAlarm canvas_UnitAlarm;
    [TabGroup("Dependencies")] public ConstantSizeChange squashAnimation;
    [TabGroup("Dependencies")] public FieldOfView fieldOfView;
    [TabGroup("Dependencies")] public SamuraiVisual visual;
    [TabGroup("Dependencies")] public BoxCollider2D closeAttackArea;
    [TabGroup("Dependencies")] public Unit_AttackTrigger attackTrigger;
    [TabGroup("Dependencies")] public Transform triggerPivot;
    [TabGroup("Dependencies")] public ExternalTrigger closeAttackTrigger;
    [TabGroup("Dependencies")] public GameObject sparkParticles;

    [TabGroup("DashAttack")] public bool canDashAttack = true;
    [TabGroup("DashAttack"), ShowIf("canDashAttack")] public float extraDashRange = 2;
    [TabGroup("DashAttack"), ShowIf("canDashAttack")] public float dashAtk_prepareDuration = 2;
    [TabGroup("DashAttack"), ShowIf("canDashAttack")] public float dashAtk_recoverDuration = 2;
    [TabGroup("DashAttack"), ShowIf("canDashAttack")] public float dashSpeed = 5;
    [TabGroup("DashAttack"), ShowIf("canDashAttack")] public AnimationCurve dashMovementCurve = AnimationCurve.EaseInOut(0,0,1,1);

    public bool CanDashAttack => canCloseAttack && waypoints != null && waypoints.Length > 0;

    public AnimationCurve curveBrightPrepareCurve = new AnimationCurve();

    [TabGroup("DashAttack"), ShowIf("canDashAttack")] public AnimationCurve aimingCurve;

    [TabGroup("CloseAttack")] public bool canCloseAttack = true;
    [TabGroup("CloseAttack"), ShowIf("canCloseAttack")] public bool canInstantCloseAttack = true;
    [TabGroup("CloseAttack"), ShowIf("canCloseAttack")] public float closeAttackPrepareRange = 4;
    [TabGroup("CloseAttack"), ShowIf("canCloseAttack")] public float closeAtk_prepareDuration = 2;
    [TabGroup("CloseAttack"), ShowIf("canCloseAttack")] public float closeAtk_recoverDuration = 1;

    private bool isAlerted = false;

    private Vector2[] waypoints;

    private bool alarmRaised = false;

    protected override void Awake()
    {
        base.Awake();
        waypoints = CollectPositions().ToArray();
    }

    protected override void OnDead(Health obj)
    {
        base.OnDead(obj);
        attackTrigger.enabled = false;
    }

    private void OnCloseAttackTrigger(Collider2D obj)
    {
    }

    private void Start()
    {
        GoTo_NoAlertPatrol_State();
    }

    protected override void OnAlarmRaised(BaseEnemyUnitMonoBehaviour source)
    {
        if(CurrentState is  PatrolUnitState)
            GoTo_AlertedPatrol_State();
        else
            GoTo_AlertedPatrol_State();
    }

    private void GoTo_NoAlertPatrol_State()
    {
        fieldOfView.CanSeeHiddenUnits = false;
        visual.SetIdle();
        SwitchState(new SamuraiPatrolUnitState(this, squashAnimation, waypoints, fieldOfView, closeAttackTrigger, noAlertColor, true, patrolSpeed, waitTimeOnPointNoAlert, waitTimeOnPathDoneNoAlert, TargetFoundWhenPatrolling, TargetStayWhenPatrolling, false, GoToCloseAttackState));
    }

    private void GoTo_AlertedPatrol_State()
    {
        fieldOfView.CanSeeHiddenUnits = true;
        visual.SetIdle();
        SwitchState(new SamuraiPatrolUnitState(this, squashAnimation, waypoints, fieldOfView, closeAttackTrigger, lowAlertColor, true, highAlertPatrolSpeed, waitTimeOnPointAlerted, waitTimeOnPathDoneAlerted,  TargetFoundWhenPatrolling, TargetStayWhenPatrolling, true, GoToCloseAttackState));
    }

    private void TargetStayWhenPatrolling(Player target)
    {
        TryAttackPlayer(target);
    }

    private void TargetFoundWhenPatrolling(Player target)
    {
        if (!alarmRaised)
            canvas_UnitAlarm.StartCounter();

        if (!TryAttackPlayer(target))
            GoTo_AlertedPatrol_State();
    }

    private bool TryAttackPlayer(Player target)
    {
        Debug.Log($"<<<<<<<<<   TargetFoundWhenPatrolling:");
        Debug.Log($"IsInCloseAttackPrepareRange(target.transform.position): {IsInCloseAttackPrepareRange(target.transform.position)}");
        if (canCloseAttack && IsInCloseAttackPrepareRange(target.transform.position)) //Puede hacer ataque corto y esta en rango de preparacion de ataque corto
        {
            Debug.Log($"<<<<<<<<<   CloseAttack:");
            if (canInstantCloseAttack && IsInCloseAttackPrepareRange(target.transform.position)) //Esta en rango de ataque y puede atacar instantaneamente al estar cerca
                GoToCloseAttackState();
            else
            {
                fieldOfView.areaColor = highAlertColor;
                SwitchState(
                   new PrepareCloseAttack_State(this, visual, fieldOfView, closeAttackTrigger, curveBrightPrepareCurve, target, closeAttackArea, canInstantCloseAttack, closeAtk_prepareDuration, GoToCloseAttackState)); //Preparar ataque
            }
            return true;
        }

        if (CanDashAttack && IsInDashAttackRange(target.transform.position)) //Ataque dash
        {
            Debug.Log($"<<<<<<<<<   DashAttack:");
            fieldOfView.areaColor = highAlertColor;
            SwitchState(new PrepareDashAttack_State(
                this,
                visual,
                fieldOfView,
                curveBrightPrepareCurve,
                closeAttackTrigger,
                target,
                aimingCurve,
                canInstantCloseAttack,
                GetFarthestWaypointPosition(GetAttackDir(target.transform.position)),
                dashAtk_prepareDuration,
                GoToDashAttackState,
                GoToCloseAttackState));
            return true;
        }
        return false;
    }


    internal void SwordsClash(Player p)
    {
        if (CurrentState is DashAttack_State dashAttack_State)
            dashAttack_State.SelfEndPosition();
    }

    public void GoToDashAttackState(Vector2 targetPosition)
    {
        SwitchState(new DashAttack_State(this, visual, fieldOfView, targetPosition, dashSpeed, dashMovementCurve,  GoToRecoverFromDashAttackState));
    }

    private void GoToRecoverFromDashAttackState()
    {
        SwitchState(new RecoverFromAttack_State(this, visual, fieldOfView, dashAtk_recoverDuration, GoTo_AlertedPatrol_State));
    }

    private void GoToRecoverFromCloseAttackState()
    {
        SwitchState(new RecoverFromAttack_State(this, visual, fieldOfView, closeAtk_recoverDuration, GoTo_AlertedPatrol_State));
    }

    private void GoToCloseAttackState()
    {
        fieldOfView.areaColor = highAlertColor;
        SwitchState(new CloseAttack_State(this, visual,  GoToRecoverFromCloseAttackState));
    }

    private bool IsInCloseAttackPrepareRange(Vector3 position)
    {
        int dir = GetAttackDir(position);
        float range = closeAttackPrepareRange;
        return ((position.x - transform.position.x) * dir) <= range;
    }

    private bool IsInDashAttackRange(Vector3 position)
    {
        int dir = GetAttackDir(position);
        float range = GetDashRange(dir) + extraDashRange;
        return ((position.x - transform.position.x) * dir) <= range;
    }

    private int GetAttackDir(Vector3 position) => (int)Mathf.Sign(position.x - transform.position.x);

    private bool IsInCloseAttackRange(Vector3 position) => closeAttackArea.OverlapPoint(position);

    private float GetDashRange(int dir)
    {
        Vector2 dashPos = GetFarthestWaypointPosition(dir);
        return (dashPos.x - transform.position.x) * dir;
    }

    private Vector2 GetFarthestWaypointPosition(int dir)
    {
        Vector2 endPos = waypoints[0];
        float endDist = 0;
        for (int i = 0; i < waypoints.Length; i++)
        {
            if (waypoints[i].x * dir < 0) continue;
            float dist = (waypoints[i].x - transform.position.x) * dir;
            if(dist > endDist)
            {
                endDist = dist;
                endPos = waypoints[i];
            }
        }
        return endPos;
    }

    public override void Flip()
    {
        base.Flip();
        visual.SetLookDir(LookDir);
        fieldOfView.transform.localPosition = new Vector2(fieldOfView.transform.localPosition.x * -1, fieldOfView.transform.localPosition.y);
        fieldOfView.Flip();
        closeAttackArea.transform.localPosition = new Vector2(closeAttackArea.transform.localPosition.x * -1, closeAttackArea.transform.localPosition.y);
        triggerPivot.localScale = new Vector3(triggerPivot.transform.localScale.x * -1, triggerPivot.transform.localScale.y, 1);
    }

    public void OnAttackDone(Collider2D col, bool isLethal)
    {
    }

    internal void ParryHook(HookKunai hookKunai)
    {
        GoToCloseAttackState();
     /*   GameObject go = Instantiate(sparkParticles);
        go.transform.position = hookKunai.transform.position;
        Destroy(go, 5);*/
        hookKunai.Cancel();
    }
}

public class RecoverFromAttack_State : BaseEnemyUnitState
{
    float closeAtk_recoverDuration;
    float t = 0;
    private SamuraiUnit samuraiUnit;
    private SamuraiVisual visual;
    Action OnRecoverDone;
    FieldOfView fov;
    float startingDrawPercentage;

    public RecoverFromAttack_State(SamuraiUnit samuraiUnit, SamuraiVisual visual, FieldOfView fov, float closeAtk_recoverDuration, Action OnRecoverDone) : base(samuraiUnit)
    {
        this.fov = fov;
        this.OnRecoverDone = OnRecoverDone;
        this.samuraiUnit = samuraiUnit;
        this.visual = visual;
        this.closeAtk_recoverDuration = closeAtk_recoverDuration;
        startingDrawPercentage = fov.drawPercentage;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        t += Time.fixedDeltaTime / closeAtk_recoverDuration;
        fov.drawPercentage = Mathf.Lerp(startingDrawPercentage, 1, t);
        if (t >= 1)
            OnRecoverDone.Invoke();
    }
}

public class PrepareDashAttack_State : BaseEnemyUnitState
{
    private SamuraiUnit samuraiUnit;
    private SamuraiVisual visual;
    private float dashAtk_prepareDuration;
    private Action<Vector2> goToDashAttackState;
    private FieldOfView fieldOfView;
    float t = 0;
    Vector2 attackPosition;
    AnimationCurve aimingCurve;
    bool canInstantCloseAttack;
    Player target;
    private Color startColor;
    private readonly Color whiteColor;
    AnimationCurve brightCurve;
    ExternalTrigger closeAttackTrigger;
    Action goToCloseAttack;
    public PrepareDashAttack_State(SamuraiUnit samuraiUnit, SamuraiVisual visual, FieldOfView fov, AnimationCurve brightCurve, ExternalTrigger closeAttackTrigger, Player target, AnimationCurve aimingCurve, bool canInstantCloseAttack, Vector2 attackPosition, float dashAtk_prepareDuration, Action<Vector2> goToDashAttackState, Action goToCloseAttack) : base(samuraiUnit)
    {
        this.goToCloseAttack = goToCloseAttack;
        this.closeAttackTrigger = closeAttackTrigger;
        this.brightCurve = brightCurve;
        this.target = target;
        this.canInstantCloseAttack = canInstantCloseAttack;
        this.fieldOfView = fov;
        this.aimingCurve = aimingCurve;
        this.attackPosition = attackPosition;
        this.samuraiUnit = samuraiUnit;
        this.visual = visual;
        this.dashAtk_prepareDuration = dashAtk_prepareDuration;
        this.goToDashAttackState = goToDashAttackState;
        whiteColor = Color.white;
    }

    public override void Update()
    {
        base.Update();
        t += Time.deltaTime / dashAtk_prepareDuration;
        fieldOfView.drawPercentage = 1 - aimingCurve.Evaluate(t);
        fieldOfView.areaColor = Color.Lerp(startColor, whiteColor, brightCurve.Evaluate(t));
        if (t >= 1)
            goToDashAttackState?.Invoke(attackPosition);
    }

    public override void Enter()
    {
        base.Enter();
        startColor = fieldOfView.areaColor;
        visual.SetPrepareDashAttack();
        closeAttackTrigger.OnEnter += OnEnterCloseAttackTrigger;
    }
    public override void Exit()
    {
        base.Exit();
        closeAttackTrigger.OnEnter -= OnEnterCloseAttackTrigger;
        goToDashAttackState = null;
    }

    private void OnEnterCloseAttackTrigger(Collider2D obj)
    {
        if (obj.gameObject.CompareTag("Player"))
            goToCloseAttack.Invoke();
        else if (obj.gameObject.CompareTag("Kunai"))
            samuraiUnit.ParryHook(obj.gameObject.GetComponent<HookKunai>());
    }
}

public class DashAttack_State : BaseEnemyUnitState
{
    private SamuraiUnit samuraiUnit;
    private SamuraiVisual visual;
    private Action goToRecoverFromDashAttackState;
    private float dashSpeed;
    private float dashDuration;
    private AnimationCurve dashMovementCurve;
    float t = 0;
    Vector2 startPosition;
    Vector2 endPosition;
    FieldOfView fieldOfView;
    public DashAttack_State(SamuraiUnit samuraiUnit, SamuraiVisual visual, FieldOfView fieldOfView, Vector2 targetPosition, float dashSpeed, AnimationCurve dashMovementCurve, Action goToRecoverFromDashAttackState) : base(samuraiUnit)
    {
        endPosition = targetPosition;
        this.samuraiUnit = samuraiUnit;
        this.visual = visual;
        this.dashSpeed = dashSpeed;
        this.dashMovementCurve = dashMovementCurve;
        this.goToRecoverFromDashAttackState = goToRecoverFromDashAttackState;
        this.fieldOfView = fieldOfView;
    }

    public override void Enter()
    {
        fieldOfView.drawPercentage = 0;
        base.Enter();
        startPosition = Owner.transform.position;
        visual.SetDashAttack();
        dashDuration = Vector2.Distance(startPosition, endPosition) / dashSpeed;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        t += Time.fixedDeltaTime / dashDuration;
        Vector2 targetPos = Vector2.Lerp(startPosition, endPosition, dashMovementCurve.Evaluate(t));
        Owner.transform.Translate((Vector3)targetPos - Owner.transform.position, Space.World);
        if (t >= 1)
            goToRecoverFromDashAttackState.Invoke();
    }

    internal void SelfEndPosition()
    {
        endPosition = Owner.transform.position;
    }
}

public class CloseAttack_State : BaseEnemyUnitState
{
    private SamuraiUnit samuraiUnit;
    private SamuraiVisual samuraiVisual;
    private Action GoToRecoverState;
    
    public CloseAttack_State(SamuraiUnit samuraiUnit, SamuraiVisual samuraiVisual,  Action GoToRecoverState) : base(samuraiUnit)
    {
        this.GoToRecoverState = GoToRecoverState;
        this.samuraiUnit = samuraiUnit;
        this.samuraiVisual = samuraiVisual;
    }
   
    public override void Enter()
    {
        base.Enter();
        samuraiVisual.SetCloseAttack();
        GoToRecoverState.Invoke();
    }
}

public class PrepareCloseAttack_State : BaseEnemyUnitState
{
    private SamuraiUnit samuraiUnit;
    private SamuraiVisual samuraiVisual;
    private bool canInstantCloseAttack;
    private float closeAtk_prepareDuration;
    private Action goToCloseAttackState;
    private Collider2D closeAttackRangeBox;
    float t = 0;
    Player target;
    FieldOfView fieldOfView;
    ExternalTrigger closeAttackTrigger;
    AnimationCurve prepareBrightCurve;
    private Color startColor;
    private readonly Color brightColor;

    public PrepareCloseAttack_State(SamuraiUnit samuraiUnit, SamuraiVisual samuraiVisual, FieldOfView fieldOfView, ExternalTrigger closeAttackTrigger, AnimationCurve prepareBrightCurve, Player target, Collider2D closeAttackRangeBox, bool canInstantCloseAttack, float closeAtk_prepareDuration,  Action goToCloseAttackState) : base(samuraiUnit)
    {
        this.prepareBrightCurve = prepareBrightCurve;
        this.closeAttackTrigger = closeAttackTrigger;
        this.closeAttackRangeBox = closeAttackRangeBox;
        this.target = target;
        this.samuraiUnit = samuraiUnit;
        this.samuraiVisual = samuraiVisual;
        this.canInstantCloseAttack = canInstantCloseAttack;
        this.closeAtk_prepareDuration = closeAtk_prepareDuration;
        this.goToCloseAttackState = goToCloseAttackState;
        this.fieldOfView = fieldOfView;
        brightColor = Color.white;
    }

    public override void Enter()
    {
        base.Enter();
        startColor = fieldOfView.areaColor;
        samuraiVisual.SetPrepareCloseAttack();
        closeAttackTrigger.OnEnter += OnEnterCloseAttackTrigger;
    }

    public override void Exit()
    {
        base.Exit();
        closeAttackTrigger.OnEnter -= OnEnterCloseAttackTrigger;
    }

    private void OnEnterCloseAttackTrigger(Collider2D obj)
    {
        if(obj.GetComponentInParent<Player>() != null)
            ExecuteAttack();
    }

    public override void Update()
    {
        base.Update();
        t += Time.deltaTime / closeAtk_prepareDuration;
        fieldOfView.areaColor = Color.Lerp(startColor,brightColor, prepareBrightCurve.Evaluate(t));
        if (t >= 1)
            ExecuteAttack();
    }

    private bool IsTargetIsRange()=> closeAttackRangeBox.OverlapPoint(target.gameObject.transform.position);

    private void ExecuteAttack()
    {
        goToCloseAttackState.Invoke();
    }
}

public class SamuraiPatrolUnitState : PatrolUnitState
{
    private Action<Player> targetStayWhenPatrolling;
    private bool startWithClosestToPlayer;
    private ExternalTrigger closeAttackTrigger;
    private readonly Action GoToCloseAttack;
    SamuraiUnit samurai;

    public SamuraiPatrolUnitState(SamuraiUnit owner, ConstantSizeChange squashAnimation, Vector2[] patrolPath, FieldOfView fieldOfView, ExternalTrigger closeAttackTrigger, Color color, bool isLooped, float speed, float waitTimeOnPoint, float waitTimeOnPathDone, Action<Player> onTargetFoundWhenPatrolling, Action<Player> targetStayWhenPatrolling, bool startWithClosestToPlayer, Action GoToCloseAttack) : base(owner, squashAnimation, patrolPath, fieldOfView, color, isLooped, speed, waitTimeOnPoint, waitTimeOnPathDone, onTargetFoundWhenPatrolling)
    {
        samurai = owner;
        this.closeAttackTrigger = closeAttackTrigger;
        this.startWithClosestToPlayer = startWithClosestToPlayer;
        this.targetStayWhenPatrolling = targetStayWhenPatrolling;
        this.GoToCloseAttack = GoToCloseAttack;
    }

    public override void Enter()
    {
        fieldOfView.OnTargetStay += OnTargetStay;
        if (startWithClosestToPlayer)
        {
            index = GetIndexOfClosesWaypointToPlayer() - 1;
            Debug.Log($"Closest{index}");
        }
        base.Enter();
        closeAttackTrigger.OnEnter += OnEnterCloseAttackTrigger;
        TryFlip();
    }

    public override void Exit()
    {
        closeAttackTrigger.OnEnter -= OnEnterCloseAttackTrigger;
        base.Exit();
        fieldOfView.OnTargetStay -= OnTargetStay;
    }

    private void OnEnterCloseAttackTrigger(Collider2D obj)
    {
        if (obj.gameObject.CompareTag("Kunai"))
            samurai.ParryHook(obj.GetComponent<HookKunai>());
        else if (obj.gameObject.CompareTag("Player"))
        {
            Player p = obj.GetComponentInParent<Player>();
            if(!p.IsHidden || p.IsHidden && fieldOfView.CanSeeHiddenUnits)
                GoToCloseAttack.Invoke();
        }
    }

    private int GetIndexOfClosesWaypointToPlayer()
    {
        int minIndex = -1;
        float distance = float.MaxValue;
        Player p = GameObject.FindObjectOfType<Player>();
        if(p != null)
        {
            for (int i = 0; i < patrolPath.Length; i++)
            {
                float dist = Vector2.Distance(p.gameObject.transform.position, patrolPath[i]);
                if (dist < distance)
                {
                    distance = dist;
                    minIndex = i;
                }
            }
        }
        return minIndex;
    }

    private void OnTargetStay(Transform obj)
    {
        Player p = obj.GetComponentInParent<Player>();
        if(p != null)
            targetStayWhenPatrolling?.Invoke(p);
    }
}


