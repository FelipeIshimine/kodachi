﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamuraiVisual : MonoBehaviour
{
    public Animator animator;
    public SpriteRenderer bodyRender;

    private void Awake()
    {
        SetIdle();
    }

    public void SetPrepareDashAttack() => animator.SetTrigger("DashAttack_Prepare");

    public void SetDashAttack() => animator.SetTrigger("DashAttack");

    public void SetRecoverFromDash() => animator.SetTrigger("DashAttack_Recover");

    public void SetIdle() => animator.SetTrigger("Idle");

    internal void SetPrepareCloseAttack() => animator.SetTrigger("CloseAttack_Prepare");

    public void SetCloseAttack() => animator.SetTrigger("CloseAttack");

    internal void SetRecoverFromCloseAttack() => animator.SetTrigger("CloseAttack_Recover");

    public void SetLookDir(int dir)
    {
        bodyRender.flipX = dir != 1;
    }
}
