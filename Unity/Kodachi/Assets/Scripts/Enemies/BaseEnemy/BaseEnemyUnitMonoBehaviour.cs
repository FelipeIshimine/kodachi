﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Health))]
public abstract class BaseEnemyUnitMonoBehaviour : MonoBehaviour, IStateMachine
{
    public static Action<BaseEnemyUnitMonoBehaviour> OnRaiseGlobalAlarm;
    public const float AlarmDistance = 20;

    public ParticleSystem deathParticle;
    Health health;
    public Action OnHooked;

    [TabGroup("NoAlert")] public Color noAlertColor;
    [TabGroup("Alerted")] public Color lowAlertColor;
    [TabGroup("Alerted")] public Color highAlertColor;

    [TabGroup("NoAlert")] public float patrolSpeed = 5;
    [TabGroup("NoAlert")] public float waitTimeOnPointNoAlert = 3;
    [TabGroup("NoAlert")] public float waitTimeOnPathDoneNoAlert = 3;

    [TabGroup("Alerted")] public float highAlertPatrolSpeed = 9;
    [TabGroup("Alerted")] public float waitTimeOnPointAlerted = 1.5f;
    [TabGroup("Alerted")] public float waitTimeOnPathDoneAlerted = 1.5f;

    [Header("Patrol")] public Transform[] pathPositions;
    protected bool IsHooked { get; private set; }
    protected bool IsHookStuned => stunTimer > 0;

    #region IStateMachine
    MonoBehaviourState MonoBehaviourState;
    public IState CurrentState =>               MonoBehaviourState.CurrentState;
    public void SwitchState(IState nState) =>   MonoBehaviourState.SwitchState(nState);
    #endregion

    public float hookStunDuration = 1;
    private float stunTimer = 0;
    protected int LookDir = 1;

    protected virtual void Awake()
    {
        OnRaiseGlobalAlarm += OnAlarmRaised;
        MonoBehaviourState = new MonoBehaviourState();
        _Initialize();
    }

    protected virtual void _Initialize()
    {
        health = GetComponent<Health>();
        health.Initialize(1);
        health.OnPreDead += OnPreDead;
        health.OnDead += OnDead;
    }

    protected virtual void Update()
    {
        MonoBehaviourState.Update();
    }

    protected virtual void LateUpdate()
    {
        MonoBehaviourState.LateUpdate();
    }

    protected virtual void FixedUpdate()
    {
        MonoBehaviourState.FixedUpdate();
        if (stunTimer >0)
            stunTimer -= Time.fixedDeltaTime;
    }

    protected virtual void OnDisable()
    {
        MonoBehaviourState.OnDisable();
    }

    protected virtual void OnEnable()
    {
        MonoBehaviourState.OnEnable();
    }

    protected virtual void OnDestroy()
    {
        MonoBehaviourState?.OnDestroy();
        OnRaiseGlobalAlarm -= OnAlarmRaised;
        health.OnPreDead -= OnPreDead;
        health.OnDead -= OnDead;
    }


    private void OnPreDead(Health obj)
    {
    }

    protected virtual void OnDead(Health obj)
    {
        GameObject go = Instantiate(deathParticle.gameObject, null).gameObject;
        go.transform.position = transform.position;
        Destroy(go, 5);
        Destroy(gameObject);
    }

    public virtual void Hook()
    {
        IsHooked = true;
        stunTimer = hookStunDuration;
        OnHooked?.Invoke();
    }

    public virtual void RaiseAlarm()
    {
        OnRaiseGlobalAlarm?.Invoke(this);
    }

    public virtual void Flip()
    {
        LookDir *= -1;
    }

    protected abstract void OnAlarmRaised(BaseEnemyUnitMonoBehaviour source);

    public bool IsBehindMe(Vector3 position)
    {
        return ((LookDir == 1 && position.x < transform.position.x) || (LookDir == -1 && position.x > transform.position.x));
    }

    protected List<Vector2> CollectPositions()
    {
        List<Vector2> positions = new List<Vector2>();
        for (int i = 0; i < pathPositions.Length; i++)
            positions.Add(pathPositions[i].position);
        return positions;
    }
}
