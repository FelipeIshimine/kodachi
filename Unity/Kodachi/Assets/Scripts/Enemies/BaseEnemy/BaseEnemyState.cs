﻿public abstract class BaseEnemyUnitState : MonoBehaviourState
{
    protected BaseEnemyUnitMonoBehaviour Owner;
    public BaseEnemyUnitState(BaseEnemyUnitMonoBehaviour BaseEnemy) : base()
    {
        this.Owner = BaseEnemy;
    }
}
