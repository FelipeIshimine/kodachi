﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy_0 : MonoBehaviour
{
    public ParticleSystem deathParticle;
    Health health;
    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        health = GetComponent<Health>();
        health.Initialize(1);
        health.OnDead += OnDead;
    }

    private void OnDestroy()
    {
        health.OnDead -= OnDead;
    }

    private void OnDead(Health obj)
    {
        GameObject go = Instantiate(deathParticle,null).gameObject;
        go.transform.position = transform.position;
        Destroy(go, 5);
        Destroy(gameObject);
    }
}
