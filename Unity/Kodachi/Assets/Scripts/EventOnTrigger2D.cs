﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class EventOnTrigger2D : MonoBehaviour
{
    [SerializeField] private Collider2D col;

    public UnityTrigger2DEvent OnEnter;
    public UnityTrigger2DEvent OnStay;
    public UnityTrigger2DEvent OnExit;

    private void OnValidate()
    {
        if (col == null)
            col = GetComponent<Collider2D>();
    }
    private void Awake()
    {
        col.isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D collision) => OnEnter?.Invoke(collision);
    private void OnTriggerStay2D(Collider2D collision) => OnStay?.Invoke(collision);
    private void OnTriggerExit2D(Collider2D collision) => OnExit?.Invoke(collision);
}

[System.Serializable]
public class UnityTrigger2DEvent: UnityEvent<Collider2D>
{

}