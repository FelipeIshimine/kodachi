﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class ExternalTrigger : MonoBehaviour
{
    public LayerMask targetMask = ~0;
    public bool IsActive { get; set; } = true;
    public Action<Collider2D> OnEnter;
    public Action<Collider2D> OnStay;
    public Action<Collider2D> OnExit;

    public BoxCollider2D col;

    public void Initialize(object other)
    {
        Register(other);
    }

    private void Register(object other)
    {
        if (other is IUseOnTriggerEnter2D useOnTriggerEnter)
            OnEnter += useOnTriggerEnter.OnTriggerEnter2D;

        if (other is IUseOnTriggerExit2D useOnTriggerExit)
            OnExit += useOnTriggerExit.OnTriggerExit2D;

        if (other is IUseOnTriggerStay2D useOnTriggerStay)
            OnStay += useOnTriggerStay.OnTriggerStay2D;
    }

    private void Unregister(object other)
    {
        if (other is IUseOnTriggerEnter2D useOnTriggerEnter)
            OnEnter -= useOnTriggerEnter.OnTriggerEnter2D;

        if (other is IUseOnTriggerExit2D useOnTriggerExit)
            OnExit -= useOnTriggerExit.OnTriggerExit2D;

        if (other is IUseOnTriggerStay2D useOnTriggerStay)
            OnStay -= useOnTriggerStay.OnTriggerStay2D;
    }

    private void OnDestroy()
    {
        OnEnter = null;
        OnExit = null;
        OnStay = null;
    }

    private void OnValidate()
    {
        if (col == null)
        {
            col = GetComponent<BoxCollider2D>();
            col.isTrigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!targetMask.Contains(collision.gameObject.layer))
            return;

        if (!IsActive) return;
        OnExit?.Invoke(collision);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!targetMask.Contains(collision.gameObject.layer))
            return;

        if (!IsActive) return;
        OnEnter?.Invoke(collision);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!targetMask.Contains(collision.gameObject.layer))
            return;

        if (!IsActive) return;
        OnStay?.Invoke(collision);
    }
}

public interface IUseOnTriggerEnter2D
{
    void OnTriggerEnter2D(Collider2D col);
}
public interface IUseOnTriggerExit2D
{
    void OnTriggerExit2D(Collider2D col);
}
public interface IUseOnTriggerStay2D
{
    void OnTriggerStay2D(Collider2D col);
}