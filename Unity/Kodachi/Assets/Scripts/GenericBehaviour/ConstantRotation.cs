﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    public float speed = 360;

    public void Update()
    {
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
    }
}
