﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantFloating : MonoBehaviour
{
    public float rate;

    public AnimationCurve curve;
    public Vector2 direction;
    public Transform target;

    private float t = 0;

    private void Awake()
    {
        t = 0;
    }

    public void Update()
    {
        t += Time.deltaTime * rate;
        t = t % 1;
        target.localPosition = curve.Evaluate(t) * direction;
    }
}
