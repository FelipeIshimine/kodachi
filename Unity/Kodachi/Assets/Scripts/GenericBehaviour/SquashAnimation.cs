﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SquashAnimation : MonoBehaviour
{
    public Vector2 extraSize;
    public AnimationCurve xCurve = AnimationCurve.EaseInOut(0,0,1,1);
    public AnimationCurve yCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public float duration = 1;
    public Transform root;

    IEnumerator rutine;

    public IEnumerator AnimationRutine()
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / duration;
            root.transform.localScale = Vector2.one + (Vector2)root.transform.up * xCurve.Evaluate(t) * extraSize.x + (Vector2)root.transform.right * yCurve.Evaluate(t) * extraSize.y;
            yield return null;
        } while (t < 1);
    }

    [Button]
    public void Play()
    {
        if (rutine != null) StopCoroutine(rutine);
        rutine = AnimationRutine();
        StartCoroutine(rutine);
    }

}
