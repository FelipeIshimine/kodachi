﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shine : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public float duration = .3f;
    public AnimationCurve alphaCurve;
    IEnumerator rutine;

    public void Go(int i)
    {
        Go();
    }
    public void Go()
    {
        if (rutine != null) StopCoroutine(rutine);
        rutine = ShineRutine();
        StartCoroutine(rutine);
    }

    IEnumerator ShineRutine()
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / duration;
            spriteRenderer.color = Color.Lerp(Color.white, Color.clear, t);
            yield return null;
        } while (t < 1);
            spriteRenderer.color = Color.clear;
    }
}
