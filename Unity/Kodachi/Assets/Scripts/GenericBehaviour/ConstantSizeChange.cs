﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantSizeChange : MonoBehaviour
{
    public Transform target;

     public Settings[] settings;

    public Settings currentSettings;

    public bool Playing { get; private set; } = false;
    float t = 0;

    int index = -1;

    private void Awake()
    {
        currentSettings = settings[0];
    }

    public void SetCicleDuration(float nCicle, bool resetTime)
    {
        currentSettings.CicleDuration = nCicle;
        if (resetTime)
            t = 0;
    }

    public void Play(bool resetTime, float cicleDuration = -1)
    {
        if(cicleDuration != -1)
            SetCicleDuration(cicleDuration, resetTime);
        Playing = true;
    }

    public void Stop()
    {
        Playing = false;
    }

    public void Update()
    {
        if (!Playing) return;
        t = (t + Time.deltaTime / currentSettings.CicleDuration) % 1;
        target.localScale = Vector2.one + currentSettings.extraScale * currentSettings.Curve.Evaluate(t);
    }

    public void NextSettings()
    {
        index = (index + 1 + settings.Length) % settings.Length;
        currentSettings = settings[index];
    }

    public void PreviousSettings()
    {
        index = (index - 1+ settings.Length) % settings.Length;
        currentSettings = settings[index];
    }

    [System.Serializable]
    public struct Settings
    {
        public AnimationCurve Curve;
        public Vector2 extraScale;
        public float CicleDuration;
    }
}

