﻿using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class Health : MonoBehaviour, IHaveHealth
{
    public static Action<Health> OnGlobalDead;

    public bool Invulnerable = false;

    [SerializeField]
    private int max = 1;
    public int Max
    {
        get { return max; }
        set { max = value; }
    }

    [SerializeField]
    private int current;

    internal void Instakill()
    {
        Damage(max);
    }

    public int Current
    {
        get { return current; }
        set { current = value; }
    }

    public Action<float> OnDamageReceived { get; set; }
    public Action<float> OnHealReceived { get; set; }
    public Action<Health> OnPreDead { get; set; }
    public Action<Health> OnDead { get; set; }

    public bool IsAlive => Current > 0;

    int IHaveHealth.Current => Current;

    int IHaveHealth.Max => Max;

    private void Awake()
    {
        Initialize();
    }

    public void Initialize()
    {
        Initialize(max);
    }

    public void Initialize(int maxValue)
    {
        Max = maxValue;
        Current = max;
    }

    public bool Damage(int ammount)
    {
        Modify(-ammount);
        return Current <= 0;
    }

    public void Heal(int ammount)
    {
        Modify(ammount);
    }

    [Button]
    public void Modify(int ammount)
    {
        if (!enabled || ammount == 0 || !IsAlive || Invulnerable) return;

        Current += ammount;

        if (ammount < 0)
            OnDamageReceived?.Invoke(-ammount);
        else
            OnHealReceived?.Invoke(ammount);

        if (Current <= 0)
        {
            OnPreDead?.Invoke(this);
            OnDead?.Invoke(this);
            OnGlobalDead?.Invoke(this);
        }
    }

    int IHaveHealth.Damage(int ammount)
    {
        Damage(ammount);
        return Current;
    }

    int IHaveHealth.Heal(int ammount)
    {
        Heal(ammount);
        return Current;
    }
}
