﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_GrandMaster : MonoBehaviour
{
    public Animator animator;
    public void Idle()
    {
        animator.SetTrigger("Idle");
    }

    public void Dissapear()
    {
        animator.SetTrigger("Dissapear");
    }

    public void Appear()
    {
        animator.SetTrigger("Appear");
    }
}
