﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimelinePlayerTrigger : MonoBehaviour
{
    public string animatorBindName = "Player_Animator";
    public bool deactivateOnTrigger = true;

    public TimelinePlayer timelinePlayer;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Player p = collision.gameObject.GetComponentInParent<Player>();
        if(p != null)
            BindPlayerAndPlayCutscene(p);
    }

    public void BindPlayerAndPlayCutscene(Player p)
    {
        Animator animator = p.GetComponent<Animator>();
        timelinePlayer.InsertBind(new BindElement(animatorBindName, animator));
        if (deactivateOnTrigger)
            gameObject.SetActive(false);
        timelinePlayer.Play(OnCutsceneDone);
    }

    private void OnCutsceneDone()
    {
    }
}
