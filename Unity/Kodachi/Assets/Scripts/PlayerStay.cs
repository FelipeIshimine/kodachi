﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerStay : MonoBehaviour
{
    public Transform stickPoint;
    public Vector2 StickPosition => stickPoint.position;
    public ExternalTrigger externalTrigger;
    [ShowInInspector, ReadOnly] private bool justUsed = false;
    private Unhookable_Timed Unhookable_Timed;

    private void Awake()
    {
        externalTrigger.OnEnter += OnEnter;
        externalTrigger.OnExit += OnExit;
    }
    private void OnDestroy()
    {
        externalTrigger.OnEnter -= OnEnter;
        externalTrigger.OnExit -= OnExit;
    }

    private void OnEnter(Collider2D obj)
    {
        if (obj.attachedRigidbody != null && obj.attachedRigidbody.CompareTag("Player") && !justUsed)
        {
            obj.attachedRigidbody.GetComponentInParent<Player>().StickPoint(this);
        }
    }

    internal void StartUse()
    {
        Unhookable_Timed = gameObject.AddComponent<Unhookable_Timed>();
        externalTrigger.col.enabled = false;
    }

    internal void EndUse()
    {
        Unhookable_Timed.Initialize(.5f);
        justUsed = true;
        externalTrigger.col.enabled = true;
    }

    private void OnExit(Collider2D obj)
    {
        if (justUsed && obj.attachedRigidbody != null && obj.attachedRigidbody.CompareTag("Player"))
        {
            externalTrigger.col.enabled = true;
            justUsed = false;
        }
    }
}
