﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public SoundPlay mainTrack;

    private static Action OnPlay;
    private static Action OnStop;

    private void Awake()
    {
        OnPlay += PlayMain;
        OnStop += StopMain;
    }
    private void OnDestroy()
    {
        OnPlay -= PlayMain;
        OnStop -= StopMain;
    }


    private void Start()
    {
        mainTrack.Play();
    }

    IEnumerator WaitAndPlay()
    {
        yield return new WaitForSeconds(5);
    }

    public static void PlayMainTrack()
    {
        OnPlay?.Invoke();
    }


    private void PlayMain()
    {
        mainTrack.Play();
    }

    public static void StopMainTrack()
    {
        OnStop?.Invoke();
    }

    private void StopMain()
    {
        mainTrack.Stop();
    }
}
