﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Sirenix.OdinInspector;

[System.Serializable]
public class Sound
{
    [AssetList]
    public AudioClip clip;
    [Range(0, 1)]
    public float volume = 1;
    public AudioMixerGroup mixerGroup;
    public bool loop = false;
    public Sound()
    {
        clip = null;
        loop = false;
        mixerGroup = null;
        volume = 1;
    }

    public Sound(AudioClip clip)
    {
        this.clip = clip;
        loop = false;
        mixerGroup = null;
        volume = 1;
    }

    public Sound(AudioClip clip, float volume)
    {
        this.clip = clip;
        loop = false;
        mixerGroup = null;
        this.volume = volume;
    }

    public Sound(AudioClip clip, float volume, AudioMixerGroup audioMixer)
    {
        this.clip = clip;
        loop = false;
        mixerGroup = audioMixer;
        this.volume = volume;
    }
}
