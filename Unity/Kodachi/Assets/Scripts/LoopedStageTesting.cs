﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class LoopedStageTesting : MonoBehaviour
{
    public List<GameObject> loopedStagePrefabs;
    public LoopedStage loopedStage;
    public Player player;

    private List<GameObject> activeModules = new List<GameObject>();

    [Button]
    public void Initialize()
    {
        List<LoopedTerrainModule> loopedTerrainModules = new List<LoopedTerrainModule>();

        activeModules.DestroyContent();
        for (int i = 0; i < loopedStagePrefabs.Count; i++)
        {
            activeModules.Add(Instantiate(loopedStagePrefabs[i], null));
            loopedTerrainModules.Add(activeModules[i].GetComponent<LoopedTerrainModule>());
        }
        loopedStage.Initialize(loopedTerrainModules, player);
    }

}
