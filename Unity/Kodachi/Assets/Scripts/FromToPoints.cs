﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FromToPoints : MonoBehaviour
{
    public Vector2 From { get => from + (Vector2)transform.position; set => from = value - (Vector2)transform.position; }
    public Vector2 To { get => to + (Vector2)transform.position; set => to = value - (Vector2)transform.position; }

    [SerializeField]  private Vector2 from;
    [SerializeField] private Vector2 to;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(From, .6f);
        Gizmos.DrawWireSphere(To, .35f);
        Gizmos.DrawLine(From, To);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1,1,1,.2f);
        Gizmos.DrawWireSphere(From, .6f);
        Gizmos.DrawWireSphere(To, .35f);
        Gizmos.DrawLine(From, To);
    }

    public override string ToString()
    {
        return $"FromToPoints       FROM:{from} TO:{to}";
    }
}
