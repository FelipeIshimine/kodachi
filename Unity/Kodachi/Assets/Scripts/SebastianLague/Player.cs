﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using System;
using FMODUnity;
using Cinemachine;

[RequireComponent(typeof(Controller2D))] 
public class Player : MonoBehaviour, IHide, IUseAttackTrigger, IHaveVelocity2D
{
    #region Events
    public Action OnEnemyKilled;
    public Action OnPlayerDead;
    #endregion

    [FoldoutGroup("Dependencies")] public CinemachineImpulseSource cinemachineImpulseSource;
    [FoldoutGroup("Dependencies")] public ParticleSystem clashParticles;
    [FoldoutGroup("Dependencies")] public Hook hook;
    [FoldoutGroup("Dependencies")] public VisualControl visualControl;
    [FoldoutGroup("Dependencies")] public TrailController trailController;
    [FoldoutGroup("Dependencies")] public ParticleSystem deathParticles;
    [FoldoutGroup("Dependencies")] public ParticleSystem deathParticles_Smoke;
    [FoldoutGroup("Dependencies")] public AttackTrigger attackTrigger;

    private Health health;
    private ScreenWrappable screenWrappable;
    
    private bool hasWeapon;

    public Vector2 minMaxJumpHeight = new Vector2(1,5.5f);

    public float timeToJumpApex = .4f;
    [HorizontalGroup("Airborne")] public float accelerationTimeAirborne = .2f;
    [HorizontalGroup("Airborne")] public float accelerationTimeGrounded = .1f;
    public Vector2 knockBackForce = new Vector2(20, 5);

    public bool isMoveFreezeCD = false;
    public float squashCD = .3f;
    private float squashTimer = 0;
    public float dashCD = .3f;

    private bool isPlayerActive = true;
    public float jumpCD = .2f;
    public float hookCD = .1f;

    [ShowInInspector] private float currentHookCd;
    private float cd = 0;

    private bool CanJump => cd <= 0;
    [ShowInInspector] private bool CanHook => currentHookCd <= 0;
    public float moveSpeed = 6;

    public float wallDashSpeedUp = 10;

    public Vector2 airDashForce = new Vector2(10, 5);
    public Vector2 jumpForce = new Vector2(20, 20);

    public float wallRunSpeed = -3;
    public float stickSpeed = 2;
    public float wallStickTime = .25f;

    public float gravity;
    public float activeGravity;

    float maxJumpVelocity;
    Vector2 velocity;
    public Vector2 Velocity => velocity;
    float velocityXSmoothing;

    PlayerStay currentStickPosition;
    Controller2D controller;

    Vector2 directionalInput;
    bool wallRunning;
    int wallDirX;

    IEnumerator rutine;

    public int currentDir = 1;
    [ShowInInspector] private bool justJump = false;
    [ShowInInspector] private bool canJump = false;
    [ShowInInspector] private bool canAirDash = false;

    private Vector2 hookedVelocity;
    bool canUseAirDash, canUseHook, canUseSquash;

    #region Checks
    public bool IsHidden { get; private set; }
    public bool IsHooked => hook.IsHooked;
    bool CanUseSquash => canUseSquash && squashTimer <= 0;
    public bool IsHookTravelling => hook.isHookTravelling;
    public bool Stuned => currentStun >= 0;
    internal bool InSquash()=>  squashTimer >= 0;
    #endregion

    Vector2 IHaveVelocity2D.Velocity
    {
        get => Velocity;
        set
        {
            velocity = value;
            currentDir = (int)Mathf.Sign(velocity.x);
        }
    }

    public float knockBackStunDuration = .5f;
    private float currentStun = 0;

    public bool isHanging = false;

    Vector2 stickedPoint;

    [Header("FMOD")]
    public StudioEventEmitter jumpSound;
    public StudioEventEmitter dashSound;
    public StudioEventEmitter ropeSound;
    public StudioEventEmitter deathSound;
    public StudioEventEmitter swordSound;
    public StudioEventEmitter deflectedSound;
    
    void Awake()
    {
        isPlayerActive = false;

        screenWrappable = GetComponent<ScreenWrappable>();
        health = GetComponent<Health>();
        controller = GetComponent<Controller2D>();

        screenWrappable.OnScreenWrapPrepare += ScreenWrapPrepare;
        screenWrappable.OnScreenWrapPerformed +=  ScreenWrapPerformed;

        gravity = -(2 * minMaxJumpHeight.y) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        activeGravity = gravity;

        Register();
    }

    private void Register()
    {
        health.OnDead += OnDead;
        hook.OnHookHit += OnHookColition;
        attackTrigger.OnAttackDone += OnAttackDone;
    }

    private void Unregister()
    {
        health.OnDead -= OnDead;
        hook.OnHookHit -= OnHookColition;
        attackTrigger.OnAttackDone -= OnAttackDone;
    }

    private void OnDead(Health h)
    {
        isPlayerActive = false;
        visualControl.gameObject.SetActive(false);


        deathParticles.transform.position = transform.position;
        deathParticles.transform.SetParent(null);
        deathParticles.Play();

        deathParticles_Smoke.transform.position = transform.position;
        deathParticles_Smoke.transform.SetParent(null);
        deathParticles_Smoke.Play();


        DropTrail();
        visualControl.HideScarft();
        OnPlayerDead?.Invoke();
        attackTrigger.enabled = false;
        deathSound.Play();
    }

    private void OnDisable()
    {
        Debug.Log("WTF");
    }

    private void OnDestroy()
    {
        Unregister();
        Destroy(deathParticles.gameObject);
        Destroy(deathParticles_Smoke.gameObject);
    }

    [Button]
    public void Initilize(bool isPlayerActive)
    {
        Initilize(isPlayerActive, visualControl.HasWeapon, canUseAirDash, canUseSquash, canUseHook, hook.UseAimAssist);
    }

    [Button]
    public void Initilize(bool isPlayerActive, bool hasWeapon, bool canUseAirDash, bool canUseSquash, bool canUseHook, bool useAimAssist = false)
    {
        gameObject.SetActive(true);
        health.Initialize(1);
        velocity = Vector3.zero;
        visualControl.HasWeapon = hasWeapon;
        attackTrigger.enabled = hasWeapon;
        this.hasWeapon = hasWeapon;
        this.isPlayerActive = isPlayerActive;
        this.canUseAirDash = canUseAirDash;
        this.canUseHook = canUseHook;
        this.canUseSquash = canUseSquash;
        hook.Initialize(canUseHook, useAimAssist);
        visualControl.gameObject.SetActive(true);

        deathParticles.transform.SetParent(transform);
        deathParticles_Smoke.transform.SetParent(transform);

        visualControl.ShowScarf();
        Unhide(0);
    }

    internal void AttackKnockbackFrom(Vector3 position, bool isFromSwordClash)
    {
        if(isFromSwordClash)
        {
            TimeManager._StartTimeShift(.1f, .1f, true, false);
            ParticleSystem particles = Instantiate(clashParticles.gameObject, null).GetComponent<ParticleSystem>();
            particles.Play();
            particles.transform.position = Vector2.Lerp(transform.position, position, .5f);
            Destroy(particles.gameObject, 2);
            attackTrigger.enabled = false;
            deflectedSound.Play();
        }

        currentStun = knockBackStunDuration;
        velocity = new Vector2(Mathf.Sign(transform.position.x - position.x) * knockBackForce.x, knockBackForce.y);
        SetDirectionalInput(new Vector2(Mathf.Sign(velocity.x),0));
    }

    void FixedUpdate()
    {
        if (!isPlayerActive) return;

        squashTimer -= Time.fixedDeltaTime;

        if (Stuned)
        {
            currentStun -= Time.fixedDeltaTime;
            if (!Stuned)
                attackTrigger.enabled = hasWeapon;
        }
        if (IsHidden)
        {
            controller.PreCalculatePosition(velocity);
            HandleWallRunning();
        }
        else
        {
            if (hook.IsHooked)
            {
                Vector2 dir = transform.position.GetDirection(hook.HookedPosition);

                velocity = Vector2.SmoothDamp(velocity, hook.goToPointSpeed * dir, ref hookedVelocity, (hook.HasHookedTarget) ? hook.agresiveSmooth : hook.normalSmooth, float.MaxValue, Time.fixedDeltaTime);

                if (hook.IsHookedTargetMovable && hook.HasHookedTarget && !hook.HookedTarget.Invulnerable)
                {   
                    Transform hookedTarget = hook.HookedTarget.transform;
                    hookedTarget.Translate(hookedTarget.position.GetDirection(transform.position) * hook.hookedTargetPoolForce * Time.fixedDeltaTime, Space.World);
                }
            }
            if (!justJump)
                SetDirectionalInput(Vector2.right * currentDir);
            else
                justJump = !justJump;

            if (isHanging)
            {
                if (Vector2.Distance(transform.position, stickedPoint) > .16f)
                    velocity = Vector2.SmoothDamp(velocity, stickSpeed * transform.position.GetDirection(stickedPoint), ref hookedVelocity, .02f, float.MaxValue, Time.fixedDeltaTime);
                else
                    velocity = Vector2.zero;
                SetDirectionalInput(Vector2.zero);
            }

            CalculateVelocity();
            HandleWallRunning();

            if (isMoveFreezeCD)
                velocity.x = 0;

            controller.Move(velocity * Time.fixedDeltaTime);

            if (hook.IsHooked)
                return;

            if (controller.collisions.above || controller.collisions.below)
            {
                if (controller.collisions.slidingDownMaxSlope)
                {
                    velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
                }
                else
                {
                    velocity.y = 0;
                }
                if (controller.collisions.below)
                {
                    visualControl.OnFloor(currentDir);
                    canAirDash = true;
                    canJump = true;
                }

                if (controller.collisions.left || controller.collisions.right)
                {
                    velocity.y = 1;
                    if (controller.collisions.above)
                    {
                        currentDir = ((controller.collisions.left) ? 1 : -1);
                        velocity.x = ((controller.collisions.left) ? moveSpeed : -moveSpeed) * .5f;
                    }
                }
            }
        }
    }

    internal void InvertXVelocity()
    {
        if (currentDir == Mathf.Sign(velocity.x))
            currentDir *= -1;
        velocity.x *= -1;
    }

    internal Vector2 NextPosition()
    {
        float targetVelocityX = directionalInput.x * moveSpeed;
        Vector2 tVelocity;
        float vSmoothing = velocityXSmoothing;
        tVelocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref vSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        tVelocity.y = velocity.y + gravity * Time.fixedDeltaTime;
        return (Vector2)transform.position + velocity * Time.fixedDeltaTime;
    }

    internal void ScreenWrapPerformed()
    {
        visualControl.FadeIn();
    }

    internal void NewTrail()
    {
        trailController.New();
    }

    internal void ScreenWrapPrepare()
    {
        hook.HideMarks();
        hook.Unhook();
        hook.HideHook();
    }

    internal void DropTrail()
    {
        trailController.Drop();
    }

    public void Update()
    {
        cd -= Time.deltaTime;
        currentHookCd -= Time.deltaTime;

        if (cd <= 0)
            isMoveFreezeCD = false;
    }

    private void SetHookCD(float nCd)
    {
        currentHookCd = nCd;
    }

    private void OnHookColition(int dir, RaycastHit2D hit)
    {
        if (isHanging)
            Unstick();

        visualControl.HookHit(dir);
        SetDirectionalInput(Vector2.right * dir);
        currentDir = dir;
        SetJumpCD(hookCD);
        canAirDash = true;
        canJump = true;
    }

    private void Unstick()
    {
        hookedVelocity = Vector2.zero;
        isHanging = false;
        visualControl.SetHanging(false, currentDir);
        gravity = activeGravity;
        currentStickPosition.EndUse();
        currentStickPosition = null;
    }

    public void SetDirectionalInput (Vector2 input)
    {
        currentDir = (int)Mathf.Sign(input.x);
        directionalInput = input;
	}

	public void Jump(int dir)
    {
        if (IsHidden)
            Unhide(dir);

        if (!CanJump || !isPlayerActive || Stuned) return;

        justJump = true;
        SetDirectionalInput(Vector2.right * dir);

        if (hook.IsHooked)
            hook.Unhook();

        if (wallRunning)
        {
            if (wallDirX == directionalInput.x && CanUseSquash) //Squash
            {
                OnWallDash(wallDirX);
                squashTimer = squashCD * 2;
                velocity.y = wallDashSpeedUp;
            }
            else if (directionalInput.x == -wallDirX) //Saltar
            {
                velocity.x = -wallDirX * jumpForce.x;
                velocity.y = jumpForce.y;
                OnJump(dir);
            }
        }
        else if (controller.collisions.below || isHanging)
        {
            velocity.x = moveSpeed * currentDir;
            if (controller.collisions.slidingDownMaxSlope)
            {
                if (directionalInput.x != -Mathf.Sign(controller.collisions.slopeNormal.x))
                { // not jumping against max slope
                    velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
                    velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
                }
            }
            else
                velocity = new Vector2(jumpForce.x * currentDir, jumpForce.y);
            OnJump(currentDir);
        }
        else if (canJump)
        {
            OnJump(currentDir);
            velocity = new Vector2(jumpForce.x * currentDir, jumpForce.y);
        }
        else if (canAirDash && canUseAirDash) //AirDash
        {
            velocity = new Vector2(currentDir * airDashForce.x, airDashForce.y);
            OnAirDash();
        }

        if (isHanging)
            Unstick();
    }

    private void OnWallDash(int dir)
    {
        //Audio
        dashSound.Play();

        isMoveFreezeCD = true;
        SetJumpCD(squashCD);
        visualControl.Squash(dir);
    }

    private void OnAirDash()
    {
        visualControl.Dash(currentDir);
        canAirDash = false;
        canJump = false;
        SetJumpCD(dashCD);
        dashSound.Play();
    }

    private void OnJump(int dir)
    {
        visualControl.Jump(dir);
        SetJumpCD(jumpCD);
        canJump = false;
        jumpSound.Play();
    }

    private void SetJumpCD(float nCD) => cd = nCD;

	void HandleWallRunning()
    {
		wallDirX = (controller.collisions.left) ? -1 : 1;
		wallRunning = false;

		if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below   /*&&velocity.y < 0*/)
        {
            wallRunning = true;

            if (velocity.y < wallRunSpeed && wallDirX == currentDir)
            {
                canAirDash = true;
                canJump = true;
                velocity.y = wallRunSpeed;
                visualControl.OnWall(wallDirX);
            }
        }
    }

	void CalculateVelocity()
    {
		float targetVelocityX = directionalInput.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.fixedDeltaTime;
	}

    [Button]
    public void OnAttackDone (Collider2D col, bool isLethal)
    {
        Debug.Log($"OnAttackDone:{isLethal}");
        if (isLethal)
        {
            Debug.Log("OnEnemyKilled");
            OnEnemyKilled?.Invoke();
            cinemachineImpulseSource.GenerateImpulse();
        }
        
        //swordSound.Play();
       /* if(isLethal)
            TimeManager.HitStop((canAirDash) ? TimeManager.HitLength.Short : TimeManager.HitLength.Long);*/
    }

    internal void SetEnable(bool value, bool resetRotation, bool resetVelocity)
    {
        Debug.Log($"<color=blue> >>>> SetEnable </color> <color=green> {value} </color>");
        controller.SetEnable(value); 
        isPlayerActive = value;

        if(!value && resetRotation)
            visualControl.SetEnable(value);

        if (!value && resetVelocity)
        {
            velocity = Vector2.zero;
            visualControl.SetIdle();
        }

        hook.SetEnable(value);
    }

    public void ShootHook(int dir)
    {
        if (Stuned)
            return;

        if (IsHidden)
            Unhide(dir);

        if (!CanHook || !isPlayerActive || !canUseHook)
        {
            Debug.Log($"CanHook: {CanHook}");
            Debug.Log($"isPlayerActive: {isPlayerActive}");
            Debug.Log($"canUseHook: {canUseHook}");
            return;
        }
        if (hook.Shoot(dir))
        {
            ropeSound.Play();
            SetHookCD(hookCD);
        }
    }

    internal void HideInSpot(Vector2 hidePosition)
    {
        IsHidden = true;
        visualControl.HideAll();
        attackTrigger.enabled = false;
        this.PlayCoroutine(ref transitionToHidePos, () => GoToPosition(.2f, hidePosition));
    }

    IEnumerator transitionToHidePos;

    IEnumerator GoToPosition(float time, Vector2 targetPos)
    {
        float t = 0;
        Vector2 startPos = transform.position;
        do
        {
            t += Time.deltaTime / time;
            yield return null;
            transform.position = Vector2.Lerp(startPos, targetPos, t);
        } while (t<1);
    }

    public void Unhide(int dir)
    {
        if(transitionToHidePos != null)
            this.StopCoroutine(transitionToHidePos);

        IsHidden = false;
        visualControl.ShowAll();
        attackTrigger.enabled = hasWeapon;
    }

    internal void CancelHook()
    {
        hook.Cancel();
    }

    internal void StickPoint(PlayerStay currentStickPosition)
    {
        if (IsHooked) return;

        visualControl.SetIdle();

        this.currentStickPosition = currentStickPosition;
        stickedPoint = currentStickPosition.StickPosition;
        currentStickPosition.StartUse();

        canAirDash = true;

        isHanging = true;
        visualControl.SetHanging(true, currentDir);
        gravity = 0;
    }
   
    public void SetUseHook(bool canUseHook) => this.canUseHook = canUseHook;

    public void SetUseSquash(bool canUseSquash) => this.canUseSquash = canUseSquash;

    internal void SetUseAirDash(bool canUseAirDash) => this.canUseAirDash = canUseAirDash;
}

public interface IHide
{
    bool IsHidden { get; }
}