﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (FieldOfView))]
public class FieldOfViewEditor : Editor {

	void OnSceneGUI() {
		FieldOfView fow = (FieldOfView)target;
		Handles.color = Color.white;
		Handles.DrawWireArc (fow.transform.position, Vector3.forward, Vector3.right, 360, fow.viewRadius);
		Vector3 viewAngleA = (-fow.viewAngle / 2).AsVector();
		Vector3 viewAngleB = (fow.viewAngle / 2).AsVector();

         Gizmos.color = Color.blue;
         Handles.DrawLine (fow.transform.position, fow.transform.position + viewAngleA * fow.viewRadius);
         
        Gizmos.color = Color.yellow;
		Handles.DrawLine (fow.transform.position, fow.transform.position + viewAngleB * fow.viewRadius);
        
		Handles.color = Color.red;
		foreach (Transform visibleTarget in fow.visibleTargets)
			Handles.DrawLine (fow.transform.position, visibleTarget.position);
	}

}
