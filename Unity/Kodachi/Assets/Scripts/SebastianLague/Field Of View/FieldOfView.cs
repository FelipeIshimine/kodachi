﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;

[RequireComponent(typeof(MeshFilter))]
public class FieldOfView : MonoBehaviour
{
    public Action<Transform> OnTargetFound;
    public Action<Transform> OnTargetStay;
    public Action<Transform> OnTargetLost;

    public float viewRadius;
	[Range(0,360)]
	public float viewAngle;

	public LayerMask targetMask;
	public LayerMask obstacleMask;

	[HideInInspector]
	public List<Transform> oldTargets = new List<Transform>();
	public List<Transform> visibleTargets = new List<Transform>();

    public float meshResolution = .5f;
	public int edgeResolveIterations;
	public float edgeDstThreshold;

	public float maskCutawayDst = .1f;

	public MeshFilter viewMeshFilter;
	Mesh viewMesh;

    public Color areaColor;
    private Color clearColor;

    [Range(0,1)]
    public float drawPercentage = 1;

    public int LookDir = 1;

    public bool CanSeeHiddenUnits { get; set; } = false;

    public bool isOn = false;

    private void OnBecameInvisible()
    {
        isOn = false;
    }

    private void OnBecameVisible()
    {
        isOn = true;
    }

    [Button]
    public void Flip()
    {
        LookDir *= -1;
    }

    void Start()
    {
        clearColor = areaColor;
        clearColor.a = 0;
        viewMeshFilter = GetComponent<MeshFilter>();
		viewMesh = new Mesh ();
		viewMesh.name = "View Mesh";
		viewMeshFilter.mesh = viewMesh;
		StartCoroutine ("FindTargetsWithDelay", .2f);
	}

	IEnumerator FindTargetsWithDelay(float delay)
    {
		while (true)
        {
			yield return new WaitForSeconds (delay);
			FindVisibleTargets ();
		}
	}

	void LateUpdate()
    {
        if (!isOn) return;
		DrawFieldOfView ();
	}
    Collider2D[] targetsInViewRadius = new Collider2D[20];

    void FindVisibleTargets()
    {
        ClearTargets();
        int logicalSize = Physics2D.OverlapCircleNonAlloc(transform.position, viewRadius, targetsInViewRadius, targetMask);

        for (int i = 0; i < logicalSize; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector2 dirToTarget = (target.position - transform.position).normalized;
            if (Vector2.Angle(transform.right * LookDir, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector2.Distance(transform.position, target.position);
                if (!Physics2D.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    if (CanSeeHiddenUnits)
                        AddTarget(target);
                    else
                    {
                        IHide hide = target.GetComponent<IHide>();
                        if (hide == null || !hide.IsHidden)
                            AddTarget(target);
                    }
                }
            }
        }
        BroadcastLostTargets();
    }

    private void BroadcastLostTargets()
    {
        for (int i = 0; i < oldTargets.Count; i++)
            if (!visibleTargets.Contains(oldTargets[i]))
                OnTargetLost?.Invoke(oldTargets[i]);
    }

    private void ClearTargets()
    {
        oldTargets = visibleTargets;
        visibleTargets = new List<Transform>();
    }

    private void AddTarget(Transform target)
    {
        if (oldTargets.Contains(target))
            OnTargetStay?.Invoke(target);
        else
            OnTargetFound?.Invoke(target);
        visibleTargets.Add(target);
    }

    void DrawFieldOfView()
    {
        float viewAngle = this.viewAngle * drawPercentage;
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
		float stepAngleSize = viewAngle / stepCount;
		List<Vector2> viewPoints = new List<Vector2> ();
		ViewCastInfo oldViewCast = new ViewCastInfo ();
        float dirAngleFix = ((LookDir == -1) ? 180 : 0);
        for (int i = 0; i <= stepCount; i++)
        {
			float angle = transform.eulerAngles.z - viewAngle / 2 + stepAngleSize * i + dirAngleFix;
			ViewCastInfo newViewCast = ViewCast (angle);

			if (i > 0)
            {
				bool edgeDstThresholdExceeded = Mathf.Abs (oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
				if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded)) {
					EdgeInfo edge = FindEdge (oldViewCast, newViewCast);
					if (edge.pointA != Vector2.zero) {
						viewPoints.Add (edge.pointA);
					}
					if (edge.pointB != Vector2.zero) {
						viewPoints.Add (edge.pointB);
					}
				}
			}

			viewPoints.Add (newViewCast.point);
			oldViewCast = newViewCast;
		}

		int vertexCount = viewPoints.Count + 1;
		Vector3[] vertices = new Vector3[vertexCount];
		int[] triangles = new int[(vertexCount-2) * 3];

		vertices [0] = Vector2.zero;
		for (int i = 0; i < vertexCount - 1; i++)
        {
			vertices [i + 1] = Vector2.right * maskCutawayDst + (Vector2)transform.InverseTransformPoint(viewPoints [i]);

			if (i < vertexCount - 2)
            {
				triangles [i * 3] = 0;
				triangles [i * 3 + 1] = i + 1;
				triangles [i * 3 + 2] = i + 2;
			}
		}

		viewMesh.Clear ();
		viewMesh.vertices = vertices;
		viewMesh.triangles = triangles;

        Color[] colors = new Color[vertices.Length];

        colors[0] = areaColor;
        for (int i = 1; i < vertices.Length; i++)
            colors[i] = Color.Lerp(areaColor, clearColor, (vertices[i].x * LookDir) / viewRadius);

        viewMesh.colors = colors;
        viewMesh.RecalculateNormals ();
	}

    internal void ClearAll()
    {
        oldTargets.Clear();
        visibleTargets.Clear();
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
		float minAngle = minViewCast.angle;
		float maxAngle = maxViewCast.angle;
		Vector3 minPoint = Vector3.zero;
		Vector3 maxPoint = Vector3.zero;

		for (int i = 0; i < edgeResolveIterations; i++) {
			float angle = (minAngle + maxAngle) / 2;
			ViewCastInfo newViewCast = ViewCast (angle);

			bool edgeDstThresholdExceeded = Mathf.Abs (minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
			if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded) {
				minAngle = angle;
				minPoint = newViewCast.point;
			} else
            {
				maxAngle = angle;
				maxPoint = newViewCast.point;
			}
		}

		return new EdgeInfo (minPoint, maxPoint);
	}


	ViewCastInfo ViewCast(float globalAngle)
    {
        Vector2 dir = globalAngle.AsVector();
		RaycastHit2D hit;
        hit = Physics2D.Raycast(transform.position, dir, viewRadius, obstacleMask);

        if (hit)
        {
			return new ViewCastInfo (true, hit.point, hit.distance, globalAngle);
		} else {
			return new ViewCastInfo (false, transform.position + (Vector3)dir * viewRadius, viewRadius, globalAngle);
		}
	}


    public struct ViewCastInfo {
		public bool hit;
		public Vector2 point;
		public float dst;
		public float angle;

		public ViewCastInfo(bool _hit, Vector2 _point, float _dst, float _angle) {
			hit = _hit;
			point = _point;
			dst = _dst;
			angle = _angle;
		}
	}

	public struct EdgeInfo {
		public Vector2 pointA;
		public Vector2 pointB;

		public EdgeInfo(Vector2 _pointA, Vector2 _pointB) {
			pointA = _pointA;
			pointB = _pointB;
		}
	}

    private void OnDisable()
    {
        viewMesh.Clear();
    }
}