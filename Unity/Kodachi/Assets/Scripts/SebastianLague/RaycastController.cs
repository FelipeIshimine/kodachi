﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class RaycastController : MonoBehaviour {

	public LayerMask collisionMask;

    public  float skinWidth = .015f;
    public float dstBetweenRays = .25f;

    public Vector2Int rayCount;
	public Vector2 raySpacing;

	public BoxCollider2D col;
	public RaycastOrigins raycastOrigins;

    public bool dynamicRayspacing = true;

	public virtual void Awake()
    {
        if(col == null)		col = GetComponent<BoxCollider2D> ();
	}

	public virtual void Start() {
		CalculateRaySpacing ();
	}

    [Button]
	public void UpdateRaycastOrigins()
    {
        if (dynamicRayspacing)
            CalculateRaySpacing();

        Bounds bounds = col.bounds;
		bounds.Expand (skinWidth * -2);
		
		raycastOrigins.bottomLeft = new Vector2 (bounds.min.x, bounds.min.y);
		raycastOrigins.bottomRight = new Vector2 (bounds.max.x, bounds.min.y);
		raycastOrigins.topLeft = new Vector2 (bounds.min.x, bounds.max.y);
		raycastOrigins.topRight = new Vector2 (bounds.max.x, bounds.max.y);
	}
	
	public void CalculateRaySpacing() {
		Bounds bounds = col.bounds;
		bounds.Expand (skinWidth * -2);

		float boundsWidth = bounds.size.x;
		float boundsHeight = bounds.size.y;

        rayCount.x = Mathf.RoundToInt (boundsHeight / dstBetweenRays);
        rayCount.y = Mathf.RoundToInt (boundsWidth / dstBetweenRays);

        raySpacing.x = bounds.size.y / (rayCount.x - 1);
        raySpacing.y = bounds.size.x / (rayCount.y - 1);
	}

	[System.Serializable]
	public struct RaycastOrigins {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}
}
