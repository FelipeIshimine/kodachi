﻿using UnityEngine;
using System.Collections;
using System;
using Sirenix.OdinInspector;
using FMODUnity;

public class Hook : MonoBehaviour
{
    [TabGroup("Dependencies")] [SerializeField] private GameObject sparkParticlesPrefab;
    [TabGroup("Dependencies")] [SerializeField] private Transform[] aimMark = null;
    [TabGroup("Dependencies")] [SerializeField] private Transform[] aimLockMark = null;
    [TabGroup("Dependencies")] [SerializeField] private Transform kunai = null;
    [TabGroup("Dependencies")] public LineRenderer lineRenderer;

    [TabGroup("Visual")] [SerializeField] private SpriteRenderer kunaiRender = null;
    [TabGroup("Visual")] [SerializeField] private Sprite kunarFull = null;
    [TabGroup("Visual")] [SerializeField] private Sprite kunarNailed = null;
    [TabGroup("Visual")] [SerializeField] public float kunaiSize = .5f;

    private Health[] currentLockedTarget = new Health[2] { null, null };
    public bool UseAimAssist { get; set; }

    private float[] aimLockTimer = new float[2];

    [ShowInInspector] public bool IsHooked { get; private set; }

    [TabGroup("Animation")] public AnimationCurve shootHookLineCurve;
    [TabGroup("Animation")] public float shootHookLineDisplacement = 1f;
    [TabGroup("Animation")] public float extraFlatDisplacement = .5f;

    [TabGroup("Animation")] public AnimationCurve retractSpeed;
    [TabGroup("Animation")] public AnimationCurve retractHookAnimation;
    [TabGroup("Animation")] public AnimationCurve retractHookAnimationX;
    [TabGroup("Animation")] public AnimationCurve retractHookLooseness;
    [TabGroup("Animation")] public float endPointExtraFlatDisplacement = 2;
    [TabGroup("Animation")] public float rectractHookDisplacement = 2;
    [TabGroup("Animation")] public float skin = .5f;
    [TabGroup("Animation")] public bool useSimpleHookAnimation = false;
    [TabGroup("Animation")] public int hookResolution = 5;

    public Action<int> OnHooked;
    public Action<int> OnShoot;
    public Action<int, RaycastHit2D> OnHookHit;
    public Action OnHookMiss;

    [TabGroup("Gameplay")] public AnimationCurve aimLockSizeCurve;
    [TabGroup("Gameplay")] public float aimLockMarkSize = 2;
    [TabGroup("Gameplay")] public float aimLockDuration = .2f;
    [TabGroup("Gameplay")] public float hookTravelSpeed = 5;
    private float hookT = 0;
    [TabGroup("Gameplay")] public float normalSmooth = .5f;
    [TabGroup("Gameplay")] public float agresiveSmooth;
    [TabGroup("Gameplay")] public float hookedTargetPoolForce = 5;
    [TabGroup("Gameplay")] public float unhookDistance = .7f;
    [TabGroup("Gameplay")] public float goToPointSpeed = 20;
    [TabGroup("Gameplay")] public float hookDuration;
    [TabGroup("Gameplay")] public float hookAngle = 45;
    [TabGroup("Gameplay")] public float hookMaxDistance = 10;
    [TabGroup("Gameplay")] public float hookRetractDuration = 2f;
    [TabGroup("Gameplay")] public LayerMask hookLayer;
    private Vector2 hookVel;

    [TabGroup("Gameplay")] public float hookHitForce = 10;
    [TabGroup("Gameplay")] public float shootCoolDown = 1;
    public bool CanHook => cd <= 0;
    private float cd = 0;


    public bool IsHookedTargetMovable { get; private set; }
    private Health hookedTarget;
    public Health HookedTarget
    {
        get => hookedTarget;
        private set
        {
            if (value == null && hookedTarget != null)
            {
                hookedTarget.OnDead -= OnHookedTargetDeath;
            }
            hookedTarget = value;
        }
    }

    private bool showMarks;
    public bool HasHookedTarget => HookedTarget != null;

    private Vector2 hookedPosition;
    public Vector2 HookedPosition
    {
        get => ((HookedTarget == null) ? hookedPosition : (Vector2)HookedTarget.transform.TransformPoint(hookedPosition));
        set => hookedPosition = value;
    }
    IEnumerator hookRutine;

    private bool isHookTraveling = false;

    internal bool isHookTravelling => isHookTraveling;

    [TabGroup("FMod")] public StudioEventEmitter shootSound;
    [TabGroup("FMod")] public StudioEventEmitter retractSound;


    public void Initialize(bool showMarkers, bool useAimAssist)
    {
        showMarks = showMarkers;
        aimMark[0].gameObject.SetActive(showMarkers);
        aimMark[1].gameObject.SetActive(showMarkers);

        aimLockMark[0].gameObject.SetActive(false);
        aimLockMark[1].gameObject.SetActive(false);
        UseAimAssist = useAimAssist;
    }

    private void SetCD()
    {
        cd = shootCoolDown;
    }

    private void FixedUpdate()
    {
        UpdateAimLock();
    }

    private void UpdateAimLock()
    {
        if (UseAimAssist)
        {
            for (int i = 0; i < aimLockTimer.Length; i++)
            {
                if (aimLockTimer[i] <= 0) continue;
                aimLockMark[i].transform.localScale = Vector3.one *  aimLockMarkSize;
                aimLockTimer[i] -= Time.fixedDeltaTime;
                if (aimLockTimer[i] <= 0)
                    DropAimLock(i);
            }
        }
    }

    private void DropAimLock(int i)
    {
        currentLockedTarget[i] = null;
        aimLockMark[i].gameObject.SetActive(false);
    }

    public void Update()
    {
        cd -= shootCoolDown;
        if (IsHooked)
        {
            hookT += Time.deltaTime;
            lineRenderer.SetPositions(new Vector3[2] { transform.position, HookedPosition });
            if (hookT >= hookDuration || Vector2.Distance(transform.position, HookedPosition) < unhookDistance)
            {
                Unhook();
            }
        }
        else if(!isHookTraveling)
            ShowMarks();
    }

    public bool Shoot(int dir)
    {
        if (isHookTraveling || !CanHook) return false;

        //Cambiamos el sprite del kunai
        kunaiRender.sprite = kunarFull;

        //Si estamos en estado de HOOK nos soltamos
        HookedTarget = null;
        if (IsHooked)
        {
            Unhook();
            return false;
        }

        //Sound 
        shootSound.Play();
        retractSound.Stop();

        HideMarks();
        SetCD();

        if (hookRutine != null) StopCoroutine(hookRutine);
        hookRutine = ShootHookCorountine(dir);
        StartCoroutine(hookRutine);
        return true;
    }

    public void HideMarks()
    {
        aimMark[0].gameObject.SetActive(false);
        aimMark[1].gameObject.SetActive(false);

        for (int i = 0; i < aimLockMark.Length; i++)
            aimLockMark[i].gameObject.SetActive(false);
    }

    private IEnumerator ShootHookCorountine(int horizontalDir)
    {
        kunai.gameObject.SetActive(true);
        float traveledDistance = 0;
        Vector2 startPo = transform.position;
        Vector2 angleDir = (horizontalDir == 1) ? this.hookAngle.AsVector() : (180 - this.hookAngle).AsVector();

        if (UseAimAssist)
        {
            int index = ((horizontalDir == 1) ? 1 : 0);
            if(currentLockedTarget[index] != null)
                angleDir = transform.position.GetDirection(currentLockedTarget[index].transform.position);
        }

        Vector2 lastPoint, point;
        lastPoint = point = (skin * (Vector3)angleDir) + transform.position;
        lineRenderer.positionCount = 2;
        float deltaDistance;
        RaycastHit2D hit;
        isHookTraveling = true;
        float extraYDisplacement = UnityEngine.Random.Range(-extraFlatDisplacement, extraFlatDisplacement);
        int normalSign = (UnityEngine.Random.Range(0, 2) == 0) ? 1 : -1;
        float angle = angleDir.AsAngle();
        int normalSignX = (UnityEngine.Random.Range(0, 2) == 0) ? 1 : -1;
        Vector3 ropeEndPoint;
        do
        {
            lastPoint = point;
            deltaDistance = Time.deltaTime * hookTravelSpeed;
            traveledDistance += deltaDistance;
            point = startPo + traveledDistance * angleDir;
            hit = Physics2D.Raycast(lastPoint, angleDir, deltaDistance, hookLayer);
            ropeEndPoint = point - (angleDir * kunaiSize);
            if (useSimpleHookAnimation)
                lineRenderer.SetPositions(new Vector3[2] { transform.position, point });
            else
                HookAnimationStep(normalSign, normalSignX, angleDir, ropeEndPoint, traveledDistance, extraYDisplacement);

            kunai.transform.position = ropeEndPoint;
            kunai.transform.rotation = Quaternion.Euler(0, 0, angle);

            hookedPosition = ropeEndPoint;
            yield return null;
        } while (!hit && traveledDistance < hookMaxDistance && isHookTraveling);

        isHookTraveling = false;
        if (hit)
        {
            if (hit.collider.GetComponent<Unhookable>() != null)
                Cancel(true);
            else
                Hit(angleDir, horizontalDir, hit);
        }
        else
            yield return RetractRutine(point);

    }

    IEnumerator RetractRutine(Vector2 point)
    {
        //Sound
        retractSound.Play();

        float traveledDistance = Mathf.Abs(Vector2.Distance(transform.position, point));
        Vector2 angleDir = transform.position.GetDirection(point);
        float extraEndDisplacement = UnityEngine.Random.Range(-endPointExtraFlatDisplacement, endPointExtraFlatDisplacement);
        int normalSign = (UnityEngine.Random.Range(0, 2) == 0) ? 1 : -1;
        int normalSignX = (UnityEngine.Random.Range(0, 2) == 0) ? 1 : -1;
        Miss();
        float t = 0;
        float multiplicator = traveledDistance / hookMaxDistance;
        do
        {
            t += Time.deltaTime / hookRetractDuration;
            if (useSimpleHookAnimation)
                lineRenderer.SetPositions(new Vector3[2] { transform.position, Vector3.Lerp(point, transform.position, t) });
            else
                RectractAnimationStep(normalSign * multiplicator, normalSignX * multiplicator, angleDir, Vector2.Lerp(point, transform.position, t), traveledDistance, extraEndDisplacement, t, kunai);
            yield return null;
        } while (t < 1);

        //Audio
        retractSound.Stop();

        kunai.gameObject.SetActive(false);
        lineRenderer.positionCount = 0;
    }

    private void RectractAnimationStep(float normalDir, float normalDirX, Vector2 direction, Vector2 endPoint, float traveledDistance, float endPointDisplacement, float t, Transform kunai)
    {
        int positionCount = Mathf.Abs(Mathf.CeilToInt(traveledDistance * hookResolution));
        lineRenderer.positionCount = positionCount;

        Vector3 startPoint = transform.position;
        Vector3[] positions = new Vector3[positionCount];
        Vector2 normal = new Vector2(direction.y, direction.x * normalDir);

        Vector2 currentEndPoint = endPoint;
        float tReverse = 1 - (t * retractSpeed.Evaluate(t));

        float loosness = retractHookLooseness.Evaluate(t);
        for (int i = 0; i < positions.Length; i++)
        {
            float tOfLenght = ((float)i / positionCount);
            float tOfRectract = tOfLenght * tReverse;
            positions[i] = Vector2.Lerp(startPoint, currentEndPoint, tOfLenght)
                +
                    (retractHookAnimation.Evaluate(tOfRectract) * rectractHookDisplacement * loosness) * normal +
                    (retractHookAnimationX.Evaluate(tOfRectract) * rectractHookDisplacement * loosness * normalDirX) * direction;
        }
        kunai.transform.position = positions[positions.Length - 1];
        if(positions.Length >=2)        kunai.transform.rotation = Quaternion.Euler(0, 0, positions[positions.Length - 2].GetAngle(positions[positions.Length-1]));
        lineRenderer.SetPositions(positions);
    }

    private void HookAnimationStep(int normalDir, int normalDirX, Vector2 direction, Vector2 endPoint, float traveledDistance, float extraYDisplacement = 0)
    {
        int positionCount = Mathf.CeilToInt(traveledDistance * hookResolution);
        lineRenderer.positionCount = positionCount;

        Vector3 startPoint = transform.position;
        Vector3[] positions = new Vector3[positionCount];
        Vector3 normal = new Vector2(direction.y, -direction.x);
        float tOverDistance = traveledDistance / hookMaxDistance;
        float tOverDIstanceInvert = 1 - tOverDistance;

        for (int i = 0; i < positions.Length; i++)
        {
            float tOfLenght = ((float)i / positionCount);
            /*  positions[i] = Vector3.Lerp(startPoint, endPoint, tOfLenght) + ((shootHookLineCurve.Evaluate(tOfLenght) * shootHookLineDisplacement + extraYDisplacement) * tOverDIstanceInvert)  * normal;*/
            positions[i] = Vector2.Lerp(startPoint, endPoint, tOfLenght)
               +
                   (retractHookAnimation.Evaluate(tOfLenght) * rectractHookDisplacement * tOverDIstanceInvert) * (Vector2)normal +
                   (retractHookAnimationX.Evaluate(tOfLenght) * rectractHookDisplacement  * normalDirX * tOverDIstanceInvert) * direction;

        }
        lineRenderer.SetPositions(positions);
    }

    private void Miss()
    {
        lineRenderer.positionCount =0;

        aimMark[0].gameObject.SetActive(true);
        aimMark[1].gameObject.SetActive(true);
        OnHookMiss?.Invoke();
    }

    private void Hit(Vector2 angleDir, int dir, RaycastHit2D hit)
    {
        //Audio
        retractSound.Play();

        hookT = 0;
        IsHooked = true;
        HideMarks();

        HookedTarget = hit.collider.GetComponentInParent<Health>();

        lineRenderer.positionCount = 2;
        lineRenderer.SetPositions(new Vector3[2] { transform.position, hit.point });

        kunaiRender.sprite = kunarNailed;
        kunai.transform.SetParent(hit.collider.transform);
        kunai.transform.position = hit.point;
        kunai.transform.rotation = Quaternion.Euler(0, 0, angleDir.AsAngle());

        if (HookedTarget != null)
        {
            HookedPosition = HookedTarget.transform.InverseTransformPoint(hit.point);
            HookedTarget.OnPreDead += OnHookedTargetDeath;
            IsHookedTargetMovable = !HookedTarget.GetComponent<Rigidbody2D>().isKinematic;
        }
        else
        {
            HookedPosition = hit.point;
            IsHookedTargetMovable = false;
        }
        Rigidbody2D rb = hit.collider.attachedRigidbody;

        if (rb != null && !rb.isKinematic)
            rb.AddForceAtPosition(angleDir * hookHitForce, hit.point, ForceMode2D.Impulse);

        BaseEnemyUnitMonoBehaviour baseEnemy = hit.collider.GetComponent<BaseEnemyUnitMonoBehaviour>();

        if (baseEnemy != null)
            baseEnemy.Hook();

        OnHookHit?.Invoke(dir, hit);
    }

    private void OnHookedTargetDeath(Health obj)
    {
        Debug.Log("HookedTarget Death");
        kunai.transform.SetParent(transform);
        Unhook();
    }

    private void OnDestroy()
    {
        if (HookedTarget != null)
            HookedTarget.OnDead -= OnHookedTargetDeath;
    }

    private void ShowMarks()
    {
        if (!showMarks) return;

        RaycastHit2D hit;

        if (UseAimAssist && currentLockedTarget[0] != null)
        {
            aimLockMark[0].transform.position = currentLockedTarget[0].transform.position;
            aimMark[0].gameObject.SetActive(false);
        }
        else
        {
            hit = Physics2D.Raycast(transform.position, (180 - this.hookAngle).AsVector(), hookMaxDistance, hookLayer);
            SetMark(hit, 0);
        }

        if (UseAimAssist && currentLockedTarget[1] != null)
        {
            aimLockMark[1].transform.position = currentLockedTarget[1].transform.position;
            aimMark[0].gameObject.SetActive(false);
        }
        else
        {
            hit = Physics2D.Raycast(transform.position, this.hookAngle.AsVector(), hookMaxDistance, hookLayer);
            SetMark(hit, 1);
        }
    }

    internal void SetEnable(bool v)
    {
        if (v)
            HideHook();
    }

    private void SetMark(RaycastHit2D hit, int index = -1)
    {
        if (hit)
        {
            if (UseAimAssist)
            {
                Health h = hit.collider.GetComponentInParent<Health>();
                if (h != null && h.GetComponent<Unhookable>() == null)
                {
                    aimLockMark[index].gameObject.SetActive(true);
                    aimLockMark[index].transform.position = h.transform.position;
                    currentLockedTarget[index] = h;
                    aimLockTimer[index] = aimLockDuration;
                }
                else
                    aimLockMark[index].gameObject.SetActive(false);
            }
            aimMark[index].transform.position = hit.point;
            aimMark[index].gameObject.SetActive(true);
        }
        else
            aimMark[index].gameObject.SetActive(false);
    }

    internal void Cancel(bool force = false)
    {
        if (force || isHookTraveling || IsHooked)
        {
            GameObject go = Instantiate(sparkParticlesPrefab, null);
            go.transform.position = hookedPosition;
            Destroy(go, 3);
            Unhook(true);
        }
        else
            Debug.LogWarning("HookCancelado sin estar activado");
    }

    internal void Unhook(bool forceUnhookAnimation = false)
    {
        //Audio
        retractSound.Stop();

        if (hookRutine != null) StopCoroutine(hookRutine);
        
        if(IsHooked || forceUnhookAnimation)
            UnhookAnimation();
        else
            lineRenderer.positionCount = 0;

        HookedTarget = null;
        IsHooked = false;
        isHookTraveling = false;
    }

    private void UnhookAnimation()
    {
        if (!gameObject.activeSelf) return;

        kunai.gameObject.SetActive(true);
        if (hookRutine != null) StopCoroutine(hookRutine);
        hookRutine = RetractRutine(HookedPosition);
        StartCoroutine(hookRutine);
    }

    internal void HideHook()
    {
        kunai.gameObject.SetActive(false);
        lineRenderer.positionCount = 0;
    }
}

