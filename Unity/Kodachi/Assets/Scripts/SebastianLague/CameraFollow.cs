﻿using UnityEngine;
using System.Collections;
using System;

public class CameraFollow : MonoBehaviour
{
	private Controller2D target;
	public float verticalOffset;
	public float verticalSmoothTime;
	public Vector2 focusAreaSize;

	FocusArea focusArea;

	float smoothVelocityY;
    public bool freezeX = true;

	public void Initialize(Controller2D target)
    {
        transform.position = target.transform.position;
        this.target = target;
        focusArea = new FocusArea (target.col.bounds, focusAreaSize);
        focusArea.center = target.transform.position;
    }

	void LateUpdate()
    {
        if (target == null) return;

		focusArea.Update (target.col.bounds);

		Vector2 focusPosition = focusArea.center + Vector2.up * verticalOffset;

		focusPosition.y = Mathf.SmoothDamp (transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime, float.MaxValue, Time.unscaledDeltaTime);

        if (float.IsNaN(focusPosition.y))
            return;

		transform.position = (Vector3)focusPosition + Vector3.forward * -10;

        if (freezeX)
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
	}

    internal void SetPosition(float height)
    {
        focusArea.center = new Vector2(0, height + verticalOffset);
        transform.position = new Vector3(0, height + verticalOffset, transform.position.z);
    }

    void OnDrawGizmos() {
		Gizmos.color = new Color (1, 0, 0, .5f);
		Gizmos.DrawCube (focusArea.center, focusAreaSize);
	}

	struct FocusArea {
		public Vector2 center;
		public Vector2 velocity;
		float left,right;
		float top,bottom;


		public FocusArea(Bounds targetBounds, Vector2 size) {
			left = targetBounds.center.x - size.x/2;
			right = targetBounds.center.x + size.x/2;
			bottom = targetBounds.min.y;
			top = targetBounds.min.y + size.y;

			velocity = Vector2.zero;
			center = new Vector2((left+right)/2,(top +bottom)/2);
		}

		public void Update(Bounds targetBounds) {
			float shiftX = 0;
			if (targetBounds.min.x < left) {
				shiftX = targetBounds.min.x - left;
			} else if (targetBounds.max.x > right) {
				shiftX = targetBounds.max.x - right;
			}
			left += shiftX;
			right += shiftX;

			float shiftY = 0;
			if (targetBounds.min.y < bottom) {
				shiftY = targetBounds.min.y - bottom;
			} else if (targetBounds.max.y > top) {
				shiftY = targetBounds.max.y - top;
			}
			top += shiftY;
			bottom += shiftY;
			center = new Vector2((left+right)/2,(top +bottom)/2);
			velocity = new Vector2 (shiftX, shiftY);
		}
	}

}
