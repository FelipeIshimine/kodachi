﻿using System;
using UnityEngine;

public class TrailController : MonoBehaviour
{
    public GameObject trailPrefab;
    public GameObject current;

    internal void Drop()
    {
        if (current != null)
        {
            current.transform.SetParent(null);
            Destroy(current, 3);
            current = null;
        }
    }

    internal void New()
    {
        if (current == null)
        {
            current = MonoBehaviour.Instantiate(trailPrefab, transform);
        }
    }
}