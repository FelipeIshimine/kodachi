﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using System;

[RequireComponent (typeof (Player))]
public class PlayerInput : MonoBehaviour {

    public Action OnAnyInput;
    public Action<int> OnJumpInput;
    public Action<int> OnHookInput;

    Player player;

	void Start ()
    {
		player = GetComponentInParent<Player> ();
	}

	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            Hook(-1);
        else if (Input.GetKeyDown(KeyCode.E))
            Hook(1);

        if (Input.GetKeyDown(KeyCode.A))
            Jump(-1);
        else if (Input.GetKeyDown(KeyCode.D))
            Jump(1);
    }

    public void Hook(int dir)
    {
        OnAnyInput?.Invoke();
        OnHookInput?.Invoke(dir);
        player.ShootHook(dir);
    }

    public void Jump(int dir)
    {
        OnAnyInput?.Invoke();
        OnJumpInput?.Invoke(dir);
        player.Jump(dir);
    }
  
}
