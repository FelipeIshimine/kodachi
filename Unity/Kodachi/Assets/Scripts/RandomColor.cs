﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    private void OnValidate()
    {
        if(!spriteRenderer)
            spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Change()
    {
        spriteRenderer.color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
    }

}
