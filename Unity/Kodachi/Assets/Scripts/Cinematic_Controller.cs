﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class Cinematic_Controller : MonoBehaviour
{
    public Animator anim;
    public Action OnEndingReached;

    [Button]
    public void Play()
    {
        anim.SetTrigger("Play");
    }

    [Button]
    public void Stop()
    {
        anim.SetTrigger("Stop");
    }

    public void EndAnimation()
    {
        OnEndingReached?.Invoke();
    }
}
