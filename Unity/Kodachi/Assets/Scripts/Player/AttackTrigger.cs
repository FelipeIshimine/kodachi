﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{
    public Action<Collider2D, bool> OnAttackDone;
    public LayerMask layerMask;
    public GameObject slashVfxPrefab;
    public  IUseAttackTrigger Owner { get; private set; }
    public Collider2D Collider { get; private set; }

    private void Awake()
    {
        Owner = GetComponentInParent<IUseAttackTrigger>();
        OnAttackDone += Owner.OnAttackDone;
        Collider = GetComponent<Collider2D>();
    }

    private void OnDestroy()
    {
        OnAttackDone -= Owner.OnAttackDone; 
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!enabled) return;
        if (layerMask.Contains(other.gameObject.layer))
        {
            Debug.Log("other.gameObject.name " + other.gameObject.name);
            Health health = other.gameObject.GetComponentInParent<Health>();
            if (health != null)
            {
                bool isLethal = health.Damage(1);
                OnAttackDone?.Invoke(other, isLethal);
                if(slashVfxPrefab != null)
                {
                    GameObject go = Instantiate(slashVfxPrefab, null);
                    go.transform.position = other.transform.position;
                    go.transform.rotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360));
                    Destroy(go, 5);
                }
                if(!isLethal && Owner is Player player)
                    player.AttackKnockbackFrom(other.transform.position, false);

            }
        }
    }
}

public interface IUseAttackTrigger
{
    void OnAttackDone(Collider2D col, bool isLethal);
}