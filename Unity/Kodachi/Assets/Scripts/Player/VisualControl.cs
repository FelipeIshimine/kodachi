﻿using System;
using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;

public class VisualControl : MonoBehaviour
{
    public LayerMask layerMask;
    public Controller2D Controller2D;
    public Animator animator;
    public Transform visualRoot;
    public Transform scarfRoot;
    public SpriteRenderer render;
    public Player player;

    public float slopeVisualFixMaxDistance = .2f;

    public float angleSmooth = .05f;
    private float angleVel;

    public float positionSmooth = .05f;
    private Vector2 targetPosition;
    private Vector2 velPosition;
    float targetAngle;

    public Scarf[] scarfs;

    public float fadeInDuration = .3f;
    public AnimationCurve fadeCurve = AnimationCurve.EaseInOut(0,0,1,1);
    private IEnumerator fadeRutine;

    private bool hasWeapon = false;
    internal bool HasWeapon
    {
        get => hasWeapon;
        set
        {
            hasWeapon = value;
            animator.SetBool("HasWeapon", value);
        }
    }

    private void Start()
    {
        Controller2D.OnCollision2D += OnCollision2D;
    }

    [Button]
    private void LateUpdate()
    {
        for (int i = 0; i < scarfs.Length; i++)
            scarfs[i].Follow(scarfRoot.position);

        animator.SetFloat("YVelocity", player.Velocity.y);
        animator.SetBool("InAir", !Controller2D.collisions.below && !Controller2D.collisions.right && !Controller2D.collisions.left);
    }
    private void OnDestroy()
    {
        Controller2D.OnCollision2D -= OnCollision2D;
    }

    private void OnCollision2D(Controller2D.CollisionInfo collision, Vector2 moveAmmount)
    {
        if (player.InSquash()) return;
        if(!collision.below && (collision.left || collision.right) && !collision.descendingSlope && !collision.climbingSlope) //Wall
            SetWallRunning(collision);
        else if(collision.below)
            SetRunning(collision);

        visualRoot.rotation = Quaternion.Euler(0, 0, Mathf.SmoothDampAngle(visualRoot.eulerAngles.z, targetAngle, ref angleVel, angleSmooth));
        visualRoot.rotation = Quaternion.Euler(0, 0, targetAngle); 
    }

    private void SetRunning(Controller2D.CollisionInfo collision)
    {
        animator.SetBool("Running", Mathf.Abs(player.Velocity.x) >0 && player.enabled);

        if(collision.faceDir == 1)
            visualRoot.localScale = new Vector3(1, 1, 1);
        else
            visualRoot.localScale = new Vector3(1, -1, 1);

        targetAngle = (collision.faceDir == 1) ? 0 : 180;
    }

    private void SetWallRunning(Controller2D.CollisionInfo collision)
    {
        animator.SetBool("Running", true);
        visualRoot.localScale = new Vector3(1, (collision.right) ? 1 : -1, 1);
        visualRoot.rotation = Quaternion.Euler(0, 0, 90);
        targetAngle = 90;
    }

    public void OnFloor(int dir)
    {
        /*animator.SetBool("Running",true);
        visualRoot.localScale = new Vector3(dir, 1, 1);
        visualRoot.rotation = Quaternion.Euler(0, 0, 0);*/
    }

    internal void ApplyScarfPositionOffset(Vector3 offset)
    {
        foreach (Scarf item in scarfs)
            item.ApplyPositionOffset(offset);
    }

    public void OnWall(int dir)
    {
        /* animator.SetBool("Running",true);
         if (dir == 1)
         {
             visualRoot.localScale = new Vector3(1, 1, 1);
             visualRoot.rotation = Quaternion.Euler(0, 0, 90);
         }
         else
         {
             visualRoot.localScale = new Vector3(1, -1, 1);
             visualRoot.rotation = Quaternion.Euler(0, 0, 90);
         }*/
    }


    public void Jump(int dir)
    {
        visualRoot.localScale = new Vector3(dir, 1, 1);
        visualRoot.rotation = Quaternion.Euler(0, 0, 0);
        targetAngle = 0;
        animator.SetTrigger("Jump");
        animator.SetBool("Running",false);
    }
   
    internal void Squash(int wallDirection)
    {
        animator.SetBool("Running", false);
        animator.SetTrigger("Squash");
        RotateToWall(wallDirection);
    }

    private void RotateToWall(int dir)
    {
        if (dir == 1)
        {
            visualRoot.localScale = new Vector3(1, 1, 1);
            visualRoot.rotation = Quaternion.Euler(0, 0, 90);
        }
        else
        {
            visualRoot.localScale = new Vector3(1, -1, 1);
            visualRoot.rotation = Quaternion.Euler(0, 0, 90);
        }
    }

    internal void Dash(int dir)
    {
        visualRoot.localScale = new Vector3(dir, 1, 1);
        visualRoot.rotation = Quaternion.Euler(0, 0, 0);
        targetAngle = 0;
        animator.SetBool("Running",false);
        animator.SetTrigger("Dash");
    }

    internal void HideScarft()
    {
        foreach (Scarf item in scarfs)
            item.gameObject.SetActive(false);
    }

    internal void ShowScarf()
    {
        foreach (Scarf item in scarfs)
            item.gameObject.SetActive(true);
    }

    internal void HideAll()
    {
        HideScarft();
        render.enabled = false;
    }

    public void ShowAll()
    {
        ShowScarf();
        render.enabled = true;
    }

    internal void SetIdle()
    {
        animator.SetBool("Running", false);
        animator.SetTrigger("Idle");
    }

    internal void FadeIn()
    {
        if (fadeRutine != null) StopCoroutine(fadeRutine);
        fadeRutine = FadeAnimation();
        StartCoroutine(fadeRutine);
    }

    private IEnumerator FadeAnimation()
    {
        animator.SetTrigger("VFX_Appear");
        float t = 0;
        Color targetColor = Color.white;
        Color startColor = render.color;
        startColor.a = 0;
        do
        {
            t += Time.deltaTime / fadeInDuration;
            render.color = Color.Lerp(startColor, targetColor, t);
            yield return null;
        } while (t<1);
    }

    internal void SetHanging(bool v, int lookDir)
    {
        if (v)
        {
            animator.SetTrigger("HangIn");
            animator.SetBool("Running", false);
        }
        animator.SetBool("Hanging", v);

        if (lookDir == 1)
            visualRoot.localScale = new Vector3(1, 1, 1);
        else
            visualRoot.localScale = new Vector3(1, -1, 1);

        targetAngle = (lookDir == 1) ? 0 : 180;
    }

    internal void HookHit(int dir)
    {
        if (dir == 1)
            visualRoot.localScale = new Vector3(1, 1, 1);
        else
            visualRoot.localScale = new Vector3(1, -1, 1);

        targetAngle = (dir == 1) ? 0 : 180;
    }

    internal void SetEnable(bool v)
    {
        if(!v)
        {
            visualRoot.localScale = new Vector3(1, 1, 1);
            visualRoot.rotation = Quaternion.identity;
            targetAngle = 0;
            animator.Play("None", 0, 0);
        }
    }
}