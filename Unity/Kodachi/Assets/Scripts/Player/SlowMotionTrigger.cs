﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionTrigger : MonoBehaviour
{
    public float slowMotionDuration = .1f;
    public float targetTime = .1f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Arrow arrow = collision.GetComponent<Arrow>();
        if (arrow != null)
            TimeManager._StartTimeShift(slowMotionDuration, slowMotionDuration, true, true);
    }

    private void OnDestroy()
    {
        TimeManager._StartTimeShiftBack();
    }
}
