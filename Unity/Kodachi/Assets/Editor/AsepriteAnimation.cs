﻿using UnityEngine;
using UnityEditor;
using System.IO;
using Leguar.TotalJSON;
using System.Collections.Generic;
using System.Linq;
using System;

public class AsepriteAnimation : MonoBehaviour
{
    /// <summary>
    ///     ASEPRITE EXPORT SETTINGS.
    /// 
    /// OPTION A  All in one Sheet
    ///***********************
    /// Sheet Type: Packed 
    /// X SplitLayers 
    /// X SplitTags
    /// 
    /// X TriSprite
    /// X Extrude
    /// X TrimCels
    /// BorderPadding 0
    /// Spacing 0
    /// Inner padding 0
    /// Array 
    ///     X Meta
    ///     ✓ Tags
    ///     X Slices
    ///  ***********************   
    /// 
    /// OPTION B  Separated by tags
    /// ***********************
    /// Sheet Type: Packed 
    /// X SplitLayers 
    /// ✓ SplitTags
    /// 
    /// X TriSprite
    /// X Extrude
    /// X TrimCels
    /// BorderPadding 0
    /// Spacing 0
    /// Inner padding 0
    /// Array 
    ///     X Meta
    ///     ✓ Tags
    ///     X Slices
    ///  ***********************
    /// </summary>
    /// 

    ///TRIM SPRITE => Recorta todo el espacio NO UTILIZADO de la animacion ANTES DE EXPORTAR. TODOS LOS FRAMES TIENE EL MISMO TAMAÑO/MARCO/CELDA
    ///TRIM CEL => Recorta el espacio NO UTILIZADO de CADA FRAME. Cada frame pasa a tener un Marco diferente en base al espacio que necesita. 

    public const int PIXELS_PER_UNIT = 16;

    [MenuItem("Assets/Aseprite/ChangePivot/Center")] private static void ChangePivotToCenter() => ChangePivotTo(new Vector2(.5f, .5f));
    [MenuItem("Assets/Aseprite/ChangePivot/Down")] private static void ChangePivotToDown() => ChangePivotTo(new Vector2(.5f, 0));
    [MenuItem("Assets/Aseprite/ChangePivot/Up")] private static void ChangePivotToUp() => ChangePivotTo(new Vector2(.5f, 1));
    [MenuItem("Assets/Aseprite/ChangePivot/Left")] private static void ChangePivotToLeft() => ChangePivotTo(new Vector2(0, .5f));
    [MenuItem("Assets/Aseprite/ChangePivot/Right")] private static void ChangePivotToRight() => ChangePivotTo(new Vector2(.5f, 1));

    private static void ChangePivotTo(Vector2 pivot)
    {
        UnityEngine.Object[] selectedObjects = Selection.objects;
        for (int i = 0; i < selectedObjects.Length; i++)
            ChangePivot(selectedObjects[i], pivot);
    }

    private static void ChangePivot(UnityEngine.Object selected, Vector2 pivot)
    {
        string assetPath = AssetDatabase.GetAssetPath(selected);
        Texture2D texture = selected as Texture2D;
        Debug.Log(selected.GetType());

        TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
        importer.isReadable = true;
        importer.wrapMode = TextureWrapMode.Clamp;
        importer.textureType = TextureImporterType.Sprite;
        importer.spriteImportMode = SpriteImportMode.Multiple;
        importer.filterMode = FilterMode.Point;
        importer.GetPlatformTextureSettings("Android").textureCompression = TextureImporterCompression.Compressed;

        SpriteMetaData[] metas = importer.spritesheet;
        for (int i = 0; i < metas.Length; i++)
        {
            metas[i].alignment = (int)SpriteAlignment.Custom;
            metas[i].pivot = pivot;
        }
        importer.spritesheet = metas;
        EditorUtility.SetDirty(selected);
        AssetDatabase.ImportAsset(assetPath, ImportAssetOptions.ForceUpdate);
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Aseprite/RawToSpriteSheet_x16")]
    private static void RawToSpriteSheet()
    {
        UnityEngine.Object[] selectedObjects = Selection.objects;
        string jsonPath, spritePath, extension;

        for (int i = 0; i < selectedObjects.Length; i++)
        {
            jsonPath = AssetDatabase.GetAssetPath(selectedObjects[i]);
            extension = Path.GetExtension(jsonPath);
            if (extension != ".json")
            {
                Debug.LogWarning($"Archivo omitido: {jsonPath} (not a .json file)");
                continue;
            }
            spritePath = jsonPath.Remove(jsonPath.Length - 4, 4) + "png";
            RawToSpriteSheet(selectedObjects[i], jsonPath, spritePath);
        }
    }

    private static void RawToSpriteSheet(UnityEngine.Object selected, string jsonPath, string spritePath)
    {
        Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>(spritePath);
        Debug.Log(selected.GetType());

        TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(spritePath);
        importer.wrapMode = TextureWrapMode.Clamp;
        importer.textureType = TextureImporterType.Sprite;
        importer.spriteImportMode = SpriteImportMode.Multiple;
        importer.filterMode = FilterMode.Point;
        importer.GetPlatformTextureSettings("Android").textureCompression = TextureImporterCompression.Compressed;
        importer.GetPlatformTextureSettings("Android").overridden = true;
        importer.spritePixelsPerUnit = PIXELS_PER_UNIT;

        string stringJson = AssetDatabase.LoadAssetAtPath<TextAsset>(jsonPath).text;

        JSON json = JSON.ParseString(stringJson);
        JArray frames = json.GetJArray("frames");

        SpriteMetaData[] metas = new SpriteMetaData[frames.Length];

        for (int i = 0; i < frames.Length; i++)
        {
            JSON currentFrame = frames.GetJSON(i);
            SpriteMetaData spriteMetaData = new SpriteMetaData();
            spriteMetaData.name = $"{i.ToString("000")} " + currentFrame.GetString("filename");
            spriteMetaData.rect =
                Convert2DCoordinatesToUnityCoordinates(
                    RectFromJsonValue(currentFrame.GetJSON("frame")),
                    new Vector2(texture.width, texture.height));
            spriteMetaData.border = new Vector4(0, 0, 0, 0);
            spriteMetaData.alignment = (int)SpriteAlignment.Center;

            FrameData frameData = new FrameData(currentFrame);
            spriteMetaData.pivot = new Vector2(.5f, .5f);
            //------
            metas[i] = spriteMetaData;
        }

        importer.spritesheet = metas.ToArray();
        AssetDatabase.ImportAsset(spritePath, ImportAssetOptions.ForceUpdate);
    }

    [MenuItem("Assets/Aseprite/SpriteSheetToAnimation")]
    private static void SpriteSheetToAnimation()
    {
        UnityEngine.Object[] selectedObjects = Selection.objects;
        string jsonPath, spritePath, extension;
        for (int i = 0; i < selectedObjects.Length; i++)
        {
            jsonPath = AssetDatabase.GetAssetPath(selectedObjects[i]);
            extension = Path.GetExtension(jsonPath);
            if (extension != ".json")
            {
                Debug.LogWarning($"Archivo omitido: {jsonPath} (not a .json file)");
                continue;
            }
            spritePath = jsonPath.Remove(jsonPath.Length - 4, 4) + "png";
            CreateAnimationFromSpriteSheet(jsonPath, spritePath);
        }
    }

    private static void CreateAnimationFromSpriteSheet(string jsonPath, string spritePath)
    {
        Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>(spritePath);
        TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(spritePath);

        string stringJson = AssetDatabase.LoadAssetAtPath<TextAsset>(jsonPath).text;
        JSON json = JSON.ParseString(stringJson);

        //FRAMES
        Dictionary<int,FrameData> framesData = GetFramesDictionary(json.GetJArray("frames"));
        Sprite[] sprites = LoadSortedSpritesFrom(spritePath);

        List<int> keys = new List<int>(framesData.Keys);
        keys.Sort();

        //FRAMETAGS
        FrameTag cTag = new FrameTag(Path.GetFileNameWithoutExtension(jsonPath), keys[0], keys[keys.Count-1], 1);

        //FrameSlices
        GetFramesSliceData(json, 
            out FramesSliceData[] pivotFramSliceData, 
            out FramesSliceData rootSliceData,
            out FramesSliceData[] collidersSliceData);

        int pixelsPerUnit = Mathf.RoundToInt(sprites[0].pixelsPerUnit); 

        if (sprites.Length != 0)
            CreateAnimation(pixelsPerUnit, spritePath, framesData, sprites, cTag, pivotFramSliceData, rootSliceData, collidersSliceData);
        else
            Debug.LogError($"Sprites.Length == 0");
    }

    [MenuItem("Assets/Aseprite/MasterSpriteSheetToAnimations")]
    private static void CreateAnimationFromMasterSpriteSheet()
    {
        UnityEngine.Object[] selectedObjects = Selection.objects;
        string jsonPath, spritePath, extension;
        for (int i = 0; i < selectedObjects.Length; i++)
        {
            jsonPath = AssetDatabase.GetAssetPath(selectedObjects[i]);
            extension = Path.GetExtension(jsonPath);
            if (extension != ".json")
            {
                Debug.LogWarning($"Archivo omitido: {jsonPath} (not a .json file)");
                continue;
            }
            spritePath = jsonPath.Remove(jsonPath.Length - 4, 4) + "png";

            _CreateAnimationFromMasterSpriteSheet(jsonPath, spritePath);
        }
    }

    private static void _CreateAnimationFromMasterSpriteSheet(string jsonPath, string spritePath)
    {
        //bool isRootAnimation = EditorUtility.DisplayDialog("Root Motion", "Does the animation use Root Motion)","Yes", "No");

        Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>(spritePath);
        TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(spritePath);

        string stringJson = AssetDatabase.LoadAssetAtPath<TextAsset>(jsonPath).text;
        JSON json = JSON.ParseString(stringJson);
        //FRAMES
        Dictionary<int, FrameData> framesData = GetFramesDictionary(json.GetJArray("frames"));

        Sprite[] sprites = LoadSortedSpritesFrom(spritePath);

        //FRAMETAGS
        List<FrameTag> frameTags = GetFrameTags(json);
        Debug.Log($"frameTags.count: {frameTags.Count}");

        //FrameSlices
        GetFramesSliceData(json,
            out FramesSliceData[] pivotFramSliceData,
            out FramesSliceData rootSliceData,
            out FramesSliceData[] collidersSliceData);

        int pixelsPerUnit = Mathf.RoundToInt(sprites[0].pixelsPerUnit);

        for (int i = 0; i < frameTags.Count; i++)
        {
            FrameTag cTag = frameTags[i];
            CreateAnimation(
                pixelsPerUnit,
                spritePath, 
                framesData,
                sprites, 
                cTag,
                pivotFramSliceData, 
                rootSliceData,
                collidersSliceData);
        }
    }

    private static Sprite[] LoadSortedSpritesFrom(string spritePath)
    {
        List<Sprite> spriteList = new List<Sprite>(AssetDatabase.LoadAllAssetsAtPath(spritePath).OfType<Sprite>());
        spriteList.Sort(SpriteSorterByName);
        Sprite[] sprites = spriteList.ToArray();
        return sprites;
    }

    private static int SpriteSorterByName(Sprite x, Sprite y)
    {
        int xNumber = GetNumberFromSpriteName(x.name);
        int yNumber = GetNumberFromSpriteName(y.name);
        if (xNumber > yNumber) return 1;
        else if (xNumber < yNumber) return -1;
        else return 0;
    }

    private static int GetNumberFromSpriteName(string name)
    {
        int number = int.Parse(name.Split(' ')[0]);
//        Debug.Log($"GetNumberFromSpriteName: {name} => {number}");
        return number;
    }

    private static void GetFramesSliceData(JSON json, out FramesSliceData[] pivotSliceData, out FramesSliceData rootSliceData, out FramesSliceData[] collidersSliceData)
    {
        rootSliceData = null;
        collidersSliceData = null;

        //FrameSlices
        JSON meta = json.GetJSON("meta");
        if (!meta.ContainsKey("slices"))
        {
            pivotSliceData = null;
            Debug.LogWarning("Slices NOT FOUND");
            return;
        }
        JArray slicesArray = meta.GetJArray("slices");
        Debug.Log($"slicesArray.Lenght: {slicesArray.Length}");

        List<FramesSliceData> sliceData = new List<FramesSliceData>();
        List<FramesSliceData> collidersData = new List<FramesSliceData>();
        for (int i = 0; i < slicesArray.Length; i++)
        {
            JSON slicesJson = slicesArray.GetJSON(i);
            if (slicesJson.GetString("name").Contains("Pivot"))
                sliceData.Add(new FramesSliceData(slicesJson.GetJArray("keys"), slicesJson.GetString("name")));
            else if(slicesJson.GetString("name").Contains("Root"))
                rootSliceData = new FramesSliceData(slicesJson.GetJArray("keys"), slicesJson.GetString("name"));
            else if(slicesJson.GetString("name").Contains("Collider"))
                collidersData.Add(new FramesSliceData(slicesJson.GetJArray("keys"), slicesJson.GetString("name")));
        }
        pivotSliceData = sliceData.ToArray();
        collidersSliceData = collidersData.ToArray();
    }

    private static void CreateAnimation(int pixelsPerUnit, string spritePath, Dictionary<int,FrameData> framesData, Sprite[] sprites, FrameTag cTag, FramesSliceData[] pivotSlicesData, FramesSliceData rootSliceData, FramesSliceData[] collidersSliceData)
    {
        AnimationClip aClip = new AnimationClip();
        aClip.frameRate = 60;
        aClip.name = $"{Path.GetFileNameWithoutExtension(spritePath)}_{cTag.name}";
        int framesCount = Mathf.Abs(cTag.to - cTag.from);

        Debug.Log($"<color=green>       >>> Creating Animation:{aClip.name} From:{cTag.from} => TO:{cTag.to} </color>");

        ///AGREGAMOS UN FRAME EXTRA PARA PODER REPRESENTAR LA DURACION DEL ULTIMO FRAME
        ObjectReferenceKeyframe[] spriteKeyFrames = new ObjectReferenceKeyframe[framesCount + 1];

        //SpriteRenderer
        AddSpriteRendererAnimationData(framesData, sprites, cTag, aClip, framesCount, spriteKeyFrames);

        Debug.Log($"<color=green>       >>> Animation:{aClip.name} DONE </color>");

        //SpriteRenderer
        ApplyPosition(pixelsPerUnit, framesData, cTag, aClip, framesCount, rootSliceData, 
            out AnimationCurve xRootPosCurve, 
            out AnimationCurve yRootPosCurve,
            out AnimationCurve xRenderPosCurve,
            out AnimationCurve yRenderPosCurve);

        //Pivot From Slice
        if (pivotSlicesData != null)
            foreach (FramesSliceData item in pivotSlicesData)
                if (item != null)
                    AddPivotAnimationData(pixelsPerUnit,
                        framesData, 
                        sprites,
                        cTag, 
                        item,
                        aClip, 
                        framesCount, 
                        rootSliceData,
                        xRootPosCurve,
                        yRootPosCurve,
                        xRenderPosCurve,
                        yRenderPosCurve);

        foreach (FramesSliceData item in collidersSliceData)
        {
            bool isRootMotion = aClip.name.Contains("ROOT");

            AnimationCurve xPosCurve = new AnimationCurve();
            AnimationCurve yPosCurve = new AnimationCurve();
            AnimationCurve xSizeCurve = new AnimationCurve();
            AnimationCurve ySizeCurve = new AnimationCurve();

            float currentTime = 0;
            float oldTime = 0;
            int currentFrameIndex = 0;

            for (int localFrameIndex = 0; localFrameIndex <= framesCount; localFrameIndex++)
            {
                if (localFrameIndex != framesCount) //Se agrega una copia del ultimo frame para interpretar la duracion del ultimo frame
                {
                    currentFrameIndex = cTag.from + localFrameIndex;
                    oldTime = currentTime;
                }

                FrameData frameData = framesData[currentFrameIndex];

                Vector2 colliderPosition = GetSliceCenterPosition(item.GetSliceAtFrame(currentFrameIndex), framesData[currentFrameIndex]);
                Vector2 centerPosition = GetSliceCenterPosition(rootSliceData[currentFrameIndex], framesData[currentFrameIndex]);

                if (isRootMotion)
                {
                    colliderPosition -= centerPosition;
                    xPosCurve.AddKey(new Keyframe(currentTime, (colliderPosition.x) / pixelsPerUnit
                        + xRootPosCurve.Evaluate(currentTime)));
                    yPosCurve.AddKey(new Keyframe(currentTime, (-colliderPosition.y) / pixelsPerUnit
                        + yRootPosCurve.Evaluate(currentTime)));
                }   
                else
                {
                    colliderPosition -= centerPosition;
                    xPosCurve.AddKey(new Keyframe(currentTime, (colliderPosition.x) / pixelsPerUnit));
                    yPosCurve.AddKey(new Keyframe(currentTime, (-colliderPosition.y) / pixelsPerUnit));
                }
           /*     xPosCurve.AddKey(new Keyframe(currentTime, colliderPosition.x / pixelsPerUnit, float.PositiveInfinity, float.NegativeInfinity));
                yPosCurve.AddKey(new Keyframe(currentTime, -colliderPosition.y / pixelsPerUnit, float.PositiveInfinity, float.NegativeInfinity));*/

                #region Size
                Vector2 colliderSize = item.GetSliceAtFrame(currentFrameIndex).Bounds.size;
                xSizeCurve.AddKey(new Keyframe(currentTime, colliderSize.x / pixelsPerUnit));
                ySizeCurve.AddKey(new Keyframe(currentTime, -colliderSize.y / pixelsPerUnit));
                #endregion

                currentTime += framesData[currentFrameIndex].Duration / (float)1000;
            }

            EditorCurveBinding colliderPosX_Binding = EditorCurveBinding.FloatCurve("Visual/" + item.Name, typeof(BoxCollider2D), "m_Offset.x");
            EditorCurveBinding colliderPosY_Binding = EditorCurveBinding.FloatCurve("Visual/" + item.Name, typeof(BoxCollider2D), "m_Offset.y");
            EditorCurveBinding colliderSizeX_Binding = EditorCurveBinding.FloatCurve("Visual/" + item.Name, typeof(BoxCollider2D), "m_Size.x");
            EditorCurveBinding colliderSizeY_Binding = EditorCurveBinding.FloatCurve("Visual/" + item.Name, typeof(BoxCollider2D), "m_Size.y");

            AnimationUtility.SetEditorCurve(aClip, colliderPosX_Binding, xPosCurve);
            AnimationUtility.SetEditorCurve(aClip, colliderPosY_Binding, yPosCurve);
            AnimationUtility.SetEditorCurve(aClip, colliderSizeX_Binding, xSizeCurve);
            AnimationUtility.SetEditorCurve(aClip, colliderSizeY_Binding, ySizeCurve);

        }

        AssetDatabase.CreateAsset(aClip, CreateAnimationClipName(spritePath, cTag.name));
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void ApplyPosition(int pixelsPerUnit,
        Dictionary<int, FrameData> framesData, 
        FrameTag cTag,
        AnimationClip aClip,
        int framesCount,
        FramesSliceData rootFrameData, 
        out AnimationCurve xPosCurveRoot,
        out AnimationCurve yPosCurveRoot ,
        out AnimationCurve xPosCurveRender, 
        out AnimationCurve yPosCurveRender)
    {
        bool isRootMotion = aClip.name.Contains("ROOT");

        AnimationCurve xPosRootCurve = new AnimationCurve();
        AnimationCurve yPosRootCurve = new AnimationCurve();

        AnimationCurve xPosRenderCurve = new AnimationCurve();
        AnimationCurve yPosRenderCurve = new AnimationCurve();

        /* AnimationCurve xPosCurve = new AnimationCurve();
         AnimationCurve yPosCurve = new AnimationCurve();*/

        float currentTime = 0;
        float oldTime = 0;
        int currentFrameIndex = 0;

        //Frame => Cuadro que contiene el SPRITE, Es la Posicion del "Recorte" del sprite dentro del SpriteSheet
        //SpriteSourceSize => Porcion del "Recorte" del sprite que tiene informacion (Porcion del frame que ignora los bordes transparentes)
        //SourceSize => Tamaño Original del Frame en Aseprite
        //bool useLocalFrameIndex = (framesData.Count == sprites.Length);

        for (int localFrameIndex = 0; localFrameIndex <= framesCount; localFrameIndex++)
        {
            if (localFrameIndex != framesCount) //Se agrega una copia del ultimo frame para interpretar la duracion del ultimo frame
            {
                currentFrameIndex = cTag.from + localFrameIndex;
                oldTime = currentTime;
            }

            FrameData frameData = framesData[currentFrameIndex];
            Vector2 rootPosition, renderPosition;
            rootPosition = renderPosition = GetOffsetFromFrameData(frameData);

            //Correccion De Root
            if (rootFrameData != null)
            {
                if(!isRootMotion)
                    rootPosition += GetSliceCenterPosition(rootFrameData[currentFrameIndex], frameData);
                renderPosition -= GetSliceCenterPosition(rootFrameData[currentFrameIndex], frameData);
            }

            xPosRootCurve.AddKey(new Keyframe(currentTime, rootPosition.x / pixelsPerUnit, float.PositiveInfinity, float.NegativeInfinity));
            yPosRootCurve.AddKey(new Keyframe(currentTime, -rootPosition.y / pixelsPerUnit, float.PositiveInfinity, float.NegativeInfinity));

            xPosRenderCurve.AddKey(new Keyframe(currentTime, renderPosition.x / pixelsPerUnit, float.PositiveInfinity, float.NegativeInfinity));
            yPosRenderCurve.AddKey(new Keyframe(currentTime, -renderPosition.y / pixelsPerUnit, float.PositiveInfinity, float.NegativeInfinity));

            currentTime += framesData[currentFrameIndex].Duration / (float)1000;
        }

        #region Binding
        EditorCurveBinding renderPivotBindingX = EditorCurveBinding.FloatCurve("Visual/Render", typeof(Transform), "m_LocalPosition.x");
        EditorCurveBinding renderPivotBindingY = EditorCurveBinding.FloatCurve("Visual/Render", typeof(Transform), "m_LocalPosition.y");

        if (isRootMotion)
        {
            EditorCurveBinding rootPivotBindingX = EditorCurveBinding.FloatCurve(string.Empty, typeof(Transform), "m_LocalPosition.x");
            EditorCurveBinding rootPivotBindingY = EditorCurveBinding.FloatCurve(string.Empty, typeof(Transform), "m_LocalPosition.y");
            AnimationUtility.SetEditorCurve(aClip, rootPivotBindingX, xPosRootCurve);
            AnimationUtility.SetEditorCurve(aClip, rootPivotBindingY, yPosRootCurve);
            AnimationUtility.SetEditorCurve(aClip, renderPivotBindingX, xPosRenderCurve);
            AnimationUtility.SetEditorCurve(aClip, renderPivotBindingY, yPosRenderCurve);
        }
        else
        {
            AnimationUtility.SetEditorCurve(aClip, renderPivotBindingX, xPosRenderCurve);
            AnimationUtility.SetEditorCurve(aClip, renderPivotBindingY, yPosRenderCurve);
            xPosCurveRoot = AnimationCurve.Constant(0, currentTime, 0);
            yPosCurveRoot = AnimationCurve.Constant(0, currentTime, 0);
        }
        xPosCurveRoot = xPosRootCurve;
        yPosCurveRoot = yPosRootCurve;
        xPosCurveRender = xPosRenderCurve;
        yPosCurveRender = yPosRenderCurve;
        #endregion
    }

    private static Vector2 GetOffsetFromFrameData(FrameData frameData)
    {
        Vector4 recorteLeftUp = new Vector2(frameData.SpriteSourceSize.x, frameData.SpriteSourceSize.y);
        Vector4 recorteRightDown = new Vector2(
            frameData.SourceSize.x - (frameData.SpriteSourceSize.x + frameData.SpriteSourceSize.width),
             frameData.SourceSize.y - (frameData.SpriteSourceSize.y + frameData.SpriteSourceSize.height));
        Vector2 offset = (recorteLeftUp - recorteRightDown) / 2;
        return offset;
    }

    private static void AddPivotAnimationData(int pixelsPerUnit, Dictionary<int, FrameData> framesData, Sprite[] sprites, FrameTag cTag, FramesSliceData framesSliceData, AnimationClip aClip, int framesCount, FramesSliceData rootFrameData, AnimationCurve xPosCurveRoot, AnimationCurve yPosCurveRoot, AnimationCurve xRenderPosCurve, AnimationCurve yRenderPosCurve)
    {
        bool isRootMotion = aClip.name.Contains("ROOT");
        Debug.Log($"framesSliceData.Name: {framesSliceData.Name}");

        EditorCurveBinding pivotBindingX = EditorCurveBinding.FloatCurve("Visual/" + framesSliceData.Name, typeof(Transform), "m_LocalPosition.x");
        EditorCurveBinding pivotBindingY = EditorCurveBinding.FloatCurve("Visual/" + framesSliceData.Name, typeof(Transform), "m_LocalPosition.y");
        AnimationCurve xPosCurve = new AnimationCurve();
        AnimationCurve yPosCurve = new AnimationCurve();

        Debug.Log("Visual/" + framesSliceData.Name);

        float currentTime = 0;
        float oldTime = 0;
        int currentFrameIndex = 0;

        Debug.Log($"From: {cTag.from} to {cTag.to}");

       // bool useLocalFrameIndex = (framesData.Count == sprites.Length);

        for (int localFrameIndex = 0; localFrameIndex <= framesCount; localFrameIndex++)
        {
            if (localFrameIndex != framesCount) //Se agrega una copia del ultimo frame para interpretar la duracion del ultimo frame
            {
                //   currentFrameIndex = (useLocalFrameIndex) ? localFrameIndex : cTag.from + localFrameIndex;
                currentFrameIndex =  cTag.from + localFrameIndex;
                oldTime = currentTime;
            }

            Debug.Log(framesSliceData.Name);

            Vector2 offset = GetSliceCenterPosition(framesSliceData.GetSliceAtFrame(currentFrameIndex), framesData[currentFrameIndex]);
            Vector2 centerPosition = GetSliceCenterPosition(rootFrameData[currentFrameIndex], framesData[currentFrameIndex]);

            if(isRootMotion)
            {
                xPosCurve.AddKey(new Keyframe(currentTime, (offset.x) / pixelsPerUnit - xPosCurveRoot.Evaluate(currentTime) + xRenderPosCurve.Evaluate(currentTime), float.PositiveInfinity, float.NegativeInfinity));
                yPosCurve.AddKey(new Keyframe(currentTime, (-offset.y) / pixelsPerUnit - yPosCurveRoot.Evaluate(currentTime) + yRenderPosCurve.Evaluate(currentTime), float.PositiveInfinity, float.NegativeInfinity));
            }
            else
            {
                offset += centerPosition;

                xPosCurve.AddKey(new Keyframe(currentTime, (offset.x) / PIXELS_PER_UNIT  + 
                    (xRenderPosCurve.Evaluate(currentTime) - xPosCurveRoot.Evaluate(currentTime)), float.PositiveInfinity, float.NegativeInfinity));
                yPosCurve.AddKey(new Keyframe(currentTime, (-offset.y) / PIXELS_PER_UNIT + 
                    (yRenderPosCurve.Evaluate(currentTime) - yPosCurveRoot.Evaluate(currentTime)), float.PositiveInfinity, float.NegativeInfinity));
            }

            currentTime += framesData[currentFrameIndex].Duration / (float)1000;
        }
        Debug.Log("CCC");

        AnimationUtility.SetEditorCurve(aClip, pivotBindingX, xPosCurve);
        AnimationUtility.SetEditorCurve(aClip, pivotBindingY, yPosCurve);
    }

    private static Vector2 GetSliceCenterPosition(FrameSlice frameSlice, FrameData frameData)
    {
        return ((frameSlice.Bounds.center)) - frameData.SourceSize / 2;
    }

    private static void AddSpriteRendererAnimationData(Dictionary<int, FrameData> framesData, Sprite[] sprites, FrameTag cTag, AnimationClip aClip, int framesCount, ObjectReferenceKeyframe[] spriteKeyFrames)
    {
        int currentFrameIndex = 0;
        float oldTime = 0, currentTime = 0;

        EditorCurveBinding spriteBinding = new EditorCurveBinding();
        spriteBinding.type = typeof(SpriteRenderer);
        spriteBinding.path = "Visual/Render";
        spriteBinding.propertyName = "m_Sprite";
        //        Debug.Log($"spriteKeyFrames.Length: {spriteKeyFrames.Length}");

        //bool useLocalFrameIndex = (framesData.Count == sprites.Length);

        for (int localFrameIndex = 0; localFrameIndex <= framesCount; localFrameIndex++)
        {
            if (localFrameIndex != framesCount) //Se agrega una copia del ultimo frame para interpretar la duracion del ultimo frame
            {
                currentFrameIndex = cTag.from + localFrameIndex;
                oldTime = currentTime;
            }
            spriteKeyFrames[localFrameIndex].time = currentTime;
            spriteKeyFrames[localFrameIndex].value = sprites[currentFrameIndex];
            AnimationUtility.SetObjectReferenceCurve(aClip, spriteBinding, spriteKeyFrames);
            currentTime += framesData[currentFrameIndex].Duration / (float)1000;
        }
    }

    private static Rect RectFromJsonVector4(Vector4 frame)
    {
        return new Rect(frame.x, frame.y, frame.w, frame.z);
    }

    private static Vector2 CenterPositionFromRect(Vector4 frame)
    {
        Rect r = new Rect(frame.x, frame.y, frame.w, frame.z);
        return r.center;
    }

    private static List<FrameTag> GetFrameTags(JSON json)
    {
        JSON meta = json.GetJSON("meta");
        JSON[] tagsJson = meta.GetJArray("frameTags").AsJSONArray();
        List<FrameTag> frameTags = new List<FrameTag>();
        foreach (var item in tagsJson)
            frameTags.Add(new FrameTag(item));
        return frameTags;
    }

    private static string CreateAnimationClipName(string assetPath, string key)
    {
        string baseName = Path.GetFileNameWithoutExtension(assetPath);
        if(baseName.Contains(key))
            return GetFolderPathFromAssetPath(assetPath) + $"Clip_{baseName}.anim";
        else
            return GetFolderPathFromAssetPath(assetPath) + $"Clip_{baseName}_{key}.anim";
    }

    private static string GetFolderPathFromAssetPath(string assetPath)
    {
        string fileName = Path.GetFileName(assetPath);
        return assetPath.Remove (assetPath.Length - fileName.Length, fileName.Length);
    }

    private static Dictionary<int,FrameData> GetFramesDictionary(JArray data)
    {
        FrameData framesData;
        Dictionary<int, FrameData> frameDictionary = new Dictionary<int, FrameData>();
        for (int i = 0; i < data.Length; i++)
        {
            framesData = new FrameData(data.GetJSON(i));
            frameDictionary.Add(framesData.Index, framesData);
        }
        return frameDictionary;
    }

    /*
    private static FrameData[] GetFramesData(JArray data)
    {
        FrameData[] framesData = new FrameData[data.Length];
        for (int i = 0; i < data.Length; i++)
            framesData[i] = new FrameData(data.GetJSON(i));

        return framesData;
    }*/

    private static float FromMilisecondsToFrame(int miliseconds, int targetFrameRate)
    {
        Debug.Log($"targetFrameRate: {targetFrameRate} miliseconds: {miliseconds}");

        float proportion = miliseconds / (float)1000 ;
        Debug.Log($"proportion: {proportion}");

        float value = targetFrameRate * proportion;
        Debug.Log($"value: {value}");

        return value;
    }


    private static Rect Convert2DCoordinatesToUnityCoordinates(Rect rect, Vector2 imageSize)
    {
        return new Rect(
            rect.x,
            imageSize.y - rect.height - rect.y,
            rect.width,
            rect.height);
    }

    private static Rect RectFromJsonValue(JSON jSON)
    {
        return new Rect(jSON.GetFloat("x"), jSON.GetFloat("y"), jSON.GetFloat("w"), jSON.GetFloat("h"));
    }

    private static Vector4 Vector4FromJsonValue(JSON jSON)
    {
        return new Vector4(jSON.GetFloat("x"), jSON.GetFloat("y"), jSON.GetFloat("w"), jSON.GetFloat("h"));
    }


    private static string GetAnimationTag(string item)
    {
        //Debug.Log($"Item:{item}");
        if (item.Contains('#'))
            return item.Split('#')[1].Split(' ')[0];
     else
            return item.Split(' ')[0];
    }


    private struct FrameTag
    {
        public string name;
        public int from;
        public int to;
        public int direction;

        public FrameTag(string name, int from, int to, int direction)
        {
            this.name = name;
            this.from = from;
            this.to = to;
            this.direction = direction;
        }

        public FrameTag(JSON data)
        {
            name = data.GetString("name");
            from = data.GetInt("from");
            to = data.GetInt("to");
            direction = (data.GetString("direction") == "forward")?1:-1;
        }
    }

    private struct FrameData
    {
        public readonly int Index;
        public readonly string Name;
        public readonly Rect Frame;
        public readonly bool Rotated;
        public readonly bool Trimmed;
        public readonly Rect SpriteSourceSize;
        public readonly Vector2 SourceSize;
        public readonly int Duration;

        public FrameData(JSON data)
        {
            string fileName = data.GetString("filename");
            this.Index = int.Parse(fileName.Split(' ')[1].Split('.')[0]);
            this.Name = fileName.Split(' ')[0];
            this.Frame = RectFromJsonValue(data.GetJSON("frame"));
            this.Rotated = data.GetBool("rotated");
            this.Trimmed = data.GetBool("trimmed");
            this.SpriteSourceSize = RectFromJsonValue(data.GetJSON("spriteSourceSize"));
            this.SourceSize = new Vector2(data.GetJSON("sourceSize").GetInt("w"), data.GetJSON("sourceSize").GetInt("h"));
            this.Duration = data.GetInt("duration");
        }

    }
     
}
