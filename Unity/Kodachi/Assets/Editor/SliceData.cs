﻿using Leguar.TotalJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

internal class FramesSliceData
{
    public readonly string Name;
    private JArray jArray;
    //    private Dictionary<int,FrameSlice> frameSlices = new Dictionary<int, FrameSlice>();

    private FrameSlice[] frameSlices;

    public FramesSliceData(JArray frameSliceArray, string name)
    {
        this.Name = name;

        List<FrameSlice> slices = new List<FrameSlice>();

        int maxFrame = 0;
        int minFrame = int.MaxValue;

        int count = frameSliceArray.Length;

        //Recorremos los SLICES en el archivo. Pueden existir frames SIN informacion de SLICES. Estos frames tienen que ser rellenados luego con la informacion del ultimo SLICE existente
        for (int i = 0; i < count; i++)
        {
            FrameSlice frameSlice = new FrameSlice(frameSliceArray.GetJSON(i));

            if (frameSlice.FrameIndex < minFrame) minFrame = frameSlice.FrameIndex;
            if (frameSlice.FrameIndex > maxFrame) maxFrame = frameSlice.FrameIndex;
            slices.Add(frameSlice);
        }

        Debug.Log($"MinFrame: {minFrame} MaxFrame: {maxFrame}");

        //Creamos el arreglo de slices
        frameSlices = new FrameSlice[maxFrame + 1];

        //Agregamos los SLICES existentes en la lista al arreglo
        for (int i = 0; i < count; i++)
        {
            frameSlices[slices[i].FrameIndex] = slices[i];
        }

        FrameSlice lastSlice = frameSlices[minFrame];

        for (int i = 0; i < maxFrame; i++)
        {
            if (frameSlices[i] == null)
                frameSlices[i] = new FrameSlice(i, lastSlice.Bounds);
            else
                lastSlice = frameSlices[i];
        }
    }

    public FrameSlice this[int index] => GetSliceAtFrame(index);

    public FrameSlice GetSliceAtFrame(int i)
    {
        if (i > frameSlices.Length - 1)
            return frameSlices[frameSlices.Length - 1];
        else
            return frameSlices[i];
    }
}

public class FrameSlice
{
    int frameIndex;
    Rect bounds;
    //Vector2Int pivot;

    public int FrameIndex => frameIndex;
    public Rect Bounds => bounds;
    //public Vector2Int Pivot => pivot;

    public FrameSlice()
    {
        this.frameIndex = -1;
    }

    public FrameSlice(JSON jSON)
    {
        this.frameIndex = jSON.GetInt("frame");
        this.bounds = RectFromJsonValue(jSON.GetJSON("bounds"));
    }

    public FrameSlice(int index, Rect bounds)
    {
        this.frameIndex = index;
        this.bounds = bounds;
    }

    private Vector2Int GetVector2Int(JSON json) => new Vector2Int(json.GetInt("x"), json.GetInt("y"));
    private static Rect RectFromJsonValue(JSON jSON) => new Rect(jSON.GetFloat("x"), jSON.GetFloat("y"), jSON.GetFloat("w"), jSON.GetFloat("h"));

}