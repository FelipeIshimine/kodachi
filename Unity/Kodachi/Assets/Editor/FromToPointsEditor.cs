﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FromToPoints)), CanEditMultipleObjects]
public class FromToPointsEditor : Editor
{
    protected virtual void OnSceneGUI()
    {
        FromToPoints example = (FromToPoints)target;

        EditorGUI.BeginChangeCheck();
        Vector3 newTargetPosition = Handles.PositionHandle(example.From, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(example, "Change Look At Target Position");
            example.From = newTargetPosition;
        }

         newTargetPosition = Handles.PositionHandle(example.To, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(example, "Change Look At Target Position");
            example.To = newTargetPosition;
        }
    }
}